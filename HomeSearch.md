# daleviaje.com - Cambios en el home search

En este README se resumirán los cambios a tomar en cuenta para integrar con el markup actual del proyecto.

### Estilos

- Los estilos reemplazan a estilos actuales, pero limitados unicamente al section `.search-forms` para no afectar otros componentes del proyecto.
- Se recomienda que los estilos sean llamados en último lugar, para que tengan más relevancia al momento de aplicarse y evitar cualquier problema.
- Se recomienda usar el archivo `homeSearch.min.css`
- Si se quieren editar los estilos se recomienda hacerlo desde los archivos `scss` y compilarlos, para evitar problemas de versiones.

### Imágenes

- Todas las imágenes están optimizadas en calidad y tamaño, no hace falta hacerles algún tipo de mejora.
- Si se desea cambiar la url de las imagenes, todas las URLs estan definidas en el archivo `scss/homeSearch/_icons.scss`

### Cambios en Markup

En esta ocasión no se trabajo con Markups customizados para no alargar el tiempo de implementación, sin embargo es necesario hacer ciertos cambios y mejoras para poder aplicar todos los estilos correctamente.

#### Tab `#buscadorVuelos`

1. Checkbox de `Solo ida`, `Equipaje de bodega` y `Vuelos directos`

- Colocar el `input` antes del `label` para que el selector `input:checked + label` se aplique
- Agregar la etiqueta `for` en los labels para interactuar con el checkbox al presionar el label

2. Placeholders

- Cambiar `Saliendo:` por `Salida`
- Cambiar `Regresando:` por `Regreso`
- Cambiar `Aerolinea:` por `Aerolinea`

#### Tab `#buscadorVueloHotel`

1. Input de habitaciones

- Al `col-md2 col-xs12` que contiene el div `FlightsInfo` agregar clase `SpaceB`

2. Checkbox de `Vueldos directos`

- Colocar el `input` antes del `label`

3. Placeholders

- Cambiar `dd/mm/yyyy:` por `Salida` y `Regreso` para que vaya en linea con el primer tab
- Cambiar `Aerolinea:` por `Aerolinea`

#### Tab `#buscadorHoteles`

1. Input de habitaciones

- Al `col-md2 col-xs12` que contiene el div `#hotelsInfo` agregar clase `SpaceB`

2. Botón submit

- Sacar el div `col-xs-12 Centrado` del div `opcionesViaje` y ponerlo al mismo nivel

3. Placeholders

- Cambiar `dd/mm/yyyy:` por `Salida` y `Regreso` para que vaya en linea con el primer tab
- Cambiar `Ingrese Ciudad` por `Ciudad`

#### Tab `#buscadorAutos`

1. Placeholders

- Cambiar `dd/mm/yyyy:` por `Recoger` y `Devolver` para que vaya en linea con el resto de inputs similares

#### Tab `#buscadorSeguros`

1. Botón submit

- Sacar el div `form-submit Centrado` del div `opcionesViaje` y ponerlo al mismo nivel

2. Placeholders

- Cambiar `dd/mm/yyyy:` por `Salida` y `Regreso` para que vaya en linea con el primer tab
- Pais de origen agregar placeholder `Pais`
