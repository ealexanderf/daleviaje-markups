# daleviaje.com - Landing BI

En este README se resumirán los cambios a tomar en cuenta para integrar con el proyecto actual.

[Demo Online](https://jovial-wright-40b33b.netlify.app/landingBI.html)

### Estilos

- Los estilos no reemplazan a nada que actualmente este en el proyecto, para aplicarlos hay que agregar las clases necesarias en los elementos actuales.
- Los estilos son parte del archivo ya integrado `newStyles.min.css`
- Si se quieren editar los estilos se recomienda hacerlo desde los archivos `scss` y compilarlos, para evitar problemas de versiones.

### Imágenes

- Todas las imágenes son solamente placeholders con los tamaños ideales

### Markup existente

- Añadir la clase `landing_BI` en el `body` o en el primer div contenedor `#main`
- En el `Header` hay que reemplazar la imagen de los listones de DaleViaje por el logo de BI
  - Cambiar el `href` de la imagen
  - Agregar la clase `logo_BI` al div que contiene la imagen
- Cambios en el div con el id `dv-cuotas` (El div que contiene el encabezado de **daleviaje a tus sueños** y el banner principal)
  - Las imágenes del Banner deben estar dentro de los divs `dvMainBanner` (Este cambio podría hacerse general para mejorar los estilos de esta sección en el sitio completo).

### Markup nuevo

- Todo el markup nuevo esta dentro del div `dvLanding`.
- Cada fila de items debe ir dentro del div `dvLanding-row`.
  - Este div acomoda a todos sus hijos en una sola fila, no hace falta añadir ningun tipo de clase extra para que estos se acomplen.
