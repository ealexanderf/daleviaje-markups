﻿/*=========================================
        Init Calendar
            - funcion que inicia los parametros para agregar los calendarios
              de acuerdo al tipo de calendiario son inicialializados.
===========================================*/
function initCalendar(_parameters) {
    moment.locale('es');
    _parameters.minIntOne = (!("minIntOne" in _parameters)) ? 0 : _parameters.minIntOne;
    _parameters.minIntTwo = (!("minIntTwo" in _parameters)) ? 1 : _parameters.minIntTwo;
    _parameters.minDateOne = (!("minDateOne" in _parameters)) ? _parameters.minIntOne : _parameters.minDateOne;
    _parameters.minDateTwo = (!("minDateTwo" in _parameters)) ? _parameters.minIntTwo : _parameters.minDateTwo;
    _parameters.numberOfMonths = ("numberOfMonths" in _parameters) ? _parameters.numberOfMonths : 1;
    _parameters.calendarType = ("calendarType" in _parameters) ? _parameters.calendarType : 1;
    _parameters.maxDays = (!("maxDays" in _parameters)) ? 0 : _parameters.maxDays;
    _parameters.Lang = ("Lang" in _parameters) ? _parameters.Lang : "es";
   
    initCalendarUi(_parameters);
}

/*=========================================
        Init Calendar UI
            - funcion que agrega los calendarios de Jquery Ui
===========================================*/
function initCalendarUi(_parametersUi) {
    _parametersUi.img = (!("img" in _parametersUi)) ? 'images/calendar.gif' : _parametersUi.img;
    _parametersUi.format = (!("format" in _parametersUi)) ? 'dd-mm-yy' : _parametersUi.format;

    if ($(_parametersUi.selectorOne).data("dateRangePicker") != undefined && $(_parametersUi.selectorOne).data("dateRangePicker") != null) {
        try {
            $(_parametersUi.selectorOne).data("dateRangePicker").destroy();
        }
        catch (e) { }
    }
    
    if (_parametersUi.type == 'ui') {

    }

    if (_parametersUi.calendarType == 'month') {
        initCalendarMonth(_parametersUi);
        
    } else {

        var minDateFormat = moment().add(_parametersUi.minIntOne, "days").format("DD-MM-YYYY");
        var objRange = {
            format: 'DD-MM-YYYY',
            startDate: minDateFormat,
            language: 'es',
            separator: 'a',
            maxDays: _parametersUi.maxDays,
            swapControlBar: true,
            customOpenAnimation: function (cb) {
                $(this).fadeIn(500, cb);
            },
            customCloseAnimation: function (cb) {
                $(this).fadeOut(500, cb);
            }
        };
        if ($(_parametersUi.selectorOne).length > 0) { 
            if ('selectorTwo' in _parametersUi) {

                objRange.minDays = (!("minDateTwo" in _parametersUi)) ? _parametersUi.minIntTwo : _parametersUi.minDateTwo;
                objRange.autoClose = false;
                objRange.getValue = function () {
                    if ($(_parametersUi.selectorOne).val() && $(_parametersUi.selectorTwo).val())
                        return $(_parametersUi.selectorOne).val() + ' a ' + $(_parametersUi.selectorTwo).val();
                    else
                        return '';
                };
                objRange.setValue = function (s, s1, s2) {
                    $(_parametersUi.selectorOne).val(s1);
                    $(_parametersUi.selectorTwo).val(s2);
                };

           
                $(_parametersUi.selectorTwo).dateRangePicker(objRange).bind('datepicker-first-date-selected', function (event, obj) {
                    if ("selectorTwo" in _parametersUi) {
                        $(_parametersUi.selectorTwo).val(moment(obj.date1).format("DD-MM-YYYY"));
                        $(_parametersUi.selectorTwo).data('dateRangePicker').close();
                        $(_parametersUi.selectorOne).data('dateRangePicker').open();
                        $(_parametersUi.selectorOne).data('dateRangePicker').setStart(obj.date1);
                    }
                }).bind('datepicker-change', function (event, obj) {
                    if ("fnChangeTwo" in _parametersUi) {
                        _parametersUi.fnChangeTwo(this);
                    }
                    if ("fnChangeOne" in _parametersUi) {
                        _parametersUi.fnChangeOne(this);
                    }
                });
            

            } else {
                objRange.singleDate = true;
            }
            $(_parametersUi.selectorOne).dateRangePicker(objRange).bind('datepicker-first-date-selected', function (event, obj) {
                if ("selectorTwo" in _parametersUi && !($("#chkOWRT").is(":checked"))) {
                    $(_parametersUi.selectorOne).val(moment(obj.date1).format("DD-MM-YYYY"));
                    $(_parametersUi.selectorOne).data('dateRangePicker').close();
                    $(_parametersUi.selectorTwo).data('dateRangePicker').open();
                    $(_parametersUi.selectorTwo).data('dateRangePicker').setStart(obj.date1);
                }
            }).bind('datepicker-change', function (event, obj) {
                if ("fnChangeOne" in _parametersUi) {
                    _parametersUi.fnChangeOne(this);
                }
                if ("fnChangeTwo" in _parametersUi) {
                    _parametersUi.fnChangeTwo(this);
                }
            });
        }
    }

}

/*=========================================
        Validate Between Date UI
            - funcion que valida los calendarios de Jquery UI
===========================================*/
function validateBetweenDateUI(_parametersUi) {
    checkFormatDate(_parametersUi);
    var _dateOne = $(_parametersUi.selectorOne).datepicker('getDate');
    var _dateTwo = $(_parametersUi.selectorTwo).datepicker('getDate');

    if (!_dateOne) {
        $(_parametersUi.selectorOne).datepicker('setDate', _parametersUi.minDateOne);
        $(_parametersUi.selectorTwo).datepicker('setDate', _parametersUi.minDateTwo);
        return null;
    }

    var _dateDiferent = _dateOne.add(_parametersUi.minIntTwo, 'days').days();

    if (typeof _dateTwo == 'undefined' || _dateTwo == null || _dateTwo.length == 0) { //sino existe hacer este proceso;
        _dateTwo = _dateDiferent;
    }

    if ((_dateOne.getTime() >= _dateTwo.getTime()) || (_dateDiferent.getTime() >= _dateTwo.getTime())) {
        _dateTwo = _dateDiferent;
        $(_parametersUi.selectorTwo).datepicker("option", "minDate", _dateTwo);
    }
    else
        $(_parametersUi.selectorTwo).datepicker("option", "minDate", _dateOne);



}


function validateBetweenDateMonthUI(_parametersUi, selectedDate)
{
    formatJS = "DD-MM-YYYY";
    var _dateOne = $(_parametersUi.selectorOne).MonthPicker('GetSelectedDate');
    var _dateTwo = $(_parametersUi.selectorTwo).MonthPicker('GetSelectedDate');

    if (!_dateOne) {
        $(_parametersUi.selectorOne).val(_parametersUi.minDateOne.toString(formatJS));
        $(_parametersUi.selectorTwo).val(_parametersUi.minDateTwo.toString(formatJS));
        return null;
    }

    var _dateDiferent = moment(_dateOne).add(_parametersUi.minIntTwo, 'days');

    if (typeof _dateTwo == 'undefined' || _dateTwo == null || _dateTwo.length == 0) { //sino existe hacer este proceso;
        _dateTwo = _dateDiferent;
    }

    if ((moment(_dateOne).valueOf() >= moment(_dateTwo).valueOf()) || (moment(_dateDiferent).valueOf() >= moment(_dateTwo).valueOf())) {
        _dateTwo = _dateDiferent;
        $(_parametersUi.selectorTwo).MonthPicker('option', 'MinMonth', _dateTwo.format(formatJS));
        $(_parametersUi.selectorTwo).val(_dateTwo.format(formatJS));
    }
    else {
        $(_parametersUi.selectorTwo).MonthPicker('option', 'MinMonth', _dateOne);       
    }

    if (selectedDate.getTime() < moment().valueOf())
    {

        $(_parametersUi.selectorOne).val(moment().add(_parametersUi.minIntOne, 'days').format(formatJS));
    }
}

function checkFormatDate(_parametersUi)
{
    try {
        var date1 = $(_parametersUi.selectorOne).datepicker('getDate');
        if (date1.toString("dd-MM-yyyy") != $(_parametersUi.selectorOne).val())
            $(_parametersUi.selectorOne).datepicker('setDate', null);
    }
    catch(e) {
        $(_parametersUi.selectorOne).datepicker('setDate', null);
    }
    try {
        var date1 = $(_parametersUi.selectorTwo).datepicker('getDate');
        if (date1.toString("dd-MM-yyyy") != $(_parametersUi.selectorTwo).val())
            $(_parametersUi.selectorTwo).datepicker('setDate', null);
    }
    catch (e) {
        $(_parametersUi.selectorTwo).datepicker('setDate', null);
    }
}


var MonthLangES =  {
        year: 'Año',
        buttonText: 'Mes',
        prevYear: "año Ant",
        nextYear: "Año Sig",
        next12Years: 'Sig. 12 años',
        prev12Years: '12 años antes',
        nextLabel: "Sig",  
        prevLabel: "Ant.",
        jumpYears: "Saltar",
        backTo: "Volver a",
        months: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Agos", "Sept", "Oct", "Nov", "Dic"]
}

var MonthLangPT = {
    year: 'Ano',
    buttonText: 'Mês',
    prevYear: "ano Ant",
    nextYear: "Ano Sig",
    next12Years: 'Sig. 12 anos',
    prev12Years: '12 anos antes',
    nextLabel: "Sig",
    prevLabel: "Ant.",
    jumpYears: "Saltar",
    backTo: "Retorno",
    months: ["Jan", "Fev", "Març", "Abr", "Mai", "Jun", "Jul", "Agos", "Set", "Out", "Nov", "Dez"]
}

function initCalendarMonth(_parametersUi)
{
    var formatJsDate = "DD-MM-YYYY";
    var ii18 = MonthLangES;
    if(_parametersUi.Lang == "PT")
        ii18 = MonthLangPT;
    var today =  moment();
    $(_parametersUi.selectorOne).val(moment().add(_parametersUi.minIntOne, 'days').format(formatJsDate));
    $(_parametersUi.selectorTwo).val(moment().add(_parametersUi.minIntTwo, 'days').format(formatJsDate));

    $(_parametersUi.selectorOne).MonthPicker({
        Button: false,
        MinMonth: moment().add(_parametersUi.minIntOne, 'days').format(formatJsDate),
        MonthFormat: _parametersUi.format,
        i18n: ii18
    });
    $(_parametersUi.selectorOne).MonthPicker('option', 'OnAfterChooseMonth', function (selectedDate) { validateBetweenDateMonthUI(_parametersUi, selectedDate); });

    $(_parametersUi.selectorTwo).MonthPicker({
        Button: false,
        MinMonth: moment().add(_parametersUi.minIntTwo, 'days').format(formatJsDate),
        MonthFormat: _parametersUi.format,
        i18n: ii18
    });
    $(_parametersUi.selectorTwo).MonthPicker('option', 'OnAfterChooseMonth', function (selectedDate) { validateBetweenDateMonthUI(_parametersUi, selectedDate); });

}