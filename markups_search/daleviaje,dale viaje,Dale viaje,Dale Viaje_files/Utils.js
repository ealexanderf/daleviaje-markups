﻿var curSymbols = { "COP": "$", "USD": "$", "BRL": "R$", "EUR": "€" }
var GobjPassengerSearch = [];
var GobjPassengersTotal = [];

function getObjFecha(cad, format) {
    var DFecha = null;
    if (cad.indexOf("Date") >= 1)
        cad = cad.replace("/Date(", "").replace(")/", "");
    if (cad != null && cad != "") {
        if (cad.length > 24) {
            cad = cad.slice(0, 24);
        }
        /*
        if (cad.length > 10) {
            cad = cad + GetTimeZoneOffset();
        }
        */
        if (format != undefined)
            DFecha = moment(cad, format);
        if (DFecha == null)
            DFecha = moment(cad);
        if (DFecha == null)
            DFecha = parseIsoDatetime(cad);
    }
    return DFecha;
}
function parseIsoDatetime(dtstr) {
    return moment(dtstr);
}

function FormateaFecha(Fecha, format) {
    return getObjFecha(Fecha, format).format("DD-MM-YYYY");
}

function FormateaFechaHora(Fecha, format) {
    return getObjFecha(Fecha, format).format("YYYY-MM-DD HH:mm");
}

function GetTimeZoneOffset() {
    var T = moment().utcOffset() / 60;
    var symbol = T < 0 ? "+" : "-";
    return T.toString().length <= 2 ? symbol + "0" + Math.abs(T) + ":00" : symbol + Math.abs(T) + ":00";
}


function FormateaFechaLarga(Fecha, format) {
    var DFecha = getObjFecha(Fecha, format);
    return GObjTraduccion.resDiasSemanaLargo[DFecha.day()] + ', ' + DFecha.date() + ' ' + GObjTraduccion.resMesesLargo[DFecha.month()] + " " + DFecha.year();
}


function FormateaFechaCorta(Fecha, format) {
    var DFecha = getObjFecha(Fecha, format);
    return GObjTraduccion.resDiasSemanaCorto[DFecha.day()] + ', ' + DFecha.date() + ' ' + GObjTraduccion.resMeses[DFecha.month()] + " " + DFecha.year();
}

function FormateaHora(Fecha, format) {
    return getObjFecha(Fecha).format("HH:mm");
}

function FormateaHoraLarga(time) {
    var hour = Math.floor(time);
    var minutes = Math.floor((time - hour) * 60).toString();
    return '' + hour + 'h:' + ((minutes.length == 2) ? minutes : "0" + minutes) + 'm';
}

function FormatDateMonthYear(Fecha, format) {
    var DFecha = getObjFecha(Fecha, format);
    return GObjTraduccion.resMesesLargo[DFecha.getMonth()] + " " + DFecha.getFullYear();
}

function ValidaFecha(Fecha, Formato) {

    var res = false;
    try {
        var da = moment(Fecha, Formato);

        res = (da != null);
    }
    catch (err) {  };

    return res;

}

function minToHoras(min) {
   /* return Math.floor(min / 60) + "h:" + Math.floor(min % 60) + "m";*/
    return ("0" + Math.floor(min / 60)).slice(-2) + "h:" + ("0" + Math.floor(min % 60)).slice(-2) + "m";
}

function getDuraccionTotalVuelos(obj) {
    var minutosVuelos = 0 //;obj.duracao
    //[0].FlightTimeMinutes
    for (i = 0; i < obj.length; i++) {
        minutosVuelos += obj[i].FlightTimeMinutes;
    }
    return minToHoras(minutosVuelos);
}


function DuraccionDias(fecha1, fecha2, format) {
    var f1 = getObjFecha(fecha1.substring(0, 10));
    var f2 = getObjFecha(fecha2.substring(0, 10));
    var milSeg = Math.abs(f2.valueOf() - f1.valueOf());
    //console.log(fecha1,fecha2);
    var dur = milSeg / (1000 * 60 * 60 * 24);
    return Math.ceil(dur);
}

function Duraccion(fecha1, fecha2, format) {
    var f1 = getObjFecha(fecha1, format);
    var f2 = getObjFecha(fecha2, format);
    var milSeg = Math.abs(f2.valueOf() - f1.valueOf());
    return Math.floor((milSeg / (1000 * 60 * 60))) + "h:" + Math.floor((milSeg / (1000 * 60)) % 60) + "m";
}

function diff_years(dt2, dt1) {
    var diff = (dt2.valueOf() - dt1.valueOf()) / 1000;
    diff /= (60 * 60 * 24);
    return Math.abs(Math.floor(diff / 365.25));
}

function params2json(d) {
    d = decodeURIComponent(d);
    if (d.length <= 0) {
        return "{}";
    }
    var json = "{";
    var data = d;
    var parejas = new Array();
    parejas = data.split('&');
    for (var i = 0; i < parejas.length; i++) {
        var info = new Array();
        info = parejas[i].split('=');
        json = json + "\"" + info[0] + "\":\"" + info[1] + "\",";
    }
    json = json.substring(0, json.length - 1);
    json = json + "}";
    return json;
}

function params2jsonArray(d) {
    d = decodeURIComponent(d);
    if (d.length <= 0) {
        return "[{}]";
    }
    var json = "[{";
    var data = d;
    var parejas = new Array();
    parejas = data.split('&');

    var info = new Array();
    info = parejas[0].split('=');
    var campo = info[0];

    for (var i = 0; i < parejas.length; i++) {

        info = parejas[i].split('=');

        if (campo == info[0]) {
            if (i > 0)
                json = json.substring(0, json.length - 1) + '},{';
        }

        json = json + "\"" + info[0] + "\":\"" + info[1] + "\",";
    }
    json = json.substring(0, json.length - 1);
    json = json + "}]";
    return json;
}

function PaginaActual() {

    var sPath = window.location.pathname;
    return sPath.substring(sPath.lastIndexOf('/') + 1).toUpperCase();
}

function isEmpty(obj) {

    for (var i in obj) {
        return false;
    }
    return true;
}


function templateModal(template, Titulo, cerrar, params, obj, width) {
    if (typeof(width) == "undefined")
        width = 800;
    
    var html = '<div id="tplContent"><div id="TPLModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
    html += ' <div class="modal-dialog" style="z-index: 99999"><div class="modal-content" style="z-index:99999">';
    html += '<div class="modal-header">';
    html += ' <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
    html += '<h4>' + Titulo + '</h4>';
    html += '</div>';
    html += '<div class="modal-body" id="TPLTxt"></div>';
    html += '<div class="modal-footer">';
    if (cerrar != "")
        html += '<span class="btn" data-dismiss="modal" aria-hidden="true" id="btnModalCerrar">' + cerrar + '</span>'; // close button

    html += '</div></div></div></div></div>';
    if ($("#tplContent").length >= 0)
        $("#tplContent").remove();
    $("body").append(html);
    $("#TPLTxt").setTemplate(getTemplateText(template), null, { filter_data: false });
    for (var i = 0; i < params.length; i++) {
        $("#TPLTxt").setParam(params[i].name, params[i].value);
    }
    if (typeof (obj) != "undefined")
        $("#TPLTxt").processTemplate(obj);
    else
        $("#TPLTxt").processTemplate({});
    $("#TPLTxt").show();
    $("#TPLModal").modal("show");
}


function Mensaje(Texto, Titulo, Tipo, onAceptar, txtBoton, onDismiss) {
    Mensaje_Boostrap(Texto, Titulo, Tipo, onAceptar, txtBoton, onDismiss);
}

function Mensaje_Boostrap(Texto, Titulo, Tipo, onAceptar, txtBoton, onDismiss) {
    var NomFuncionAceptar = '"javascript:hideModal()"';
    var NomFuncionCerrar = '';
    var dblBtn = false;

    if (typeof (onDismiss) != "undefined" && onDismiss != null) {
        NomFuncionCerrar = onDismiss;
    } 
    if (typeof (onAceptar) != "undefined" && onAceptar != null)
        NomFuncionAceptar = 'javascript:' + onAceptar;
    
    var aceptar = GObjTraduccion.resBotonAceptar;

    if (typeof (txtBoton) == "undefined" && txtBoton == null)
        txtBoton = "";
    else
        aceptar = txtBoton;

    var html = '<div id="MSGModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
    html += ' <div class="modal-dialog" style="z-index: 99999;"><div class="modal-content" style="z-index:99999">';
    html += '<div class="modal-header">';
    html += ' <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
    html += '<h4>' + Titulo + '</h4>'
    html += '</div>';
    html += '<div class="modal-body" id="MSGModalBody">';
    html += '<p>' + Texto + '</p></div>';
    html += '<div class="modal-footer">';

    if (NomFuncionAceptar != "" && onAceptar != null) {
        html += '<span class="btn btn-success"';
        html += ' onClick=' + NomFuncionAceptar + '>' + aceptar;
        html += '</span>';
        dblBtn = true;
    }
    if (txtBoton != "" && !dblBtn)
        html += '<span class="btn btn-default" data-dismiss="modal" aria-hidden="true">' + txtBoton + '</span>'; // close button

    html += '</div></div></div></div>';
    $("#divMensaje").html(html);
    $("#MSGModal").modal();

    if (NomFuncionCerrar != "" && onDismiss != null) {
        $("#MSGModal").on("hide.bs.modal", NomFuncionCerrar );
    }

}


function hideModal() {
    $('.modal.in').modal('hide');
}

function CerrarMensaje(onAceptar) {

    $('#divMensaje').dialog("close");
    if (typeof (onAceptar) != "undefined") {
        onAceptar();
    }
}


function Pregunta(Texto, Titulo, onAceptar) {

    var NomFuncionAceptar = "";
    if (onAceptar != undefined) {
        NomFuncionAceptar = onAceptar;
    }

    $("#divMensaje").html("<div class=\"mensajeModal\"><h2>" + Titulo + "</h2><div id=\"innerMensaje\"><p><label>"
        + Texto + "</label></p></div><input id=\"btnMensajeCancelar\" type=\"submit\" value=\"Cancelar\" onclick=\"javascript:CerrarMensaje();\"/>"
        + "<input id=\"btnMensajeAceptar\" type=\"submit\" value=\"" + GObjTraduccion.resBotonAceptar + "\" onclick=\"javascript:CerrarMensaje(" + NomFuncionAceptar + ");\"/><p><a href=\"javascript:CerrarMensaje();\" class=\"btnCerrar\">X</a></p>");

    $("#divMensaje").fadeIn();
}

function getJSONResultAJAX(data) {
	if(data==null)
		return null;
    data = data.replace(/\n/g, " ").replace(/\r/g, " ");
    return $.evalJSON(data);
}

function setChars(s, at, c) {
    return s.substr(0, at) + c.substr(0, s.length - at) + s.substr(at + c.length);
}

function ConvierteDiaSemana(Dia) {

    var SubDia = 0;
    switch (Dia) {

        case "lblSunday":
            SubDia = 0;
            break;
        case "lblMonday":
            SubDia = 1;
            break;
        case "lblTuesday":
            SubDia = 2;
            break;
        case "lblWednesday":
            SubDia = 3;
            break;
        case "lblThursday":
            SubDia = 4;
            break;
        case "lblFriday":
            SubDia = 5;
            break;
        case "lblSaturday":
            SubDia = 6;
            break;
    }
    return SubDia

}
function ultimoDiaMes(mes, anno) {
    if (mes.charAt(0) == '0')
        mes = mes.charAt(1);
    mes = parseInt(mes);
    anno = parseInt(anno);
    switch (mes) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12: return 31;
        case 2: return (anno % 4 == 0) ? 29 : 28;
    }
    return 30;
}
function formatNumber(valor, decimales) {
    if (decimales == undefined)
        decimales = parseInt(GObjParametrosConfig.B2C2_NumeroDecimales);
    var num = CeilDecimales(valor, decimales);
    if (num == NaN)
        return valor;
    num += '';
    x = num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

function formatCurrency(valor, format, currency, getcurrency) {

    // return "$" + valor.toFixed(parseInt(GObjParametrosConfig.B2C2_NumeroDecimales));

    if (typeof (format) == "undefined" || format == "")
        format = "code";
    if (typeof (currency) == "undefined")
        currency = GObjDatosDominio.moneda.toUpperCase();
    if (typeof (getcurrency) == "undefined")
        getcurrency = false;

    if (typeof (Intl) != "undefined") {
        //TODO: parametrizar el locale
        var formatter = new Intl.NumberFormat(getLocaleFromDomainObj(), {
            style: 'currency',
            currency: currency,
            currencyDisplay: format,
            maximumFractionDigits: parseInt(GObjParametrosConfig.B2C2_NumeroDecimales),
            minimumFractionDigits: parseInt(GObjParametrosConfig.B2C2_NumeroDecimales)
        });
        var cad = formatter.format(valor);
        if (!getcurrency) {
            var decimales = typeof(GObjParametrosConfig.B2C2_NumeroDecimales) != "undefined" ? GObjParametrosConfig.B2C2_NumeroDecimales : 0;
            var iconFormat = typeof (GObjParametrosConfig.B2C2_iconoFormatoMoneda) != "undefined" ? GObjParametrosConfig.B2C2_iconoFormatoMoneda : "$";
            if (cad.indexOf(currency) != -1) {
                try {
                    if (decimales > 0) {
                        cad = (cad.replace(currency, "")).trim();
                        cad = Number.parseFloat(valor).toFixed(decimales);
                        cad = iconFormat + cad;
                    }
                    else
                    {
                        cad = (cad.replace(currency, "")).trim();
                        cad = iconFormat + cad;
                    }
                }
                catch
                {
                    cad = (cad.replace(currency, "")).trim();
                    cad = iconFormat + cad;
                }
            }
        }
        return cad;
    }
    else {
        return formatNumber(valor);

    }
}

    function getLocaleFromDomainObj() {
        if (GObjDatosDominio.moneda == "COP") {
            return "es-CO";
        }
        if (GObjDatosDominio.moneda == "CLP") {
            return "es-CL";
        }
        if (GObjDatosDominio.moneda == "PYG") {
            return "es-PY";
        }
        return "en-US";
    }

function CeilDecimales(valor, decimales) {

    try {
        var ld = parseInt(1 + (new Array(decimales + 1).join("0")));
        return Math.ceil(valor * ld) / ld;
    }
    catch (err) {
    }
    return Math.ceil(valor);
}


Array.prototype.comparison = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    for (var i = 0; i < this.length; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].compare(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
}

wareThread = function () {

    var funcs = [], threading = true,
        thread = null,
        thread_speed = 100;

    /*** 
     *	Registering a function into a queue.
     */
    this.register = function (func) {
        funcs[funcs.length] = func;
    }

    /***
     *	Execute the queue to run through the list of functions.
     */
    this.execute = function () {

        var index = 0,
            funcsLen = funcs.length;

        /**
         * Threading or None Threaded Function Loops
         */
        if (threading) {
            var thread = setInterval(function () {

                if (index < funcsLen) {
                    //If the function returns false stop!
                    var res = false;
                    try {
                        res = funcs[index]();
                    }
                    catch (err) { }
                    if (res === false) {
                        clearInterval(thread);
                        return false;
                    } else {
                        //If not false increment up and do the next.
                        index += 1;
                    }
                } else {
                    clearInterval(thread);
                }
            }, thread_speed);
        } else {
            /** 
             * Regular loop through 
             */
            for (/* index = 0*/; index < funcsLen; index += 1) {
                if (funcs[index]() === false) {
                    return false;
                }
            }
            return true;
        }
    }
}


function validaObj(obj, tipo, msg) {
    if (!ValidarCadena(obj.value, tipo)) {
        Mensaje(msg, document.title, "INFO");
        obj.value = "";
        obj.focus();
    }
}
function ValidarCadena(cad, tipo) {
    switch (tipo) {
        case "nombre":
            var reg = /^([a-z ñáéíóú]{2,60})$/i;
            if (reg.test(cad)) return true;
            else return false;
            break;
        case "email":
            var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (reg.test(cad)) return true;
            else return false;
            break;
    }
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

/*  ANALITYCS */

(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

function fechaServidor(cadFecha) {
    var momento = moment(cadFecha).zone(cadFecha);
    return new Date(momento.format("ddd MMM D YYYY"));
}

function googleAnalyctics(cod, url) {
    if (cod && url) {
        ga('create', cod, url);
        ga('send', 'pageview');
        ga('set', 'contentGroup1', "Entidad:_" + GObjDatosDominio.codentidad);
    }
}


function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function formatDateUTC(obj, format) {
    if (obj.indexOf("DATE") != -1)
        return (new Date(parseInt(obj.replace("/Date(", "").replace(")/", "")))).toString(format);
    else
        return (new Date(obj)).toString(format);
}

function fillFrmReserva() {
    var tipo = "";
    if (typeof (GObjInfoPasajeros) != 'undefined')
        for (var i = 0; i < GObjInfoPasajeros.length; i++) {
            if (GObjInfoPasajeros[i].type = "ADT")
                tipo = "ad";

            if (GObjInfoPasajeros[i].type = "CHD")
                tipo = "chd";

            if (GObjInfoPasajeros[i].type = "inf")
                tipo = "inf";

            $("#pasa" + tipo + "_nombre_" + i).val(GObjInfoPasajeros[i].name);
            $("#pasa" + tipo + "_apellido_" + i).val(GObjInfoPasajeros[i].lastname);
            if ($("#pasa" + tipo + "_email_" + i).length > 0)
                $("#pasa" + tipo + "_email_" + i).val(GObjInfoPasajeros[i].email);
            if ($("#pasa" + tipo + "_fechanac_" + i).length > 0)
                $("#pasa" + tipo + "_fechanac_" + i).val(GObjInfoPasajeros[i].birthday);
            if ($("#pasa" + tipo + "_sexo_" + i).length > 0)
                $("#pasa" + tipo + "_sexo_" + i).val(GObjInfoPasajeros[i].gender);
        }
}

function imgError(image, imageURL) {
    if (typeof imageQueue === 'undefined') {
        imageQueue = [];
    }

    if (imageQueue[image.id] == undefined) {
        imageQueue[image.id] = 1;
    } else {
        imageQueue[image.id]++;
    }

    if (imageQueue[image.id] < 4)    //tres intentos
    {
        setTimeout(function () {
            image.src = imageURL.replace('http://', '//');
        }, (1000 * imageQueue[image.id]));
    } else {
        image.onerror = null;
        image.src = GObjParametrosConfig.B2C_URLImagenes + "/layout/noImagePlan.jpg";
    }
}
function fillFrmPassengers() {
    if (typeof (GObjPasajerosInfo) != 'undefined')
        for (var i = 0; i < GObjPasajerosInfo.length; i++) {
            if (GObjPasajerosInfo[i].tipoPax == "ADT")
                var sufijo = "ad";
            if (GObjPasajerosInfo[i].tipoPax == "CHD")
                var sufijo = "ni";
            if (GObjPasajerosInfo[i].tipoPax == "INF")
                var sufijo = "inf";
            $("#pasa" + sufijo + "_nombre_" + (i + 1)).val(GObjPasajerosInfo[i].nombrePax);
            $("#pasa" + sufijo + "_apellido_" + (i + 1)).val(GObjPasajerosInfo[i].apellidoPax);
            $("#pasa" + sufijo + "_email_" + (i + 1)).val(GObjPasajerosInfo[i].emailPax);
            $("#pasa" + sufijo + "_fechanac_" + (i + 1)).val(GObjPasajerosInfo[i].fechanacPax);
            $("#pasa" + sufijo + "_sexo_" + (i + 1)).val(GObjPasajerosInfo[i].sexoPax);
        }
    if (typeof (GObjDatosUsuario) != "undefined" && GObjDatosUsuario != null)
        $("#email_contact").val(GObjDatosUsuario.usua_email);

}


function ContenidoNombre(Contenedor, NombreContenido, onTermino) {

    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/ListarContenidoNombre",
        contentType: "application/json; charset=utf-8",
        data: '{"nombreContenido":"' + NombreContenido + '"}',
        dataType: "json",
        async: true,
        success: function (res) {
            if (res.d == "") {
                return;
            }
            var objContenido = getJSONResultAJAX(res.d);
            if (objContenido.ListaContenido != null) {
                $("#" + Contenedor).html(objContenido.ListaContenido.Contenido[0].Contenido);

                if (onTermino != undefined) {
                    onTermino();
                }
            }
            cargarFrms();
        }
    });
}

function ContenidoCategoria(Contenedor, codCategoria, Template) {

    var objContenido = GObjContenidos["cat_" + codCategoria];
    if ((objContenido != null) && (objContenido.ListaContenido != null)) {
        $("#" + Contenedor).setTemplate(getTemplateText(Template), null, { filter_data: false });
        $("#" + Contenedor).processTemplate(objContenido);
    }
}

function MostrarPaginaContenido(ContPpal, ContSecundario) {
    location.href = "/Contenido/contenido.aspx?NombreContPpal=" + encodeURIComponent(ContPpal) + "&CodCategoriaSec=" + ContSecundario;
}
//TODO: definir mejor manera de comprobar los formularios
function cargarFrms() {
    if (typeof (GObjParametrosConfig.B2C2_CodigoFormularioDinamico) != "undefined" && $("#frmSolicitud").length > 0) {
        formulario(GObjParametrosConfig.B2C2_CodigoFormularioDinamico, "frmSolicitud");
    }
}

function ContenidoHotelesDestacados(Contenedor, CantidadHoteles) {

    if ((typeof (GObjHotelesDestacados) != 'undefined') && (GObjHotelesDestacados != null)) {
        $("#" + Contenedor).setTemplate(getTemplateText('hoteles_recomendados'), null, { filter_data: false });
        $("#" + Contenedor).setParam('CantidadHoteles', CantidadHoteles);
        $("#" + Contenedor).setParam('contenedor', Contenedor);
        $("#" + Contenedor).processTemplate(GObjHotelesDestacados);
    }
}
function fixContenido() {
    //  setTimeout(function () { $("#divContenido").appendTo("#mainContent"); $("#divContenido").css("display", "block") }, 500);
}

function chatZopim() {
    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) {
            z._.push(c)
        }, $ = z.s = d.createElement(s), e = d.getElementsByTagName(s)[0];
        z.set = function (o) {
            z.set._.push(o)
        };
        z._ = [];
        z.set._ = [];
        $.async = !0; $.setAttribute('charset', 'utf-8');
        $.src = '//cdn.zopim.com/?' + GObjParametrosConfig.B2C2_keyChat;
        z.t = +new Date;
        $.type = 'text/javascript';
        e.parentNode.insertBefore($, e);
    })(document, 'script');
}

function googleTagManager(GTMid) {
    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', GTMid);
}

function IniComboPaises(idCmbPaises, idCmbEstados, idCmbCiudades, select) {
    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/ListaGeoNombres",
        data: '{"Tipo": "P", "Padre": "' + 0 + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        //TODO ERROR
        error: function (x, t, m) {
            if (t === "timeout") {
                logThis('Time Out');
            } else {
                var objError = getJSONResultAJAX(x.responseText);
            }
        },
        success: function (res) {
            var obj = $.evalJSON(res.d);
            //$("#" + pre + "Paises").html("");
            for (var i = 0; i < obj.length; i++) {
                var selected = "";                
                if (select != null && select.toUpperCase() == RemoveAccents(obj[i].gnom_nombre.toUpperCase()))
                    selected = " selected='selected'";
                $("#" + idCmbPaises).append('<option value="' + obj[i].geon_geon + "_" + obj[i].gnom_nombre + '"' + selected + '>' + obj[i].gnom_nombre + '</option>');
            }
            if (idCmbEstados != null)
                IniComboEstados(idCmbPaises, idCmbEstados, idCmbCiudades);
        }
    });
}
function IniComboEstados(idCmbPaises, idCmbEstados, idCmbCiudades) {
    if ($("#" + idCmbPaises).length == 0)
        return;
    var valorPadre = $("#" + idCmbPaises)[0].value.split("_")[0];

    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/ListaGeoNombres",
        data: '{"Tipo": "E", "Padre": "' + valorPadre + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        //TODO ERROR
        error: function (x, t, m) {
            if (t === "timeout") {
                logThis('Time Out');
            } else {
                var objError = getJSONResultAJAX(x.responseText);
            }
        },
        success: function (res) {
            var obj = $.evalJSON(res.d);
            $("#" + idCmbEstados).html("");
            for (var i = 0; i < obj.length; i++) {
                $("#" + idCmbEstados).append('<option value="' + obj[i].geon_geon + "_" + obj[i].gnom_nombre + '">' + obj[i].gnom_nombre + '</option>');
            }
            if (idCmbCiudades != null)
                IniComboCiudades(idCmbPaises, idCmbEstados, idCmbCiudades);
        }
    });
}

function IniComboCiudades(idCmbPaises, idCmbEstados, idCmbCiudades) {
    if ($("#" + idCmbPaises).length == 0)
        return;
    var valorPadre = $("#" + idCmbEstados).val().split("_")[0];

    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/ListaGeoNombres",
        data: '{"Tipo": "C", "Padre": "' + valorPadre + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        //TODO ERROR
        error: function (x, t, m) {
            if (t === "timeout") {
                logThis('Time Out');
            } else {
                var objError = getJSONResultAJAX(x.responseText);
            }
        },
        success: function (res) {
            var obj = $.evalJSON(res.d);
            $("#" + idCmbCiudades).html("");
            for (var i = 0; i < obj.length; i++) {
                $("#" + idCmbCiudades).append('<option value="' + obj[i].geon_geon + "_" + obj[i].gnom_nombre + '">' + obj[i].gnom_nombre + '</option>');
            }
        }
    });
}

function fixHotelBedsImg(image) {
    try {
        var imageURL = imgObj.src;
        if (typeof (imageQueue) === 'undefined') {
            imageQueue = [];
        }
        if (imageQueue[image.id] == undefined) {
            imageQueue[image.id] = 1;
        } else {
            imageQueue[image.id]++;
        }
        if (imageQueue[image.id] < 4)    //tres intentos
        {
            setTimeout(function () {
                if (imageQueue[image.id] < 2) {
                    image.src = imageURL.replace('http://', '//');
                } else if (altImageURL != 'undefined' && altImageURL != "") {
                    image.src = altImageURL.replace('http://', '//');
                } else {
                    image.src = imageURL.replace('http://', '//').replace('/giata/bigger/', '/giata/');
                }
            }, (500 * imageQueue[image.id]));
        }
        else if (imageQueue[image.id] < 7) {
            setTimeout(function () {
                if (imageQueue[image.id] < 5) {
                    image.src = imageURL.replace('http://', '//');
                } else {
                    image.src = imageURL.replace('http://', '//').replace('/giata/bigger/', '/giata/small/');
                }
            }, (500 * imageQueue[image.id]));
        }
    }
    catch (e) {
        image.src = "/images/noImageBlue.png";
    }

}

function imgErrorHotels(image, sourceName) {
    if (typeof (sourceName) != 'undefined' && (sourceName.toUpperCase() == 'HOTELBEDS' || sourceName.toUpperCase() == 'HOTELS_HOTELBEDS')) {
        fixHotelBedsImg(image);
    }
    else {
        image.onerror = null;
        image.src = "/images/noImageBlue.png";
    }
}


function ContenidoNombretoModal(NombreContenido, onTermino, onAceptar, txtBoton) {

    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/ListarContenidoNombre",
        contentType: "application/json; charset=utf-8",
        data: '{"nombreContenido":"' + NombreContenido + '"}',
        dataType: "json",
        async: true,
        success: function (res) {
            var objContenido = getJSONResultAJAX(res.d);
            if (objContenido != null && objContenido.ListaContenido != null) {
                Mensaje_Boostrap(objContenido.ListaContenido.Contenido[0].Contenido, "", "", onAceptar, txtBoton);                
            }
            if (onTermino != undefined) {
                onTermino();
            }
        }
    });
}

function LocalSave(name, obj) {
    if (typeof (obj) == 'undefined') {
        return JSON.parse(window.localStorage[name]);
    } else {
        window.localStorage[name] = JSON.stringify(obj);
    }
}

function getAirlineLogo(cia) {
    for (var key in GObjCiasAereas) {
        if (GObjCiasAereas.hasOwnProperty(key) && GObjCiasAereas[key].NomCia.toLowerCase() == cia.toLowerCase()) {
            return "<img src='" + GUrlCiasAereas + "/" + key + ".png' />";
        }
    }
    return "";
}

function objPathValidate(obj, strPath) {
    var empty = {};
    var path = strPath.split(".");
    var valid = true;
    try {
        for (var i = 1; i < path.length && valid; i++) {
            var array = false;
            if (path[i].indexOf("[") != -1) {
                var arrayName = path[i].substring((path[i].indexOf("[") + 1), path[i].indexOf("]"));
                path[i] = path[i].substring(0, (path[i].indexOf("[")));
                array = true;
            }
            if ((obj || empty).hasOwnProperty(path[i]))
                obj = array ? obj[path[i]][arrayName] : obj[path[i]];
            else
                valid = false;
        }
    }
    catch (e) {
        valid = false;
    }
    return valid;
}

function isSafePath(obj) {
    if (!obj)
        return false;

    var args = [];
    if (arguments.length > 1)
        args = arguments[1].split(".");

    for (var i = 0; i < args.length; i++) {
        var arrayStartIndex = args[i].indexOf('[');

        if (arrayStartIndex > -1) {
            var arrayEndIndex = args[i].indexOf(']');
            var subArg = args[i].substring(0, arrayStartIndex);
            var subIdx = parseInt(args[i].substring(arrayStartIndex + 1, arrayEndIndex));

            if (!obj || !obj.hasOwnProperty(subArg))
                return false;

            obj = obj[subArg];

            if (obj.length < subIdx)
                return false;

            obj = obj[subIdx];
        }
        else {
            if (!obj || !obj.hasOwnProperty(args[i])) {
                return false;
            }
            obj = obj[args[i]];
        }
    }
    return true;
}

function setTextRelevance(txt, relevance, withBg) {
    var css = "";
    if (withBg != undefined)
        css = "bg-";
    else
        css = "text-";
    css = css + relevance;
    return "<span class='" + css + "'>" + txt + "</span>";
}

function groupBy(collection, property) {
    var i = 0, val, index,
        values = [], result = [];
    for (; i < collection.length; i++) {
        val = collection[i][property];
        if (typeof (val) != 'undefined') {
            index = values.indexOf(val);
            if (index > -1)
                result[index].data.push(collection[i]);
            else {
                values.push(val);
                var tempdata = [];
                tempdata.push(collection[i]);
                var groupedObj = { key: val, data: tempdata };
                result.push(groupedObj);
            }
        }
    }
    return result;
}


function tooltipIni(Class) {
   $('[data-toggle="popover"]').popover({
        placement: 'bottom',
       container: 'body',
       width: 'auto',
        html: true,        
        content: function () {
            return $(this).next(Class).html();
        }
    });
}


function imgErrorImage(image, imageURL, imageUrlAlt) {
    if (typeof imageQueue === 'undefined') {
        imageQueue = [];
    }

    if (imageQueue[image.id] == undefined) {
        imageQueue[image.id] = 1;
    } else {
        imageQueue[image.id]++;
    }

    if (imageQueue[image.id] < 3)    //tres intentos
    {
        setTimeout(function () {
            image.src = imageURL;
        }, (1000 * imageQueue[image.id]));
    }
    else if (imageQueue[image.id] < 6) {
        setTimeout(function () {
            image.src = imageUrlAlt;
        }, (1000 * imageQueue[image.id]));
    }
    else {
        image.onerror = null;
        image.src = "/images/noImage.jpg";
    }
}

function resolveImageURL(objBPI, alternative) {
    var outPut = "/images/noImage.jpg";

    try {
        if (typeof (objBPI) != 'undefined') {
            if (alternative != undefined && alternative) {
                if (isSafePath(objBPI, "mainImage.AltURL.Address")) {
                    outPut = objBPI.mainImage.AltURL.Address.replace('http://', '//');
                } else if (isSafePath(objBPI, "ImageURL") && objBPI.ImageURL != null && objBPI.ImageURL != "") {
                    outPut = objBPI.ImageURL.replace('http://', '//');
                }
            } else {
                if (isSafePath(objBPI, "mainImage.URL.Address")) {
                    outPut = objBPI.mainImage.URL.Address.replace('http://', '//');
                } else if (isSafePath(objBPI, "ImageURL") && objBPI.ImageURL != null && objBPI.ImageURL != "") {
                    outPut = objBPI.ImageURL.replace('http://', '//');
                }
            }
        }
    } catch (e) { }

    return outPut;
}

function removeDiatrics(r) {
    r = r.replace(new RegExp(/[àáâãäå]/g), "a");
    r = r.replace(new RegExp(/[èéêë]/g), "e");
    r = r.replace(new RegExp(/[ìíîï]/g), "i");
    r = r.replace(new RegExp(/ñ/g), "n");
    r = r.replace(new RegExp(/[òóôõö]/g), "o");
    r = r.replace(new RegExp(/[ùúûü]/g), "u");
    r = r.replace(new RegExp(/[ÀÁÂÃÄÅ]/g), "A");
    r = r.replace(new RegExp(/[ÈÉÊË]/g), "E");
    r = r.replace(new RegExp(/[ÌÍÎÏ]/g), "I");
    r = r.replace(new RegExp(/Ñ/g), "N");
    r = r.replace(new RegExp(/[ÒÓÔÕÖ]/g), "O");
    r = r.replace(new RegExp(/[ÙÚÛÜ]/g), "U");
    return r;
}

function checkSession(Process, token, Action, timeOut) {
    if (typeof (timeOut) == "undefined") {
        timeOut = 30000;
    }
    var processTimeOut = window.setInterval(function () {
        $.ajax({
            type: "POST",
            url: "/UtilsB2C.aspx/PingSession",
            contentType: "application/json; charset=utf-8",
            data: '{"Process":"' + Process + '", "Token":"' + token + '"}',
            dataType: "json",
            async: true,
            success: function (res) {
                if (!res.d) {
                    Action();
                    clearInterval(processTimeOut);
                }
            }
        });
    }, timeOut);
}
function msgTimeOut(msg, action) {
    var lnk = "";
    var typeMsg;
    if (action == "back") {
        lnk = " <a href='javascript:window.history.back()'>" + GObjTraduccion.resVolverBusqueda + "</a>";
    }
    if (action == "reload") {
        lnk = " <a href='javascript:location.reload()'>Recargar</a>";
    }
    if (action == "home") {
        lnk = " <a href='/'>Inicio</a>";
    }
    if (action != "" && lnk == "") {
        lnk = " <a href='" + action + "'>IR</a>";
    }
    msg += lnk;
    showPersistMsg(msg, "warning");
}
function showPersistMsg(msg, typeMsg, idMsg) {
    if (typeof (typeMsg) == "undefined" || typeMsg == "")
        typeMsg = "info";
    if (typeof (idMsg) == "undefined" || idMsg != "") {
        var id = " id='" + idMsg + "' ";
    }
    else
        var id = "";
    var elm = $('<div class="alert alert-' + typeMsg + ' alert-dismissible"' + id + 'role="alert" style="position: fixed;bottom: 0px;left: 0;" />');
    var btn = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close" >').append('<span aria-hidden="true">&times;</span >');
    elm.append(btn);
    elm.append("<br /> " + msg);
    $("body").append(elm);
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function showCountDown(minutes, divId, action) {
    showPersistMsg("", "info", divId);
    var countDownDate = moment().add(minutes, 'minutes').valueOf();
    if (GObjTraduccion.resTimeOut) {
        var x = setInterval(function () {
            var now = moment().valueOf();
            var distance = countDownDate - now;
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            document.getElementById(divId).innerHTML = GObjTraduccion.resTimeOut + ": <b>" + minutes + "m " + seconds + "s </b>";
            if (distance < 0) {
                clearInterval(x);
                action();
            }
        }, 1000);
    }
}

function checkLoginState() {
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            console.log(response.authResponse.accessToken);
        }
        console.log(response);
    });
}

function getAirlineInfo(airlineName) {
    if (typeof (GObjCiasAereas) != "undefined") {
        for (var key in GObjCiasAereas) {
            if (GObjCiasAereas.hasOwnProperty(key) && GObjCiasAereas[key].NomCia.toLowerCase() == airlineName.toLowerCase()) {
                return GObjCiasAereas[key];
            }
        }
    }
    return { logoCia: "/logosCias/unknow.png", NomCia: airlineName };
}

function validarCPF(cpf) {
    cpf = cpf.replace(/[^\d]+/g, '');

    if (cpf == '') return false;
    //agregado para validar cpfs repetidos
    if (!validaCamposRepetidos('pasaportePax')) return false;

    // Elimina CPFs invalidos conhecidos
    if (cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999")
        return false;

    // Valida 1o digito
    add = 0;
    for (i = 0; i < 9; i++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(9)))
        return false;

    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return false;

    return true;
}

function getImageBaggageFromSegment(segment) {
    var bagagem = segment.Flights[0].FareOption.BaggageAllowance;
    var FareFamilyName = segment.Flights[0].FareOption.FareFamilyName;
    var cia = segment.Flights[0].CarrierCode;
    bagagem = bagagem.replace(" ", "");
    return getImageBaggage(bagagem, FareFamilyName, cia);
}


function showAlertTime(msg, timeSeg, typeMsg, divId) {
    if (typeof (divId) == "undefined")
        divId = "divAlertTime";
    if (typeof (typeMsg) == "undefined")
        typeMsg = "info";
    showPersistMsg(msg, typeMsg, divId);
    setTimeout(function () {
        $("#" + divId).fadeOut(1000, function () {
            $("#" + divId).remove();
        });
    }, timeSeg);
}

/*facebook API*/
window.fbAsyncInit = function () {
    FB.init({
        appId: '492008921365274',
        xfbml: true,
        version: 'v4.0'
    });
    FB.AppEvents.logPageView();
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function ingresarF() {
    FB.login(function (response) {
        FB.getLoginStatus(function (response) {
            if (response.status == 'connected') {
                FB.api('/me?fields=id,name,email', function (response) {
                    alert('Bienvenido ' + response.name);
                    console.log(JSON.stringify(response));
                   
                });
            } else if (response.status == 'not_authorized') {
                alert('Debes autorizar la app!');
            } else {
                alert('Debes ingresar a tu cuenta de Facebook!');
            }
        });

    }, { scope: 'public_profile, email' });

}

function getCurrentUserInfo() {
    FB.api('/me?fields=id,name,email', function (userInfo) {
        console.log("nombre: " + userInfo.name + 'email: ' + userInfo.email);
    });
}

function getCabinFromClassOfService(flight) {
    var service = $.grep(flight.ClassOfServices, function (e, i) {
        return e.FareBase == flight.FareOption.FareBase;
    });
    return service.length == 0 ? "" : service[0].CabinType;
}
function checkBagageFromSegments(Segments)
{
    var bagages = Segments[0].Flights[0].FareOption.BaggageAllowance;
    var segment = $.grep(Segments, function (s) {
        var flight = $.grep(s.Flights, function (f) {
            return bagages == f.FareOption.BaggageAllowance;
        });
        return s.Flights.length == flight.length;
    });
    return segment.length == Segments.length;
}
function FormateaFechaFormat(Fecha, format) {
    return getObjFecha(Fecha, format).toString("yyyy-MM-dd");
}

function getImageBaggage(bagagem, family, cia) {

    return BagGetInfoWithArray(bagagem, family, cia).join(" ");
}

function BagGetInfoWithArray(bagagem, family, cia) {
    if (bagagem == null) bagagem = "";
    if (family == null || family == undefined) family = "";
    if (cia == null || cia == undefined) cia = "";
    bagagemOri = bagagem;
    bagagem = bagagem.replace(" ", "").toLowerCase();
    var imgBG = [];
    var tittleBag = bagagemOri.replace(" ", "");
    var tittle = GObjTraduccion["resBagagge_" + tittleBag];
    var TieneMaletaMano = true;
    var TieneMaletaCabina = true;
    //equipaje de mano
    if (typeof (GObjParametrosConfig.AEROLINEANOTIENEMALETADEMANO) != "undefined" && GObjParametrosConfig.AEROLINEANOTIENEMALETADEMANO != "" && family == "")
    {
        var aerolineasMano = GObjParametrosConfig.AEROLINEANOTIENEMALETADEMANO.split(",");
        for (var contAerolinea = 0; contAerolinea < aerolineasMano.length; contAerolinea++)
        {
            if (aerolineasMano[contAerolinea] == cia)
            {
                TieneMaletaMano = false;
                break;
            }
        }
    }
    else
    {
        TieneMaletaMano = getBaggageFromFamilyDef(cia, family, "mano");
    }
    if (TieneMaletaMano)
    {
        imgBG.push("<img src='/images/baggage/carteraMano.svg' alt='" + GObjTraduccion.resequipajemano + "' title='" + GObjTraduccion.resequipajemano + "'/>");
    }
    else
    {
        imgBG.push("<img src='/images/baggage/no-carteraMano.svg' alt='" + GObjTraduccion.resNoIncluyeBolsoMano + "' title='" + GObjTraduccion.resNoIncluyeBolsoMano + "'/>");
    }

    //calculo equipaje de cabina
    if (typeof (GObjParametrosConfig.AEROLINEANOTIENEMALETADECABINA) != "undefined" && GObjParametrosConfig.AEROLINEANOTIENEMALETADECABINA != "" && family == "")
    {
        var aerolineasCabina = GObjParametrosConfig.AEROLINEANOTIENEMALETADECABINA.split(",");
        for (var contAerolinea = 0; contAerolinea < aerolineasCabina.length; contAerolinea++)
        {
            if (aerolineasCabina[contAerolinea] == cia)
            {
                TieneMaletaCabina = false;
                break;
            }
        }
        
    }
    else
    {
        if (family == 'ZERO' || family == 'QP' || (cia == 'VH' && (family != 'FF' && bagagem != '1p20k')) || cia == 'VE' || cia == '9R' || cia == 'JA' || (cia == 'UA' && family == 'ECOBASIC') || cia == 'WJ' || cia == 'NK' || (cia == 'Y4' && family == 'BASIC'))
        {
            TieneMaletaCabina = false;
        }
        else
        {
            TieneMaletaCabina = getBaggageFromFamilyDef(cia, family, "cabina");
        }
    }
    if (TieneMaletaCabina)
    {
        imgBG.push("<img src='/images/baggage/bgCabina.svg' alt='" + GObjTraduccion.resequipajecabina + "' title='" + GObjTraduccion.resequipajecabina + "'/>");
    }
    else
    {
        imgBG.push("<img src='/images/baggage/no-bgCabina.svg' alt='" + GObjTraduccion.resnoequipajecabina + "' title='" + GObjTraduccion.resnoequipajecabina + "'/>");
    }
    //calculo equipaje de bodega
    bagagem = bagagem == "" ? "0P23k" : bagagem;
    if (bagagem != 'n/a')
    {
        imgBG.push("<img src='/images/baggage/" + bagagem + ".svg' alt='" + bagagemOri + " 'title='" + tittle + "'/>");
    }
    return imgBG;
}

function getInfoDescriptionBag(bagagem, family, cia, segment) {
    if (bagagem == null && segment != null)
    {
        bagagem = segment.Flights[0].FareOption.BaggageAllowance;
        family = segment.Flights[0].FareOption.FareFamilyName;
        cia = segment.Flights[0].CarrierCode;

    }
    var strImg = BagGetInfoWithArray(bagagem, family, cia);
    var regexpre = /title="([^"]*)"|title='([^']*)'/;

    for (var i = 0; i < strImg.length; i++) {

        var res = regexpre.exec(strImg[i]);
        strImg[i] += "<span>" + " " + res[2] + "</span><br/>";

    }
    return strImg.join("");
}
function redirectModal(ResponseId) {
    document.location = "/reserva_vuelos.aspx?bookingID=" + ResponseId;
}

function RemoveAccents(strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    return strAccentsOut;
}


function getHotelImage(ImageURL) {

    if (ImageURL != null && ImageURL.includes("i.travelapi.com")) {
        array = ImageURL.split('_');
        last = (array.length > 1) ? array[array.length - 1].split('.') : '';

        if (last.length > 1) {
            extension = last[1];
            size = last[0];
            ImageURL = (size != 'z') ? ImageURL.replace('_' + size + '.' + extension, '_z.' + extension) : ImageURL;
        }
        return ImageURL.replace('http://', '//');

    } else {
        return ImageURL.replace('http://', '//');
    }
}
function FuncionCambiosbusquedaAbrir(cont, contParam) {
    $("#" + cont).show();
    $("#" + contParam).hide();
}
function FuncionCambiosbusquedaCerrar(cont, contParam) {
    $("#" + cont).hide();
    $("#" + contParam).show(); 
}

function  initCaptcha() {
    if ($(".g-recaptcha").val() != undefined) {
        grecaptcha.render('g-recaptcha', { 'sitekey': GObjDatosDominio.Google_KeySite });
    }
}
function checkboxformreserva() {
    var compare = document.getElementById('chkfacturar');
    if (compare.checked) {
        $("#nombre_contact").attr("required", true);
        $("#apellido_contact").attr("required", true);
        $("#tipoDocumento_contact").attr("required", true);
        $("#documento_contact").attr("required", true);
        $("#depto_contact").attr("required", true);
        $("#ciudad_contact").attr("required", true);
        $("#address_contact").attr("required", true);
    }
    else {
        $("#nombre_contact").attr("required", false);
        $("#apellido_contact").attr("required", false);
        $("#tipoDocumento_contact").attr("required", false);
        $("#documento_contact").attr("required", false);
        $("#depto_contact").attr("required", false);
        $("#ciudad_contact").attr("required", false);
        $("#address_contact").attr("required", false);
    }
}
function checkboxformreservaV2() {
    var compare = document.getElementById('chkfacturar');
    if (compare.checked) {
        $("#nombre_contact").attr("required", true);
        $("#apellido_contact").attr("required", true);
        $("#tipoDocumento_contact").attr("required", true);
        $("#documento_contact").attr("required", true);
    }
    else {
        $("#nombre_contact").attr("required", false);
        $("#apellido_contact").attr("required", false);
        $("#tipoDocumento_contact").attr("required", false);
        $("#documento_contact").attr("required", false);
    }
}
function getTotalReservaPedidos(pedido) {
    var Total = 0;
    if (pedido.InfoPagos.length > 0) {
        Total = pedido.InfoPagos[0].total;
        switch (pedido.InfoPagos[0].pasarela.toLowerCase())
        {
            case "paybancard":
                Total = formatCurrency(pedido.InfoPagos[0].total);
                Total = Total != "$1" ? Total : formatCurrency(pedido.InfoPagos[0].total);
                return Total;
            default:
                return formatCurrency(Total);
        } 
    }
    else {
        if (pedido.valorTotal > 0) {
            Total = formatCurrency(pedido.valorTotal);
            return Total;
        }
        if (pedido.ReservasHoteles.length > 0) {
            Total = formatCurrency(pedido.ReservasHoteles[0].RESH_VALORVENTA);
        }
        if (pedido.Vuelos.length > 0) {
            if (pedido.Vuelos[0].valorCASH > 0) {
                Total = formatCurrency(pedido.Vuelos[0].valorCASH);
            }
            if (pedido.Vuelos[0].valorTC > 0) {
                Total = formatCurrency(pedido.Vuelos[0].valorTC);
            }
        }
        if (pedido.paquetes.length > 0) {
            Total = formatCurrency(pedido.paquetes[0].rpaq_vrtotal);
        }
        if (pedido.Autos.length > 0) {
            Total = formatCurrency(pedido.Autos[0].rauto_valor);
        }
        if (Total < 1 && pedido.payments.length > 0) {
            Total = formatCurrency(pedido.payments[0].pagop_valorTotal);
        }
    }
    return Total;
}
function changeProduct(product)
{
    switch (product) {
        case "VUELOS":
            product = "Flights";
            break;
        case "HOTELES":
            product = "Hotels";
            break;
        case "PAQUETES":
            product = "Packages";
            break;
    }
    return product;
}
function fillGobjPassengerSearch(product) {
    product = changeProduct(product)
    var totalAdts = 0;
    var totalChds = 0;
    var agesChds = [];
    var agesInfs = [];
    var ages = [];
    var valid = false;
    if ((product != "Flights") && typeof (GObjParametrosHoteles) != "undefined" && GObjParametrosHoteles != null) {
        var habs = typeof (GObjParametrosHoteles) != "undefined" && typeof (GObjParametrosHoteles) != null ? GObjParametrosHoteles.HotelSearchCriteria.Criterion.RoomStayCandidatesType.RoomStayCandidates : 0;
        for (var contHab = 0; contHab < habs.length; contHab++) {
            var adts = 0;
            var chds = 0;
            var agesHab = [];
            for (var contPassengers = 0; contPassengers < habs[contHab].GuestCountsType.GuestCounts.length; contPassengers++) {
                if (contPassengers == 0) {
                    adts = habs[contHab].GuestCountsType.GuestCounts[0].Count;
                    chds = habs[contHab].GuestCountsType.GuestCounts.length - 1;
                }
                if (habs[contHab].GuestCountsType.GuestCounts[contPassengers].Age != null) {
                    agesHab.push(habs[contHab].GuestCountsType.GuestCounts[contPassengers].Age);
                    if (habs[contHab].GuestCountsType.GuestCounts[contPassengers].Age > 2) {
                        agesChds.push(habs[contHab].GuestCountsType.GuestCounts[contPassengers].Age);
                    }
                    else
                    {
                        agesInfs.push(habs[contHab].GuestCountsType.GuestCounts[contPassengers].Age);
                    }
                    ages.push(habs[contHab].GuestCountsType.GuestCounts[contPassengers].Age);
                }
            }
            totalAdts += adts;
            totalChds += chds;
            GobjPassengerSearch.push({
                adt: adts,
                chd: chds,
                ageChd: agesHab,
                hab: habs.length,
                product: product,
                clase: (typeof (GObjParametrosVuelos) != "undefined" && GObjParametrosVuelos != null && typeof(GObjParametrosVuelos.Cabin) != "undefined")? GObjParametrosVuelos.Cabin : ""
            });
        }
        valid = true;
    }
    if (product == "Flights") {
        ages = GObjParametrosVuelos.AgeChild != null ? GObjParametrosVuelos.AgeChild : 0;
        totalAdts = GObjParametrosVuelos.PaxAdults;
        totalChds = GObjParametrosVuelos.AgeChild != null ? GObjParametrosVuelos.AgeChild.length : 0;
        if (ages.length > 0) {
            for (var x = 0; x < ages.length; x++) {
                if (ages[x] > 2) {
                    agesChds.push(ages[x]);
                }
                else {
                    agesInfs.push(ages[x]);
                }
            }
        }
        GobjPassengerSearch.push({
            adt: totalAdts,
            chd: totalChds,
            ageChd: ages,
            hab: 1,
            product: "Flights",
            clase: GObjParametrosVuelos.Cabin != undefined ? GObjParametrosVuelos.Cabin : ""
        });
        valid = true;
    }
    if (valid) {
        GobjPassengersTotal = {
            adts: totalAdts,
            chds: totalChds,
            agesChds: agesChds,
            agesInfs: agesInfs,
            agesTotals: ages,
            total: totalAdts + totalChds,
        };
    }
}

/**
 * Obtiene y muestra un widget de hoteles en promocion por cada id en el array dado.
 * @param {any} idSlides Arreglo con los dentificadores de cada campaña o slide.
 */
function mostrarSlidesPromocionHotel(idSlides = []) {

    if (idSlides.length >= 1) {
        $("sliderPromoHoteles").html("");
        idSlides.forEach(elemento => {
            getSlideHotelConfig(elemento);
        });
    }    
}

/**
 * Obtiene y muestra el widget de hoteles en promocion.
 * @param {any} idSlide Identificador de la campaña o slide.
 */
function getSlideHotelConfig(idSlide) {

    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/slideHotelWS",
        contentType: "application/json; charset=utf-8",
        data: "{ 'slideConfig':" + idSlide + "}",
        dataType: "json",
        async: true,
        success: function (res) {
            if (res == null) {
                return;
            }
            const objSlide = getJSONResultAJAX(res.d);
            setGObjSlidesPromoHoteles(objSlide.SlidesHotel, idSlide);            
            InicializaSlidePromoHoteles("sliderPromoHoteles", "promoHoteles", idSlide);
        }
    });
}

/**
* Obtiene una cadena con formato html con la cantidad de estrellas segun la categoria del hotel. Se utiliza iconos de font awesome
* @param {string} categoria categoria del hotel
* @return {string} Cadena en formato html con los iconos de cantidad de estrellas de hotel
*/
function getEstrellasHotelImgen(categoria = "0") {

    const categoriaAux = parseInt(categoria);
    let estrellas = "";
    if (categoriaAux > 0) {

        for (var i = 1; i <= categoriaAux; i++) {
            estrellas += '<i class="fas fa-star fa-1x"></i>';
        }

        if (categoria.indexOf(".") != -1) {
            estrellas += '<i class="fas fa-star-half-alt fa-1x"></i>';
        }

    } else {
        return estrellas = "";
    }
    return estrellas;
}

/**
* Setea el objeto GObjSlides.PromoHoteles con los datos para enviar al template que muestra los hoteles en oferta.
* @param {object} SlidesHotel Array con los objetos que contienen la informacion del hotel
*/
function setGObjSlidesPromoHoteles(slidesHotel, idSlide) {

    if (GObjSlides.promoHoteles == undefined) {
        GObjSlides.promoHoteles = {};
    }
    GObjSlides.promoHoteles[idSlide] = [];
    slidesHotel.forEach(elemento => {

        GObjSlides.promoHoteles[idSlide].push({
            slide_descripcion: null,
            slide_dinamico: null,
            slide_estado: "A",
            slide_id: 17,
            slide_id1: 17,
            slide_nombre: elemento.Destiny_place,
            slide_tipo: "HOME",
            sslide_estado: "A",
            sslide_id: elemento.Hotel_code,
            sslide_img_ruta: elemento.Image,
            sslide_img_ruta_mobile: null,
            sslide_img_url: elemento.Link,
            sslide_info_color: null,
            sslide_info_texto: elemento.Destiny_place.substring(3, elemento.Destiny_place.length),
            sslide_moneda: elemento.Currency,
            sslide_pack: 4,
            sslide_posValue: "left",
            sslide_precio_color: null,
            sslide_precio_texto: elemento.Price,
            sslide_precio_titulo_color: null,
            sslide_precio_titulo_texto: null,
            sslide_tipo: "base",
            sslide_titulo_color: null,
            sslide_titulo_texto: elemento.Hotel_name,
            urlbase: null,
            categoria: elemento.Category > 0 ? elemento.Category : "0",
            acomodacion: elemento.Acomodation,
            checkin: elemento.Checkin,
            checkout: elemento.Checkout,
            policies: elemento.Policies,
            source: elemento.Source
        });
    });
}

function getBaggageFromFamilyDef(cia, family, item) {
    var tieneMaleta = true;
    if (typeof (family) == "undefined" && family != null) family = "";
    if (typeof (cia) == "undefined" && cia != null) cia = "";
    var faItem = $.grep(familyFlightDefinitions, function (e) {
        return e.Cia == cia && e.Code == family;
    });
    if (faItem != null && faItem.length > 0)
    {
        for (var itemsDescriptions = 0; itemsDescriptions < faItem[0].Descriptions.length; itemsDescriptions++)
        {
            if (item == "mano" && faItem[0].Descriptions[itemsDescriptions].Field == "BaggageHand")
            {
                if (faItem[0].Descriptions[itemsDescriptions].Value < 1)
                {
                    tieneMaleta = false;
                    break;
                }
                break;
            }
            if (item == "cabina" && faItem[0].Descriptions[itemsDescriptions].Field == "BaggageHold")
            {
                if (faItem[0].Descriptions[itemsDescriptions].Value < 1)
                {
                    tieneMaleta = false;
                    break;
                }
                break;
            }
        }
    }
    return tieneMaleta;
}

function estadoTransaccion(polEstado)
{
    var estado = "";
    switch (String(polEstado)) { 
        case "0":
            estado = "Ninguna respuesta";
            break;
        case "1":
            estado = "Transacción aceptada";
            break;
        case "2":
            estado = "Transacción fallida";
            break;
        case "3":
            estado = "Transacción pendiente";
            break;
        case "4":
            estado = "Transacción denegada";
            break;
        case "5":
            estado = "Transacción cancelada";
            break;  
    }
    return estado;
}