﻿(function ($) {
    //#region settings
    var settings = {
        DivTotal: '',
        Producto: '',
        PayTotalBooking: null,
        countActualizaCupones: 0
    };

    //var TotalBooking = { Total: 0, Currency: "", AncilliaryBook: 0, Details: [] };  //total de la reserva

    var methods = {
        init: function (options) {

            settings = $.extend(settings, options);
            return this.each(function () {
                methods.initProcess(this);
                var $this = $(this);
            });
        },

        initProcess: function (obj) {
            methods.drawCoupons(obj);
        },

        drawCoupons: function (obj) {
            $(obj).setTemplate(getTemplateText('coupons'), null, { filter_data: false });
            $(obj).processTemplate({});
            $("#btnInfoCoupon").on("click", function () { $(obj.id).PaymentCoupons("getInfoCoupon") });
        },

        getInfoCoupon: function (cuponCode) {
            if (cuponCode == null) {
                var code = $("#couponCode").val();
            }
            else {
                var code = cuponCode;
            }
            $.ajax({
                type: "POST",
                url: "/UtilsB2C.aspx/getCouponData",
                data: "{code:'" + code + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    var obj = $.evalJSON(res.d);
                    if (!obj.error) {
                        if (GObjCupones != null) {
                            valueBeforeCoupon = GObjCupones.valorAplicar;
                        }
                        GObjCupones = obj.data;
                        // $("#couponMSG").html(GObjTraduccion.resValorCupon + formatNumber(GObjCupones.Balance) + "<br />" + GObjCupones.Policies);
                        GObjCupones.valorAplicar = GObjCupones.CouponValue + valueBeforeCoupon;
                        GObjCupones.valorAplicar = GObjCupones.valorAplicar > TotalBooking.Total ? TotalBooking.Total : GObjCupones.valorAplicar;
                        GObjCupones.Balance = GObjCupones.valorAplicar;

                        methods.actualizaValores();

                        if (GObjCupones.HasCode) {
                            $("#couponSecurityCode").show();
                            $("#txtcouponSecurityCode").attr("required", "required");
                        }

                        var containCoupon = $.grep(couponListValid, function (element) {
                            if (element == GObjTraduccion.resCuponValido.replace("%0", code) + " <br/>")
                                return true;
                        });
                        if (containCoupon.length == 0 && typeof (GObjTraduccion.resCuponValido) != "undefined") {
                            couponListValid.push(GObjTraduccion.resCuponValido.replace("%0", code) + " <br/>");
                        }

                        if (typeof (GObjParametrosConfig.B2C2_CuponAplicarMultiples) != 'undefined' &&
                            parseInt(GObjParametrosConfig.B2C2_CuponAplicarMultiples) == couponListValid.length) {
                            $("#divCoupons").hide();
                        }
                    }
                    else {
                        var containCoupon = $.grep(couponListInvalid, function (element) {
                            if (element == GObjTraduccion.resCuponInvalido.replace("%0", code) + " <br/>"
                                || element == GObjTraduccion.resCuponExpirado.replace("%0", code) + " <br/>"
                                || element == GObjTraduccion.resCuponUsado.replace("%0", code) + " <br/>")
                                return true;
                        });
                        if (containCoupon.length == 0) {
                            methods.checkCuponsInvalidMessages(obj.message, code)
                        }


                    }
                    var stringCoupons = "";
                    for (var i = 0; i < couponListValid.length; i++) {
                        stringCoupons += couponListValid[i];
                    }
                    for (var i = 0; i < couponListInvalid.length; i++) {
                        stringCoupons += couponListInvalid[i];
                    }
                    $("#couponMSG").show();
                    $("#couponMSG").html(stringCoupons);
                    $("#couponCode").val("");
                }
            });

        },
        removerCoupon: function () {
            GObjCupones.valorAplicar = 0;
            GObjCupones.Balance = 0;
            valueBeforeCoupon = 0;
            couponListValid = [];
            couponListInvalid = [];
            $("#couponMSG").html("");
            methods.actualizaValores();
            GObjCupones = null;
            $("#InfoCupon").html("");
            $("#InfoCuponTotalH").hide();
            $("#cuponTipo").html("");
            $("#couponCode").val("");
            $("#divCoupons").show();
            methods.ClearCouponSession();
        },

        actualizaHtmlCupones: function () {
            totalReserva = $.fn.Payment ? $().Payment("getTotalBooking") : getTotalReserva(); // Probar al agregar cupon
            var totalUse = typeof (totalReserva.Total) != "undefined" ? totalReserva.Total : totalReserva.total;
            var currencyUse = typeof (totalReserva.Currency) != "undefined" ? totalReserva.Currency : totalReserva.moneda;
            if (GObjCupones == null)
                return;
            var totalValueCuponApli = totalUse - GObjCupones.valorAplicar;
            var totalReservaCupones = ConvValorMoneda(parseFloat(totalValueCuponApli), currencyUse);
            $("#InfoCupon").setTemplate(getTemplateText('coupon_info'), null, { filter_data: false }).processTemplate({});
            $("#InfoCuponTotalH").show();
            $("#cuponTipo").html("");
            $("#InfoCupon a").on("click", function () { $('').PaymentCoupons('removerCoupon') });
            $("#cuponValor").html("- " + ConvValorMoneda(GObjCupones.valorAplicar, GObjCupones.Currency));
            $("#TotalBooking").html(totalReservaCupones);
            $("#TotalBooking2").html(totalReservaCupones);
            $(".TotalBookingPago").html(totalReservaCupones);
            $(".TotalBookingPagoCambio").html("(" + getCambioMonedaSpecific(totalValueCuponApli, currencyUse, GObjDatosDominio.moneda) + " " + GObjDatosDominio.moneda+")");
            cuponApply = totalUse == 0;
            $("#cuponValue").val(GObjCupones.valorAplicar);
            $('#infocuotas').hide();
        },

        GetCouponSession: function () {
            $.ajax({
                type: "POST",
                url: "/UtilsB2C.aspx/GetCouponSession",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    if (res.d != null) {
                        var TotalValueCoupons = 0;
                        var Balance = 0;
                        var CuponValue = 0;
                        for (var i = 0; i < res.d.length; i++) {
                            TotalValueCoupons += res.d[i].CouponValue;
                            Balance += res.d[i].Balance;
                            GObjCupones = res.d[i];

                        }
                        GObjCupones.CouponValue = TotalValueCoupons;
                        GObjCupones.valorAplicar = TotalValueCoupons;
                        GObjCupones.Balance = Balance;
                        methods.actualizaValores();
                    }
                }
            });
        },

        ClearCouponSession: function () {
            $.ajax({
                type: "POST",
                url: "/UtilsB2C.aspx/CleanCuponSession",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                }
            });
        },

        actualizaValores: function () {
            methods.actualizaTotalesCupones();
            methods.actualizaHtmlCupones();
          },


        checkCuponsInvalidMessages: function (mssg, code) {
            if (mssg == 'Cupon Expired') {
                couponListInvalid.push(GObjTraduccion.resCuponExpirado.replace("%0", code) + " <br/>");
            } else if (mssg == 'Cupon Used'
                || (couponListValid.includes(GObjTraduccion.resCuponValido.replace("%0", code) + " <br/>")
                    && mssg == "Its same coupon to before")) {
                couponListInvalid.push(GObjTraduccion.resCuponUsado.replace("%0", code) + " <br/>");
            }
            else {
                couponListInvalid.push(GObjTraduccion.resCuponInvalido.replace("%0", code) + " <br/>");
            }
        },
        actualizaTotalesCupones: function () {
            GObjTotales = $.grep(GObjTotales, function (o) { return o.Type != "coupons" });
            settings.countActualizaCupones++;
            var totalCoupons = 0;
            var valorCuponAplicable = 0;
            var currency = "";
            if (typeof (GObjCupones) == "array") {
                $.each(GObjCupones, function (i, e) {
                    totalCoupons += e.CouponValue;
                    valorCuponAplicable += e.CouponValue / settings.countActualizaCupones; 
                });
                currency = GObjCupones[0].Currency;
            }
            else {
                currency = GObjCupones.Currency;
                valorCuponAplicable = GObjCupones.valorAplicar / settings.countActualizaCupones; 
                totalCoupons = GObjCupones.valorAplicar; 
            }
            if (GObjCupones != null) {
                GObjTotales.push({
                    Type: "coupons",
                    Total: totalCoupons,
                    Reward: 0,
                    indexPayment: 0,
                    Currency: currency,
                    Gateway: "",
                    PaymentType: "coupons"
                });
                for (var i = 0; i < GObjTotales.length; i++) {
                    if (GObjTotales[i].Type != "coupons" && GObjTotales[i].Total > 0 || GObjTotales[i].Reward > 0) {
                        if (valorCuponAplicable > 0) {
                            if (GObjTotales[i].Total > 0)
                            {
                                GObjTotales[i].Total = GObjTotales[i].Total <= valorCuponAplicable ? 0 : GObjTotales[i].Total - valorCuponAplicable;
                            }
                            if (GObjTotales[i].Reward > 0)
                            {
                                GObjTotales[i].Reward = GObjTotales[i].Reward <= valorCuponAplicable ? 0 : GObjTotales[i].Reward - valorCuponAplicable;
                            }
                        }
                        else {
                            if (settings.PayTotalBooking != null && GObjTotales[i].Reward < 1)
                            {
                                GObjTotales[i].Total = settings.PayTotalBooking.Total;
                            }
                            if (settings.PayTotalBooking != null && GObjTotales[i].Reward > 1)
                            {
                                GObjTotales[i].Reward = settings.PayTotalBooking.Total;
                            }
                        }
                        continue;
                    }
                }
                //si todos son 0
                if ($.grep(GObjTotales, function (o) { return o.Total == 0 }).length == GObjTotales.length) {
                    GObjTotales[0].Total = TotalBooking.Total;
                }
            }
            
        },

    }

    $.fn.PaymentCoupons = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.Payment');
        }
    };

})(jQuery);