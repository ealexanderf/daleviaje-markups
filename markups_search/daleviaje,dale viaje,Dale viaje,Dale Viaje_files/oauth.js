﻿(function ($) {
    var settings = {
        providers: ["MSO", "MSO365", "google", "facebook", "GrupoNacion"],
        providersLogos: ["/Images/OauthLogos/microsoft.png", "/Images/OauthLogos/microsoft.png", "/Images/OauthLogos/google.png", "/Images/OauthLogos/facebook.png", "/Images/OauthLogos/club-nacion-login.png"],
        sSourceEntities: "/UtilsB2C.aspx/getOauthConfigDominio",
        config: [],
        idDiv: "loginOAUTH"
    };
    var methods = {
        init: function (options) {
            settings = $.extend(settings, options);
            $("#" + settings.idDiv).toggle();
            methods.getAgencies();
            
        },
        OATHLogin: function (oauthType)
        {
            if ($("#CmbOauthAgency").val() != null && $("#CmbOauthAgency").val() != "")
            {
                document.location = "/initSession.aspx?Oauth=" + oauthType + "&agency=" + $("#CmbOauthAgency").val();
            }
        },
		renderForm: function ()
		{
			var fieldSet = $("<span class='button'><a class='slidingButton' id='btnLoginOauth'><span>Enterprise Login</span></a></span>");
                $("#xnetLogin .buttonBlock3").prepend(fieldSet);
                //  $("#enterpriseLoginOpt :input").checkboxradio();   //upgrade jqueryUI
                $("#btnLoginOauth").on("click", function (e) { $("#xnetLogin").toggle(); $("#" + settings.idDiv).toggle(); });
                $("#btnLoginXnet").on("click", function (e) { $("#xnetLogin").toggle(); $("#" + settings.idDiv).toggle(); });
                for (var i = 0; i < settings.providers.length; i++) {
                    var aux = $.grep(settings.config, function (t) { return t.tipo.toLowerCase() == settings.providers[i].toLowerCase() }); 
                    if(aux != null && aux.length>0)
                    {
                        $("#OauthProviders").append("<li><img src='" + settings.providersLogos[i] + "' id='" + settings.providers[i] + "' class='OAUTH_logo' /></li>");
                        $("#" + settings.providers[i]).on("click", function (e) { methods.OATHLogin(this.id); })
                    }
                }
                methods.setDefaultLogin();
		},
        getAgencies: function ()
        {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: settings.sSourceEntities,
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    settings.config = res.d;
                    for (var i = 0; settings.config != null && i < settings.config.length; i++) {
                        render = true;
                        $("#CmbOauthAgency").append("<option value='" + settings.config[i].codConfig + "'>" + settings.config[i].label + "</option>");
                    }
                    if (settings.config != null && settings.config.length > 0)
                        methods.renderForm();
                }
            });
        },
        setDefaultLogin: function ()
        {
            var agency = readCookie("OauthAgency");
            if (agency != null)
            {
                $("#xnetLogin").hide();
                $("#" + settings.idDiv).show();
                $("#CmbOauthAgency").val(agency);
            }
        }
    };
    $.fn.LoginOauth = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.LoginOauth');
        }
    };

})(jQuery);
