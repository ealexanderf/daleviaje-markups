﻿var source;
var vector;
var draw, snap; // global so we can remove them later
var poly = false;
var miniBar = {};
var indx;

function loadMap(idDiv, long, lat, i = null, j = 0) {
    $('#' + idDiv).empty();
    long = parseFloat(long.replace(',', '.'));
    lat = parseFloat(lat.replace(',', '.'));

    if (map == null) {
        map = new Array(2);
    }
    map[j] = new ol.Map({
        controls: ol.control.defaults().extend([new ol.control.FullScreen(), new ol.control.ZoomSlider(), new ol.control.LayerSwitcherImage()]),
        target: idDiv.toString(), // the div id
        layers: includedMaps(),
        view: new ol.View({
            center: ol.proj.fromLonLat([long, lat]),
            zoom: 10,
            minZoom: 5,
            maxZoom: 20
        }),
    });
    indx = j;
    if (!(isSafePath(GObjParametrosConfig, "HIDECOORDINATESFILTER") && GObjParametrosConfig.HIDECOORDINATESFILTER.toUpperCase() == "S") && indx == 0) {
        var isAvailavility = document.URL.toLowerCase().includes("hoteles");
        if (isAvailavility) {
            /* Search bar */
            var note = new ol.control.Notification();
            map[indx].addControl(note)

            miniBar.Find = new ol.control.TextButton({
                html: "<i class='fa fa-bullseye' aria-hidden='true'></i>",
                title: GObjTraduccion.resFiltrarCoordenadas,
                handleClick: function (b) {
                    if (poly) {
                        info(null, note.show("<p>" + GObjTraduccion.resBuscando + "... <i class='fa fa-info-circle'></i></p>"));
                        miniBar.Draw.setActive(false);
                        applyCoordinatesFilter(0);
                    } else {
                        info(null, note.show('<p>' + GObjTraduccion.resDescripcionFiltroCoordenadas + ' <i class="fa fa-info-circle"></i></p>'),
                        );
                        miniBar.Draw.setActive(false);
                    }
                },
            });

            var sub2 = new ol.control.Bar({
                toggleOne: true,
                controls: [miniBar.Find]
            });

            miniBar.Draw = new ol.control.Toggle({
                html: "<i class='fa fa-map-marker' aria-hidden='true'></i>",
                title: GObjTraduccion.resDescripcionFiltroCoordenadas,
                autoActivate: false,
                onToggle: function (b) {
                    if (b) {
                        info("Button 1 " + (b ? "activated" : "deactivated"),
                            note.show('<p>' + GObjTraduccion.resDescripcionFiltroCoordenadas + ' <i class="fa fa-info-circle"></i></p>'),
                        );
                        drawShape();
                    } else {
                        cancelSearch(false);
                        info(null, note.show('<p>' + GObjTraduccion.resRemover + ' <i class="fa fa-info-circle"></i></p>'),
                        );
                    }
                },
                // Second level nested control bar
                bar: sub2
            });

            miniBar.Cancel = new ol.control.Toggle({
                html: "<i class='fa fa-times' aria-hidden='true'></i>",
                title: GObjTraduccion.resRemover,
                autoActivate: false,
                onToggle: function (b) {
                    info(null, note.show('<p>' + GObjTraduccion.resRemover + ' <i class="fa fa-info-circle"></i></p>'),
                    );
                    cancelSearch();
                },
            });

            var sub1 = new ol.control.Bar({
                toggleOne: true,
                controls: [miniBar.Draw, miniBar.Cancel]
            });

            miniBar.Search = new ol.control.Toggle({
                html: '<i class="fa fa-search"></i>',
                title: GObjTraduccion.resBuscar,
                // First level nested control bar
                bar: sub1,
                onToggle: function () {
                    info(note.show('<p>' + GObjTraduccion.resBuscar + ' <i class="fa fa-info-circle"></i></p>'));
                    miniBar.Draw.setActive(false);
                }
            });

            var mainbar = new ol.control.Bar({
                controls: [miniBar.Search]
            });
            map[indx].addControl(mainbar);
            // Show info
            function info(i) {
                $("#info").html(i || "");
            }
            /* Search bar */
        }


        //Search place
        // Current selection
        var sLayer = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    stroke: new ol.style.Stroke({
                        color: 'rgb(255,165,0)',
                        width: 3
                    }),
                    fill: new ol.style.Fill({
                        color: 'rgba(255,165,0,.3)'
                    })
                }),
                stroke: new ol.style.Stroke({
                    color: 'rgb(255,165,0)',
                    width: 3
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255,165,0,.3)'
                })
            }),
            displayInLayerSwitcher: false
        });
        map[indx].addLayer(sLayer);

        // Set the search control 
        var search = new ol.control.SearchNominatim(
            {
                reverse: true,
                position: true	// Search, with priority to geo position
            });
        map[indx].addControl(search);

        // Select feature when click on the reference index
        search.on('select', function (e) {	// console.log(e);
            sLayer.getSource().clear();
            // Check if we get a geojson to describe the search
            if (e.search.geojson) {
                var format = new ol.format.GeoJSON();
                var f = format.readFeature(e.search.geojson, { dataProjection: "EPSG:4326", featureProjection: map[indx].getView().getProjection() });
                sLayer.getSource().addFeature(f);
                var view = map[indx].getView();
                var resolution = view.getResolutionForExtent(f.getGeometry().getExtent(), map[indx].getSize());
                var zoom = view.getZoomForResolution(resolution);
                var center = ol.extent.getCenter(f.getGeometry().getExtent());
                //Save coordinates
                mapShape = center;
                // redraw before zoom
                setTimeout(function () {
                    view.animate({
                        center: center,
                        zoom: Math.min(zoom, 14)
                    });
                }, 100);
            }
            else {
                map[indx].getView().animate({
                    center: e.coordinate,
                    zoom: Math.max(map[indx].getView().getZoom(), 14)
                });
                //Save coordinates
                mapShape = e.coordinate;
            }
            //start new search
            if (isAvailavility) {
                applyCoordinatesFilter(0);
                cancelSearch(false);
                miniBar.Draw.setActive(true);
                miniBar.Search.setActive(true);
            }
        });

        //Search place
    }

    // Addfeatures to the cluster
    function addFeatures(nb) {
        var ext = map[j].getView().calculateExtent(map[j].getSize());
        clusterSource.getSource().clear();
        if (i == null) {
            clusterSource.getSource().addFeatures(features);
        } else {
            clusterSource.getSource().addFeatures(features[i]);
        }
    }

    // Style for the clusters
    function getStyle(feature, resolution) {
        var size = feature.get('features').length;
        var style = styleCache[size];
        if (!style) {
            var color = size > 25 ? "192,0,0" : size > 8 ? "255,128,0" : "0,128,0";
            var radius = Math.max(8, Math.min(size * 0.75, 20));
            var dash = 2 * Math.PI * radius / 6;
            var dash = [0, dash, dash, dash, dash, dash, dash];
            style = styleCache[size] = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: radius,
                    stroke: new ol.style.Stroke({
                        color: "rgba(" + color + ",0.5)",
                        width: 15,
                        lineDash: dash,
                        lineCap: "butt"
                    }),
                    fill: new ol.style.Fill({
                        color: "rgba(" + color + ",1)"
                    })
                }),
                text: new ol.style.Text({
                    text: size.toString(),
                    //font: 'bold 12px comic sans ms',
                    //textBaseline: 'top',
                    fill: new ol.style.Fill({
                        color: '#fff'
                    })
                })
            });
        }
        return style;
    }

    // Cluster Source
    var clusterSource = new ol.source.Cluster({
        distance: 20,
        source: new ol.source.Vector()
    });

    // Animated cluster layer
    var clusterLayer = new ol.layer.AnimatedCluster({
        name: 'Cluster',
        source: clusterSource,
        animationDuration: 700,
        // Cluster style
        style: getStyle
    });
    map[j].addLayer(clusterLayer);
    // add 2000 features
    addFeatures(20);

    // Style for selection
    var img = new ol.style.Circle({
        radius: 10,
        stroke: new ol.style.Stroke({
            color: "rgba(0,255,255,1)",
            width: 1
        }),
        fill: new ol.style.Fill({
            color: "rgba(0,255,255,0.3)"
        })
    });
    var style0 = new ol.style.Style({
        image: img
    });
    var style1 = new ol.style.Style({
        image: img,
        // Draw a link beetween points (or not)
        stroke: new ol.style.Stroke({
            color: "#fff",
            width: 1
        })
    });

    // Select interaction to spread cluster out and select features
    var selectCluster = new ol.interaction.SelectCluster({
        // Point radius: to calculate distance between the features
        pointRadius: 14,
        // circleMaxObjects: 40,
        // spiral: false,
        animate: true,
        // Feature style when it springs apart
        featureStyle: function () {
            return [true ? style1 : style0]
        },
        // selectCluster: false,	// disable cluster selection
        // Style to draw cluster when selected
        style: function (f, res) {
            var cluster = f.get('features');
            if (isSafePath(cluster, 'length') && cluster.length > 1) {
                var s = [getStyle(f, res)];
                if ($("#convexhull").prop("checked") && ol.coordinate.convexHull) {
                    var coords = [];
                    for (i = 0; i < cluster.length; i++) coords.push(cluster[i].getGeometry().getFirstCoordinate());
                    var chull = ol.coordinate.convexHull(coords);
                    s.push(new ol.style.Style({
                        stroke: new ol.style.Stroke({ color: "rgba(0,0,192,0.5)", width: 2 }),
                        fill: new ol.style.Fill({ color: "rgba(0,0,192,0.3)" }),
                        geometry: new ol.geom.Polygon([chull]),
                        zIndex: 1
                    }));
                }
                return s;
            } else {
                return [
                    new ol.style.Style({
                        image: new ol.style.Circle({
                            stroke: new ol.style.Stroke({ color: "rgba(0,0,192,0.5)", width: 2 }),
                            fill: new ol.style.Fill({ color: "rgba(0,0,192,0.3)" }),
                            radius: 10
                        })
                    })];
            }
        }
    });
    map[j].addInteraction(selectCluster);

    // Popup overlay
    var popup = new ol.Overlay.Popup(
        {
            popupClass: "black", //"tooltips", "warning" "black" "default", "tips", "shadow",
            closeBox: false,
            // onshow: function(){ console.log("You opened the box"); },
            // onclose: function(){ console.log("You close the box"); },
            positioning: 'auto',
            autoPan: true,
            autoPanAnimation: { duration: 500 }
        });
    map[j].addOverlay(popup);

    // Control Select 
    var select = new ol.interaction.Select({});
    map[j].addInteraction(select);

    // On selected => show/hide popup
    select.getFeatures().on(['add'], function (e) {
        var feature = isSafePath(e, 'element.values_.features.length') ? (e.element.values_.features.length > 1 ? null : e.element.values_.features[0]) : null;
        if (feature) {
            map[j].getView().setCenter(ol.proj.fromLonLat([feature.get('lon'), feature.get('lat')]));
            var content = feature.get('htmlBlock');
            popup.show(feature.getGeometry().getFirstCoordinate(), content);
        }
    });
    select.getFeatures().on(['remove'], function (e) {
        popup.hide();
    })

    // change mouse cursor when over marker
    var target = map[j].getTarget();
    var jTarget = typeof target === "string" ? $("#" + target) : $(target);
    $(map[j].getViewport()).on('mousemove', function (e) {
        var pixel = map[j].getEventPixel(e.originalEvent);
        var hit = map[j].forEachFeatureAtPixel(pixel, function (feature, layer) {
            return true;
        });
        if (hit) {
            jTarget.css("cursor", "pointer");
        } else {
            jTarget.css("cursor", "");
        }
    });
}

function getCoordsMapsGoogle(Lat, Long) {
    Lat = parseFloat(Lat);
    Long = parseFloat(Long);
    var latlng = new google.maps.LatLng(Lat, Long);
    return latlng;
}


function loadMapAvailability(idDiv, lat, long, i = null) {
    $('#' + idDiv).empty();
    lat = lat.replace(',', '.');
    long = long.replace(',', '.');
    loadMap(idDiv, long, lat, i, 1);


    setTimeout(function () { map[1].updateSize(); }, 1000)
    setTimeout(function () { map[1].getOverlays().array_[0].show(features[i].getGeometry().getFirstCoordinate(), features[i].get('htmlBlock')); }, 1500);

}


function getLongitud(hotelStay) {
    if (hotelStay.BasicPropertyInfo.Position != null || (hotelStay.HotelDescriptiveContent != null && hotelStay.HotelDescriptiveContent.HotelInfo != null && hotelStay.HotelDescriptiveContent.HotelInfo.Position != null)) {
        var longitud = hotelStay.BasicPropertyInfo.Position.Longitude != null ? hotelStay.BasicPropertyInfo.Position.Longitude :
            hotelStay.HotelDescriptiveContent.HotelInfo.Position.Longitude != null ? hotelStay.HotelDescriptiveContent.HotelInfo.Position.Longitude :
                '';
        return longitud;
    }
    return '';
}

function getLatitud(hotelStay) {
    if (hotelStay.BasicPropertyInfo.Position != null || (hotelStay.HotelDescriptiveContent != null && hotelStay.HotelDescriptiveContent.HotelInfo != null && hotelStay.HotelDescriptiveContent.HotelInfo.Position != null)) {
        var latitud = hotelStay.BasicPropertyInfo.Position.Latitude != null ? hotelStay.BasicPropertyInfo.Position.Latitude :
            hotelStay.HotelDescriptiveContent.HotelInfo.Position.Latitude != null ? hotelStay.HotelDescriptiveContent.HotelInfo.Position.Latitude :
                '';
        return latitud;
    }
    return '';
}

function LoadHotelsMap() {
    try {
        var Coords = { lat: 0, lng: 0 };

        features = new Array(GObjHoteles.HotelStaysType.HotelStays.length);
        for (var i = 0; i < GObjHoteles.HotelStaysType.HotelStays.length; i++) {
            if (objPathValidate(GObjHoteles.HotelStaysType.HotelStays[i], "HotelStays.BasicPropertyInfo.Position.Latitude") &&
                objPathValidate(GObjHoteles.HotelStaysType.HotelStays[i], "HotelStays.BasicPropertyInfo.Position.Longitude")) {
                Coords = new google.maps.LatLng(GObjHoteles.HotelStaysType.HotelStays[i].BasicPropertyInfo.Position.Latitude, GObjHoteles.HotelStaysType.HotelStays[i].BasicPropertyInfo.Position.Longitude);
                var objHotel = GObjHoteles.HotelStaysType.HotelStays[i];
                getFeaturesHotel(objHotel, i);
            }
        }

        if (objPathValidate(GObjHoteles.HotelStaysType.HotelStays[0], "HotelStays.BasicPropertyInfo.Position.Latitude") &&
            objPathValidate(GObjHoteles.HotelStaysType.HotelStays[0], "HotelStays.BasicPropertyInfo.Position.Longitude")) {
            Coords = new google.maps.LatLng(GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.Position.Latitude, GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.Position.Longitude);
            loadMap('divMapHotels', GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.Position.Longitude, GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.Position.Latitude);
        }

        $('#MapModal').on('shown.bs.modal', function (e) {
            $("#" + ObjIfHotel.divMaps).appendTo("#bigMapHotel");
            $("#" + ObjIfHotel.divMaps).css("width", "550px");
            $("#" + ObjIfHotel.divMaps).css("height", "450px");
            map[0].updateSize();

        });
        $('#MapModal').on('hidden.bs.modal', function (e) {
            $("#" + ObjIfHotel.divMaps).appendTo("#mapContainer");
            $("#" + ObjIfHotel.divMaps).css("width", "380px");
            $("#" + ObjIfHotel.divMaps).css("height", "280px");
            map[0].updateSize();
        });

        if (isSafePath(GObjParametrosHoteles, 'FilterResponse.FilterCoordinates.ApplyFilter') && GObjParametrosHoteles.FilterResponse.FilterCoordinates.ApplyFilter) {
            miniBar.Search.setActive(true);
        }
    }
    catch (e) { }
}

function getFeaturesHotel(objHotel, i, isDetail = false) {
    var lat = objHotel.BasicPropertyInfo.Position.Latitude;
    var lon = objHotel.BasicPropertyInfo.Position.Longitude;
    var hotelName = objHotel.BasicPropertyInfo.HotelRef.HotelName;
    var imageURL = objHotel.BasicPropertyInfo.ImageURL;
    var hotelCode = objHotel.BasicPropertyInfo.HotelRef.HotelCode;
    var htmlBlock = [];
    var urlHotelBotton = '';
    var messageButton = '';

    if (isDetail) {
        urlHotelBotton = 'javascript:ObjIfHotel.getPricing()';
        messageButton = 'Reservar';
    } else {
        urlHotelBotton = ObjIfHotel.withFlight ? ObjIfHotel.getDetailUrl() + "/" + objHotel.BasicPropertyInfo.HotelRef.HotelCode + "/_" : ObjIfHotel.getDetailUrl() + "/" + objHotel.BasicPropertyInfo.HotelRef.HotelCode;
        messageButton = 'Ver Detalle';
    }
    htmlBlock = ['<div style="width:max-content; height:max-content;padding:5px">',
        '<div style="width:max-content; height:max-content;">'/*,
                    '<img src="' + imageURL + '" style="width: 160px; max-height: 100px;padding:5px;border-radius:10px"  onerror="imgErrorHotels(this, "")"><br>'*/];

    htmlBlock = htmlBlock.concat([
        '<h4 style="margin: 0;">' + hotelName + '</h4>']);

    htmlBlock = htmlBlock.concat([
        '<p class="blue-text"><span class="stars">',
        '<i class="fa fa-star' + ((objHotel.BasicPropertyInfo.Award.Rating < 1) ? "-o" : "") + '"></i>',
        '<i class="fa fa-star' + ((objHotel.BasicPropertyInfo.Award.Rating < 2) ? "-o" : "") + '"></i>',
        '<i class="fa fa-star' + ((objHotel.BasicPropertyInfo.Award.Rating < 3) ? "-o" : "") + '"></i>',
        '<i class="fa fa-star' + ((objHotel.BasicPropertyInfo.Award.Rating < 4) ? "-o" : "") + '"></i>',
        '<i class="fa fa-star' + ((objHotel.BasicPropertyInfo.Award.Rating < 5) ? "-o" : "") + '"></i>',
        '</span></p>']);


    htmlBlock = htmlBlock.concat([
        '<a class="btnPrimary btnBlue btn-buscar" href="' + urlHotelBotton + '" target="hotel_' + hotelCode + '">' + messageButton + '</a><br />',
        '</div>',
        '</div>']);

    features[i] = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.fromLonLat([lon, lat])),
        name: hotelName,
        htmlBlock: htmlBlock.join(''),
        lat: lat,
        lon: lon,
    });
}

function includedMaps() {
    var osm = new ol.layer.Tile({
        title: "OSM",
        baseLayer: true,
        source: new ol.source.OSM(),
    });

    //var stamen = new ol.layer.Tile({
    //    title: "Watercolor",
    //    baseLayer: true,
    //    source: new ol.source.Stamen({
    //        layer: 'watercolor'
    //    }),
    //    visible: false,
    //});
    var attrib = '<a href="https://www.google.com/intl/es_ES/help/terms_maps/" target="_blank">Google Maps Terminos de Uso</a>';
    var olgoogle = new ol.layer.Tile({
        title: 'Google',
        baseLayer: true,
        source: new ol.source.XYZ({
            url: 'http://mt1.google.com/vt/lyrs=m@113&hl=en&&x={x}&y={y}&z={z}',
            attributions: attrib,
            attributionsCollapsible: false,
            key: (isSafePath(GObjParametrosConfig, 'Key_GoogleMaps') ? GObjParametros.Key_GoogleMaps : "")
        }),
        visible: false,
    });

    source = new ol.source.Vector();

    var style = new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new ol.style.Stroke({
            color: '#33cc33',
            width: 2,
        }),
        image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
                color: '#ffcc33',
            }),
        }),
    });

    vector = new ol.layer.Vector({
        source: source,
        style: style,
        title: 'Buscador',
        preview: './JQuery/ImgBuscador.png',
        displayInLayerSwitcher: false,
    });

    switch ((isSafePath(GObjParametrosConfig, 'Key_GoogleMaps') && GObjParametrosConfig.Key_GoogleMaps != "") ? 1 : 2) {
        case 1:
            return [osm, olgoogle, vector];
            break;

        default:
            return [osm, vector];
            break;
    }
}

function drawShape() {

    draw = new ol.interaction.Draw({
        source: source,
        type: 'Circle',
        dragPan: true,
        geometryFunction: new ol.interaction.Draw.createRegularPolygon(4),
        //features: function () {
        //    if (mapShape != null && isSafePath(GObjParametrosHoteles, 'FilterResponse.FilterCoordinates.ApplyFilter')) {

        //    }

    });
    function addInteractions() {
        draw = draw;
        map[indx].addInteraction(draw);
        snap = new ol.interaction.Snap({ source: source });
        map[indx].addInteraction(snap);
    }

    draw.on('drawend', function () {
        if (vector.getSource().getFeatures().length > 0) {
            vector.getSource().clear();
        }
        if (mapShape != null) {
            mapShape = null;
        }
        poly = true;
        miniBar.Draw.setActive(true);
    });

    addInteractions();
}

function cancelSearch(cancelbuttton = true) {
    miniBar.Draw.setActive(false);
    miniBar.Cancel.setActive(false);
    if (cancelbuttton) {
        miniBar.Search.setActive(false);
    }

    if (poly) {
        vector.getSource().clear();
        map[indx].removeInteraction(snap);
        map[indx].removeInteraction(draw);
        poly = false;
    }

    if (isSafePath(GObjParametrosHoteles, 'FilterResponse.FilterCoordinates.ApplyFilter') && GObjParametrosHoteles.FilterResponse.FilterCoordinates.ApplyFilter && cancelbuttton) {
        applyCoordinatesFilter(1);
    }
}

function resetMap() {
    map[0] = null;
    source = null;
    vector = null;
    //draw = null;
    //snap = null;
    //poly = false;
    //miniBar = {};
}