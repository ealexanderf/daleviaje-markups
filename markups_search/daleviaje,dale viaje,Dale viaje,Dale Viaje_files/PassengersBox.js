﻿(function ($) {
    var defaults = {
        maxRooms: 4,
        maxADT: 9,
        minAdt: 1,
        maxCHD: 8,
        config: [],
        ageChdMax: 11,
        productBox: "",
        maxAcomodation: 9,
        cantPassDefault: 1
    };

    var methods = {
        init: function (options) {
          
            if ($(this).length > 0) {
                $(this).setTemplate(getTemplateText("sb_passengers_box"), null, { filter_data: false });
                $(this).setParam("verClase", options.productBox.indexOf("Flights") != -1);
                $(this).setParam("cantPassDefault", options.cantPassDefault);
                $(this).processTemplate({});
                settings = $.extend({}, defaults, options);
                methods.setSettings(this, settings);
                methods.initEnvents(this);
                if (this[0].id == "FlightsInfo")
                {
                    methods.hideControlsRoom(this);
                }
            }
        },
        initEnvents: function (obj) {
           
            $(".IfPassInfo", obj[0]).on("click", methods.togglePopover);
            $(".IfClaseSeleccionadaLabel", obj[0]).on("click", methods.togglePopover);
            $(".ifPassAddRoom", obj[0]).on("click", methods.addRoom);
            $(".ifPassRemRoom", obj[0]).on("click", methods.removeRoom);
            $(".ifPassAplicar", obj[0]).on("click", methods.ApplyPassenger);                  
            methods.initEventsPass(obj[0]);
        },
        hideControlsRoom: function (obj)
        {
            $(".IfPassRoomCtrl", obj).hide();
        },
        initEventsPass: function (obj)
        {
            $(".IfPassAdtAdd", obj).unbind("click").on("click", methods.addAdt);
            $(".IfPassAdtRem", obj).unbind("click").on("click", methods.removeAdt);
            $(".IfPassChdAdd", obj).unbind("click").on("click", methods.addChd);
            $(".IfPassChdRem", obj).unbind("click").on("click", methods.removeChd);
        },
        addAdt: function (obj) {
            methods.getSettings(obj);
            var context = $(obj.target).closest(".IfPassRoomAdt");
            var adt = $(".IfPassAdts", context).val();
            if (adt >= 0 && adt <= settings.maxADT) {
                var all = methods.getRoom(context.closest(".IfPassRoom"));
                if ((all.adts + all.chds) < settings.maxAcomodation) {
                    $(".IfPassAdts", context).val(parseInt($(".IfPassAdts", context).val()) + 1);
                }
                else
                {
                    var contextFull = $(obj.target).closest(".IfPassContainer");
                    var msg = methods.getTranslate("B2C_PassengerAlertMaxAcomodation", "No se permiten busquedas de mas de %1 pasajeros.", settings.productBox);
                    msg = msg.replace("%1", settings.maxAcomodation);
                    $("div.IfPassMessage", contextFull).html(methods.msgAlert(msg));
                }
            }
        },
        removeAdt: function (obj) {
            methods.getSettings(obj);
            var context = $(obj.target).closest(".IfPassRoomAdt");
            var adt = parseInt($(".IfPassAdts", context).val()) - 1;
            if (adt >= settings.minAdt) {
                $(".IfPassAdts", context).val(adt);
            }
        },
        addChd: function (obj) {
            methods.getSettings(obj);
            var context = $(obj.target).closest(".IfPassRoomChd");
            var chd = $(".IfPassChds", context).val();
            if (chd >= 0 && chd < settings.maxCHD) {
                var all = methods.getRoom(context.closest(".IfPassRoom"));
                if ((all.adts + all.chds) < settings.maxAcomodation) {
                    $(".IfPassChds", context).val(parseInt($(".IfPassChds", context).val()) + 1);
                    methods.changeAgeChd(obj, chd);
                }
                else
                {
                    var contextFull = $(obj.target).closest(".IfPassContainer");
                    var msg = methods.getTranslate("B2C_PassengerAlertMaxAcomodation", "No se permiten busquedas de mas de %1 pasajeros.", settings.productBox);
                    msg = msg.replace("%1", settings.maxAcomodation);
                    ;
                    $("div.IfPassMessage", contextFull).html(methods.msgAlert(msg));
                }
            }
        },
        msgAlert: function (alertString) {
            return "<div class='IfPassMessageCont alert alert-danger' role='alert'>" + alertString + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span class='alert-Passenger' aria-hidden='true'>&times;</span></button></div>"
        },
        changeAgeChd: function (obj, indexChd, force, objInfo, valid) {
            if (force == undefined)
            {
                methods.getSettings(obj);
                var context = $(obj.target).closest(".IfPassRoom");
                var cantDiv = context.find(".IfPassRoomAges");
            }
            else
            {
                methods.getSettings(obj);
                var context = $(obj).find(".IfPassRoom")[valid].closest(".IfPassRoom");
                var cantDiv = $(context).find(".IfPassRoomAges");
            }
            if (context != null) {
                var ageChdMaxI = settings.ageChdMax == undefined ? defaults.ageChdMax : settings.ageChdMax;
                var productBoxI = settings.productBox == undefined ? objInfo.product : settings.productBox;
                var agesOtp = '<option value="-1">' + methods.getTranslate("resEdad", "Edad", productBoxI) + '</option>';
                for (var i = 0; i <= ageChdMaxI; i++) {
                    if (force && i == objInfo.ageChd[indexChd]) {
                        agesOtp += '<option selected value="' + i + '">' + i + '</option>';
                    }
                    else
                    {
                        agesOtp += '<option value="' + i + '">' + i + '</option>';
                    }
                }
                var transl = methods.getTranslate("B2C_PassengerLabelChdAge", "", productBoxI);
                transl = transl.replace("%1", parseInt(indexChd) + 1);
                var selectHtml = '<div class="col-xs-4"><select class="form-control pull-right select-Passenger">' + agesOtp + '</select></div></div>';
                if (transl.indexOf("%2") != -1) {
                    transl = transl.replace("%2", selectHtml);
                }
                else {
                    transl += selectHtml;
                }
                cantDiv.append(transl);
            }
        },
        getTranslate: function (resourceName, strDefault, productBox) {
            if (typeof (GObjTraduccion[resourceName + "_" + productBox]) != "undefined") {
                return GObjTraduccion[resourceName + "_" + productBox];
            }
            if (typeof (GObjTraduccion[resourceName]) != "undefined") {
                return GObjTraduccion[resourceName];
            }
           
            return strDefault;
        },
        removeChd: function (obj) {
            methods.getSettings(obj);
            var context = $(obj.target).closest(".IfPassRoom");
            var chd = $(".IfPassChds", context).val();
            if (chd > 0 && chd <= settings.maxCHD) {
                $(".IfPassChds", context).val(parseInt($(".IfPassChds", context).val()) - 1);
                methods.removeAgeChd(obj);
            }
        },
        removeAgeChd: function (obj) {
            var context = $(obj.target).closest(".IfPassRoom");
            var cantDiv = context.find(".IfPassRoomAges");
            var contTradu = cantDiv.find(".IfPassAgesCont");
            if (contTradu.length > 0) {
                cantDiv.find(".IfPassAgesCont").last().remove();
            }
            else {
                cantDiv.find("select").last().remove();
            }
        },  
        addRoom: function (obj, force, product) {
            if (force == undefined) {
                methods.getSettings(obj);
                var context = $(obj.target).closest(".IfPassContainer");
                var room = $(".IfPassRoom", context);
                var crl = $("span.ifPassRemRoom", context);
            }
            else {
                methods.getSettings(obj);
                var context = $(obj).find(".IfPassContainer");
                var room = $(".IfPassRoom", context);
                var crl = $("span.ifPassRemRoom", context);
            }
            if (context != null) {
                var maxRoomsI = settings.maxRooms == undefined ? defaults.maxRooms : settings.maxRooms;
                var productBoxI = settings.productBox == undefined ? product : settings.productBox;
                if (room.length < maxRoomsI) {
                    var roomClone = $(room[0]).clone();
                    $(crl.last()).remove();
                    roomClone.find("h4.IfPassRoomTitle").remove();

                    roomClone.prepend("");
                    roomClone.prepend("<div class='IfPassContTitleRom col-xs-12 contMarginPassenger'><h4 class='IfPassRoomTitle col-xs-9 contMarginPassenger title-Hab-Passenger'>" + methods.getTranslate("resHabitacion", "Habitación.", productBoxI) + " " + (room.length + 1) + "</h4> <span class='ifPassRemRoom col-xs-3 span-Delete-Passenger' type='button'>" + methods.getTranslate("resEliminar", "Eliminar.", productBoxI) + "</span></div>");
                    if ($(room[0]).find(".IfPassRoomTitle").length == 0) {
                        $(room[0]).prepend("<div class='col-xs-12 contMarginPassenger'><h4 class='IfPassRoomTitle col-xs-9 contMarginPassenger title-Hab-Passenger'>" + methods.getTranslate("resHabitacion", "Habitación.", productBoxI) + " 1</h4></div>");
                    }
                    $(room.parent()).append(roomClone);
                    methods.initEventsPass(context);
                    methods.addCrlRoom(context);
                }
            }
        },
        removeRoom: function (obj) {
            methods.getSettings(obj);
            var context = $(obj.target).closest(".IfPassContainer");
            var room = $("div.IfPassRoom", context);
            if (room.length > 1) {
                $(room.last()).remove();
                room = $("div.IfPassRoom", context);
                if (room.length > 1) {
                    room.last().find("h4.IfPassRoomTitle").remove();
                    room.last().prepend("<div class='IfPassContTitleRom col-xs-12 contMarginPassenger'><h4 class='IfPassRoomTitle col-xs-9 contMarginPassenger title-Hab-Passenger'>" + methods.getTranslate("resHabitacion", "Habitación.", settings.productBox) + " " + (room.length) + "</h4> <span class='ifPassRemRoom col-xs-3 span-Delete-Passenger' type='button'>" + methods.getTranslate("resEliminar", "Eliminar.", settings.productBox) + "</span></div>");
                    methods.addCrlRoom(context);
                }
            }
        },
        ApplyPassenger: function (obj) {
            methods.getSettings(obj);
            var context = $(obj.target).closest(".IfPassContainer");
            var validator = methods.showInfoPasenger(context);
            if (validator)
            {
                $(".IfPassPopover", context).toggle();
                $("div.IfPassMessage", context).html("");
            }
            
        },
        addCrlRoom: function (obj)
        {
            $(".ifPassRemRoom", obj).on("click", methods.removeRoom);
        },
        showInfoPasenger: function (obj) {
            methods.getSettings(obj);
            var all = methods.getRoomsArray(obj);
            var validator = true;
            if (all.claseLabel.length > 0) {
                $("span.IfClaseSeleccionadaLabel", obj).html('<i class="fa fa-flag"></i>&nbsp;' + all.claseLabel);
            }
            for (var i = 0; i < all.ages.length; i++)
            {
                if (all.ages[i] == "-1")
                {
                    validator = false;
                }
            }
            if (validator) {
                var peopleCant = all.adts + all.chds;                
                $(".IfPassInfo", obj).html("<i class='fa fa-user'></i><span class='IfPassCantInfo'>" + peopleCant + " " + methods.getTranslate(peopleCant > 1 ? "resPersonas" : "resPersona", peopleCant > 1 ? "Personas" : "Persona", settings.productBox) + "</span>"); 
                return true;
            }
            else
            {
                var context = $(obj).closest(".IfPassContainer");
                $("div.IfPassMessage", context).html("<div class='IfPassMessageCont alert alert-danger' role='alert'>" + methods.getTranslate("resValidacionEdadDeNinnoNo", "Por favor digite las edades de los niños", settings.productBox) + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span class'alert-Passenger' aria-hidden='true'>&times;</span></button></div>");
                return false;
            }
        },
        getRoomsArray: function (obj) {
            var room = $("div.IfPassRoom", obj);
            var rooms = room.length;
            var adts = 0;
            var chds = 0;
            var clase = $("select.select-clase", obj).val();
            var claseLabel = $("select.select-clase", obj).find('option:selected').text();
            for (var i = 0; i < room.length; i++)
            {
                adts = adts + parseInt($(room[i]).find("input.IfPassAdts").val());
                chds = chds + parseInt($(room[i]).find("input.IfPassChds").val());
            }
            var ages = [];
            var options = $(".IfPassRoomAges", obj).find("select");
            for (var ii = 0; ii < options.length; ii++)
            {
                ages.push($(options[ii]).val());
            }
            return { adts, chds, ages, rooms, clase, claseLabel };
        },
        getRoom: function (obj) {
            var room = $(obj).closest(".IfPassRoom");
            var adts = 0;
            var chds = 0;

            for (var i = 0; i < room.length; i++) {
                adts = adts + parseInt($(room[i]).find("input.IfPassAdts").val());
                chds = chds + parseInt($(room[i]).find("input.IfPassChds").val());
            }
            var ages = [];
            var options = $(".IfPassRoomAges", obj).find("select");
            for (var ii = 0; ii < options.length; ii++) {
                ages.push($(options[ii]).val());
            }
            return { adts, chds, ages };
        },
        getRoomsArrayForHab: function (obj)
        {
            var room = $("div.IfPassRoom", obj);
            var rooms = room.length;
            var adts = [];
            var chds = [];   
            var clase = $("select.select-clase", obj).val();
            var claseLabel = $("select.select-clase", obj).find('option:selected').text();
            for (var i = 0; i < room.length; i++) {
                adts.push($(room[i]).find("input.IfPassAdts").val());
                chds.push($(room[i]).find("input.IfPassChds").val() != 0 ? ($(room[i]).find("input.IfPassChds").val() +"-") : "0-");
                var options = $(".IfPassRoomAges", $(room[i])).find("select");
                for (var ii = 0; ii < options.length; ii++) {
                    chds.push($(options[ii]).val());
                    if (ii < options.length - 1)
                    {
                        chds.push(";");
                    }
                }
                if (i < room.length - 1) {
                    chds.push(",");
                }
            }
            return { adts, chds, clase, claseLabel };
        },
        getRoomsInfo: function () {
            return methods.getRoomsArray(this); 
        },
        getRoomsInfoDown: function () {
            return methods.getRoomsArrayForHab(this);
        },
        togglePopover: function (obj) {
            var context = $(obj.target).closest(".IfPassContainer");
            $(".IfPassPopover", context).toggle();
        },
        setSettings: function (obj, settings) {
            var context = $(".IfPassContainer",obj);
            $(context).data(settings);
        },
        getSettings: function (obj) {
            var context = $(obj.target).closest(".IfPassContainer");
            settings = $(context).data();
            if (settings == null) {
                settings = $(obj).data();
            }
        },
        getInfoSearch: function (objInfoPassenger) {
            var adts = 0;
            var chds = 0;
            if (objInfoPassenger.length > 0) {
                if (typeof(objInfoPassenger[0].clase) != "undefined" && objInfoPassenger[0].clase.length > 0) {
                    $("select.select-clase", this).val(objInfoPassenger[0].clase);
                    $("span.IfClaseSeleccionadaLabel", this).html('<i class="fa fa-flag"></i>&nbsp;' + $("select.select-clase", this).find('option:selected').text());
                }
                for (var validHab = 1; validHab < objInfoPassenger[0].hab; validHab++) {
                    methods.addRoom(this, true, objInfoPassenger[validHab - 1].product);
                }
                for (var valid = 0; valid < objInfoPassenger[0].hab; valid++) {
                    var contextAdt = $(this).find(".IfPassRoomAdt")[valid];
                    if (contextAdt != undefined) { 
                        $(".IfPassAdts", contextAdt).val(objInfoPassenger[valid].adt);
                        var contextChd = $(this).find(".IfPassRoomChd")[valid];
                        $(".IfPassChds", contextChd).val(objInfoPassenger[valid].chd);
                        for (var x = 0; x < objInfoPassenger[valid].chd; x++) {
                            methods.changeAgeChd(this, x, true, objInfoPassenger[valid], valid);
                        }
                        adts += objInfoPassenger[valid].adt;
                        chds += objInfoPassenger[valid].chd;
                        var contextContainer = $(this).find(".IfPassContainer");
                        var peopleCant = adts + chds;
                        $(".IfPassInfo", contextContainer).html("<i class='fa fa-user'></i><span class='IfPassCantInfo'>" + peopleCant + " " + methods.getTranslate(peopleCant > 1 ? "resPersonas" : "resPersona", peopleCant > 1 ? "Personas" : "Persona", objInfoPassenger[valid].product) + "</span>");
                    }
                }
            }
        },        
    }
    $.fn.IFPassengerBox = function (method) {
        try {
            if (methods[method]) {
               
                    return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
                } else if (typeof method === 'object' || !method) {              
                    return methods.init.apply(this, Array.prototype.slice.call(arguments));
                } else {
                    $.error('Method ' + method + ' does not exist on jQuery.IFPassengerBox');
                    return;
                }
        }
        catch (e) { console.log(e); return; }
    };

})(jQuery);