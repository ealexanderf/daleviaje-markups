﻿function InicializaSeguros(Contenedor, nombrefuncionBusqueda) {
    $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_buscadorSeguros'), null, { filter_data: false });
    $("#" + Contenedor).setParam("funcionBusqueda", nombrefuncionBusqueda);
    $("#" + Contenedor).processTemplate({});
    $("#tabSearch").append('<li><a href="#" class="activo btnbuscadorSeguros">Asistencia en Viajes</a></li>');
    inicializaInputs();    
}


function inicializaInputs() {
    var _numberOfMonths;
    var usasegurospropios = GObjParametrosConfig.USASEGUROSPROPIOS != undefined && GObjParametrosConfig.USASEGUROSPROPIOS=="S";
    if (isMobile.any() != null) {
        _numberOfMonths = 1;
    }
    else {
        _numberOfMonths = 2;
    }
    initCalendar({
        type: 'ui',
        selectorOne: "#dateFromInsurance",
        selectorTwo: "#dateToInsurance",
        numberOfMonths: _numberOfMonths,
        minIntOne: parseInt(GObjParametrosConfig.B2C2_OffsetFechaSeguros),
        minIntTwo: parseInt(GObjParametrosConfig.B2C2_OffsetToFechaSeguros),
        fnChangeOne: function (sender) {
            ChangeDateInsurance();
        },
    });
    if (typeof(GObjParametrosSeguros)!="undefined" && GObjParametrosSeguros != null) {
        $("#selToInsurance").val(getcodeDestination(GObjParametrosSeguros.quoteRQ.IDDestination));
        $("#selAdtInsurance").val(GObjParametrosSeguros.quoteRQ.NumAdults);
        var from = getObjFecha(GObjParametrosSeguros.quoteRQ.DateDeparture, "YYYY-MM-DD");
        var to = getObjFecha(GObjParametrosSeguros.quoteRQ.DateReturn, "YYYY-MM-DD");
        $("#dateFromInsurance").val(from.format("DD-MM-YYYY"));
        $("#dateToInsurance").val(to.format("DD-MM-YYYY"));
        ChangeselectInsurance();
       
    }

    if (!usasegurospropios) {
        $("#selAdtInsurance").change(function () {
            ChangeselectInsurance();
        });

        AutocompleteCountryInsurances("fromInsurance");
    }    

    $("#dateFromInsurance").bind("change", function (e) { onChangeCheckIn(e); });
}

function ChangeselectInsurance() {    
    var cantsel = parseInt($("#selAdtInsurance").val());
    for (var i = 0; i < cantsel; i++) {
        $("#edad_" + (i+1)).show();
    }

    for (var i = cantsel; i < 9; i++) {
        $("#edad_" + (i+1)).hide();
    }
}

function ChangeDateInsurance() {
    if ($("#dateFromInsurance").val() != "" && $("#dateToInsurance").val() != "") {
        var dateTo = moment(moment($("#dateToInsurance").val(), 'DD-MM-YYYY').format('YYYY-MM-DD')).valueOf();
        var dateFrom = moment(moment($("#dateFromInsurance").val(), 'DD-MM-YYYY').format('YYYY-MM-DD')).valueOf();
        var diasI = dateTo - dateFrom;
        var res = Math.ceil(diasI / (1000 * 60 * 60 * 24)) + 1;
        var trad = res == 1 ? GObjTraduccion.resDia : GObjTraduccion.resDias;
        $("#NochesInsu").html(res + " " + trad);

    }   
}

function onChangeCheckIn(e) {
    if ($("#dateToInsurance").length > 0) {
        $("#dateToInsurance").datepicker("option", "minDate", $("#dateFromInsurance").datepicker("getDate"));
    }
}


function getcodeDestination(code) {
        if (code == "03")
            return "mundo";
        if (code == "02")
        return "europa";
    return "suramerica";   //04
}

function BuscarSeguros() {
    var usasegurospropios = GObjParametrosConfig.USASEGUROSPROPIOS != undefined && GObjParametrosConfig.USASEGUROSPROPIOS == "S";
    var Virtual = getVirtualPath("Seguros");
    if (!ValidarFormularioSeguros()) {
        return;
    }
    var filtros = [];
   // document.location = "/Seguros/resultado_seguros.aspx";
    //Seguros/{Origen}/{Destino}/{DateFrom}/{DateTo}/{Adt}/{Family}/{filters}
    var origen = $("#fromInsurancename").val();
    var destino = $("#selToInsurance").val();
    var dateFrom = $("#dateFromInsurance").val();
    var dateTo = $("#dateToInsurance").val();
    var ADT = $("#selAdtInsurance").val();    
    var Age = "";

    var cantsel = parseInt($("#selAdtInsurance").val());

    for (var i = 0; i < cantsel; i++) {

        if ($("#edad_" + (i + 1)).val() != "" && i == 0) {
            Age = $("#edad_" + (i + 1)).val();
        } else {
            Age = Age + "_" + $("#edad_" + (i + 1)).val();
        }
    }


    var filt = filtros.length == 0 ? "_" : filtros.join(";");
    
    if (usasegurospropios) {
        document.location = "/" + Virtual + "/" + origen + "/" + destino + "/" + dateFrom + "/" + dateTo + "/" + ADT + "/_/" + filt;
    } else {
        document.location = "/" + Virtual + "/" + origen + "/" + destino + "/" + dateFrom + "/" + dateTo + "/" + ADT + "/" + Age + "/_/" + filt;
    }    
    
}

function ValidarFormularioSeguros() {
    var usasegurospropios = GObjParametrosConfig.USASEGUROSPROPIOS != undefined && GObjParametrosConfig.USASEGUROSPROPIOS == "S";

    var ok = true;
    if ($("#fromInsurancename").val() == "")
        ok = false;
    if ($("#selToInsurance").val() == "")
        ok =  false;
    if ($("#dateFromInsurance").val() == "")
        ok =  false;
    if ($("#dateToInsurance").val() == "")
        ok =  false;
    if ($("#selAdtInsurance").val() == "")
        ok = false;

    var cantsel = parseInt($("#selAdtInsurance").val());

    if (!usasegurospropios) {
        for (var i = 0; i < cantsel; i++) {
            if ($("#edad_" + (i + 1)).val() == "") {
                ok = false;
            }
        }
    }

    if(!ok)
        Mensaje(GObjTraduccion.resCompleteParametrosBusqueda, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
     return ok;
}

function AutocompleteCountryInsurances(input) {
    try {
        tipo = "MAPR";

        $("#" + input).blur(function () {
            var keyEvent = $.Event("keydown");
            keyEvent.keyCode = $.ui.keyCode.ENTER;
            $(this).trigger(keyEvent);
            return false;
        }).autocomplete({
            minLength: 3,
            autoFocus: true,
            source: function (request, response) {
                tooltipAutoComplete(input, false);
                $.ajax({
                    type: "POST",
                    url: "/UtilsB2C.aspx/autoCompDestinosInsurance",
                    data: "{'q': '" + request.term + "', 'tipo': '" + tipo + "' }",

                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var objPaises = getJSONResultAJAX(data.d);
                        var arrPaises = new Array();
                        $.each(objPaises, function (i, n) {
                            var objPais = null;
                            for (var i = 0; i < arrPaises.length; i++) {
                                if (arrPaises[i].value == n.P.geoCodigos["ISO"].codi_codigo) {
                                    objCity = arrPaises[i];
                                    break;
                                }
                            }
                            if (objPais == null) {
                                objPais = new Object();
                                objPais.value = n.P.gnom_nombre;//n.P.geoCodigos["ISO"].codi_codigo;
                                objPais.label = n.P.gnom_nombre;
                                objPais.name = n.P.gnom_nombre;
                                objPais.code = n.P.geoCodigos["ISO"].codi_codigo;

                                arrPaises[arrPaises.length] = objPais;
                            }

                            objPais = null;
                            for (var i = 0; i < arrPaises.length; i++) {
                                if (arrPaises[i].code == n.P.geoCodigos["ISO"].codi_codigo) {
                                    objPais = arrPaises[i];
                                    break;
                                }
                            }
                            if (objPais == null) {
                                objPais = new Object();
                                objPais.value = n.P.geoCodigos["ISO"].codi_codigo;
                                arrPaises[arrPaises.length] = objPais;
                            }

                            objPais.code = n.P.geoCodigos["ISO"].codi_codigo;
                            objPais.label = n.P.gnom_nombre;
                            objPais.name = n.P.gnom_nombre;                            
                        });

                        response(arrPaises);                        
                    }
                });
            },
            select: function (event, ui) {
                var selectedObj = ui.item;
                if (selectedObj != null) {
                    $("#fromInsurancename").val(selectedObj.code);
                    $("#" + input).val(selectedObj.name);
                }
            }

        });

    }
    catch (e) { console.log(e);}
}
