﻿var GObjCamposFormulario = new Array();
var ObjDatosInfo;
var DynaCodFrm=0;
var DynaFrmDiv="";

function formulario(codFormulario, contenedor, callback) {
    DynaCodFrm = codFormulario;
    DynaFrmDiv = contenedor;
    $("#" + contenedor).setTemplate(getTemplateText('formulario_dinamico'), null, { filter_data: false });
    $("#" + contenedor).processTemplate({});
    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/formularioDin",
        contentType: "application/json; charset=utf-8",
        data: '{"codFormulario":"' + codFormulario + '"}',
        dataType: "json",
        success: function (res) {


            if (typeof (GObjHoteles) != 'undefined') {
                getInfoHotel();
                $("#frmHeaderInfo").setTemplate(getTemplateText('dinafrmheader'), null, { filter_data: false });
                $("#frmHeaderInfo").processTemplate({});
                initCaptcha();
            }

            var objContenido = getJSONResultAJAX(res.d);
            console.log(objContenido);
            Campos(objContenido, contenedor);
            if (typeof (callback) == "function") {
                callback();
            }

            if (objContenido.formularioDin != null) {
                $("#" + Contenedor).html(objContenido.formularioDin.Contenido[0].Contenido);
                if (onTermino != undefined) {
                    onTermino();
                }
            }            

        }
    });
}

function Campos(obj, contenedor) {

    if (typeof (contenedor) == "undefined")
        contenedor = "formularioSolicitud";
    $("#" + contenedor).setTemplate(getTemplateText('dinafrm'), null, { filter_data: false });
    $("#" + contenedor).processTemplate(obj);
    $("#" + contenedor).find(".txtDate").each(function () {
        $(this).datepicker();
       
        $(this).click(function () { $(this).datepicker("open"); });
    });
}



function enviarParametrosCampo() {
   // mostrarMensajeEnviando('planes');
    var labels = {};
    var valida = true;
    $("#" + DynaFrmDiv).find("input").each(function () {
        var name = "";
        if (typeof ($(this).attr('title')) == 'undefined') {
            name = $(this).attr('name');
        } else {
            name = $(this).attr('title');
        }
        labels[$(this).attr('id')] = name;
        if ($(this).attr('required') == "required" && $(this).val() == "")
            valida = false;
    });
    $("#" + DynaFrmDiv).find("textarea").each(function () {
        var name = "";
        if (typeof ($(this).attr('title')) == 'undefined') {
            name = $(this).attr('name');
        } else {
            name = $(this).attr('title');
        }
        labels[$(this).attr('id')] = name;
        if ($(this).attr('required') == "required" && $(this).val() == "")
            valida = false;
    });
    $("#" + DynaFrmDiv).find("select").each(function () {
        var name = "";
        if (typeof ($(this).attr('title')) == 'undefined') {
            name = $(this).attr('name');
        } else {
            name = $(this).attr('title');
        }
        labels[$(this).attr('id')] = name;
        if ($(this).attr('required') == "required" && $(this).val() == "")
            valida = false;
    });

    if (!valida) {
        $('#modalDinamico').hide();
        $('#divMensaje').attr('style', 'position:absolute');
        ocultarMensajeEnviando("faltan campos", "");
        return "";
    }

    var frmReserva = $("#frmRegistro").serialize();
    var JsonReserva = params2json(frmReserva);
    var JsonLabels = JSON.stringify(labels);
    var urlsite = document.location.href;
    var datosInfo = typeof (ObjDatosInfo) != "undefined" ? ObjDatosInfo : {};
    


    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/frmEmail",
        data: '{"form":' + JsonReserva + ', "codFormulario":' + DynaCodFrm + ', "labels":' + JsonLabels + ', "DatosInfo":' + JSON.stringify(datosInfo) + ', "urlsite":"' + urlsite +'"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function (x, t, m) {
            $("body").removeClass("loading");
            var Error = "Unexpected Error";
            if (t === "timeout") {
                Error = "Time Out";
            } else {
                var objError = getJSONResultAJAX(x.responseText);
                Error = objError.Message;
            }

        },
        success: function (res) {
            $('#modalDinamico').hide();
            if (res.d != "") {
                var Error = "";

                try {
                    var respuesta = getJSONResultAJAX(res.d);
                    if (respuesta.Error == '')
                        mensaje = GObjTraduccion["resFrmDinaSaludo"];
                    else
                        mensaje = GObjTraduccion["resFrmDinaError"];
                } catch (err) {
                    mensaje = err;
                }
            }
            if (typeof (GObjHoteles) != 'undefined') {
                $('#CotizaHotel').modal('toggle');
            }
            ocultarMensajeEnviando(mensaje, "");

            $("input[type='text']").val("");
            $("input[type='email']").val("");
            $("input[type='date']").val("");
            $("textarea").val("");
        }


    });
}

function mostrarMensajeEnviando(tipo) {
    $("#mainContent").hide();
    $("#sidebar").hide();

    $("#mainContent_mensaje").setTemplate(getTemplateText('mensaje_reservando_' + tipo), null, { filter_data: false });
    $("#mainContent_mensaje").processTemplate({});
    $("#mainContent_mensaje").show();
}

function ocultarMensajeEnviando(mensaje, Error) {
    if (mensaje != "") {
        $("#mainContent").show();
        $("#sidebar").show();
        $("#mainContent_mensaje").hide();
        Mensaje(mensaje, document.title, "ERROR");
    } else if (Error != "") {
        $("#mainContent_mensaje").setTemplate(getTemplateText('error'), null, { filter_data: false });
        $("#mainContent_mensaje").processTemplate({});
    }
}


function getInfoHotel() {
    var description = "";
    var nameCity = typeof (GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.Address.CityName) != 'undefined' ? GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.Address.CityName : "";
    if (nameCity == "") {
        nameCity = typeof (GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.Address.AddressLine) != 'undefined' ? GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.Address.AddressLine : "";
    }

    if (nameCity == "") {
        nameCity = typeof (GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.Address.CityCode) != 'undefined' ? GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.Address.CityCode : "";
    }

    var HotelStay = GObjHoteles.HotelStaysType.HotelStays[0];
    var roomStay = ObjIfHotel.getRoomStay(HotelStay.RoomStayRPH);
    var sumTotal = 0;

    var selTipoHab = $("input[name^=RoomFareType_" + HotelStay.RoomStayRPH + "_]:checked");
    for (var i = 0; i < selTipoHab.length; i++) {
        var indRoom = parseInt(selTipoHab[i].id.split("_")[2]);
        var RoomID = roomStay.RoomRates[indRoom].RoomID;
        var RPH = i;
        var RatePlanID = roomStay.RoomRates[indRoom].RatePlanID;
        var RatePlanCode = roomStay.RoomRates[indRoom].RatePlanCode;
        var Accommodation = (roomStay.RoomRates[indRoom].AccomodationType != 'undefined' && roomStay.RoomRates[indRoom].AccomodationType != null) ? roomStay.RoomRates[indRoom].AccomodationType : "";


        var RatePlans = $.grep(roomStay.RatePlans,
            function (e) {
                return e.RatePlanCode == RatePlanCode && e.RatePlanID == RatePlanID;
            });
        var RoomTypes = $.grep(roomStay.RoomTypes,
            function (e) {
                return e.RoomID == RoomID;
            });

        var RoomRates = roomStay.RoomRates[indRoom];


        var nameRoom = RoomTypes[0].RoomName;
        var totalRoom = RoomRates.Total.AmountAfterTax;
        var currency = RoomRates.Total.CurrencyCode;
        var totalRoom = ConvValorMoneda(totalRoom, currency);
        sumTotal += RoomRates.Total.AmountAfterTax;
        description += "" + GObjTraduccion.resRoom + " " + (i + 1) + ": " + nameRoom;
    }
    var totalHotel = ConvValorMoneda(sumTotal, GObjHoteles.HotelStaysType.HotelStays[0].Price.CurrencyCode);
    ObjDatosInfo = {
        producto: "H",
        nombre: GObjHoteles.HotelStaysType.HotelStays[0].BasicPropertyInfo.HotelRef.HotelName,
        origen: "",
        destino: nameCity,
        checkin: FormateaFechaLarga(GObjParametrosHoteles.HotelSearchCriteria.Criterion.StayDateRange.Start),
        checkout: FormateaFechaLarga(GObjParametrosHoteles.HotelSearchCriteria.Criterion.StayDateRange.End),
        valorTotal: totalHotel,
        URL: window.location.href,
        Descripcion: description
    };
    
    return ObjDatosInfo;
}