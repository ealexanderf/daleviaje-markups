﻿function InitPage() {
    GCarpetaTemplBuscador = "buscador";
   /*$("#mainContent").setTemplate(getTemplateText("master_home"), null, { filter_data: false });
    $("#mainContent").processTemplate({});
    */
    TabsBusqueda("tabsBusqueda", "tabsbusqueda");
    if (GObjParametrosConfig.B2C2_TieneVuelos == "S") {
        InicializaVuelos("buscadorVuelos", "BuscarVuelosHome");
    }
    if (GObjParametrosConfig.B2C2_TieneVueloHotel == "S") {
        InicializaVueloHotel("buscadorVueloHotel", "BuscarVueloHotelHome");
    }

    if (GObjParametrosConfig.B2C2_TieneHoteles == "S") {
        InicializaHoteles("buscadorHoteles", "BuscarHotelesHome");
    }
    if (GObjParametrosConfig.B2C2_TieneAutos == "S") {
        InicializaAutos("buscadorAutos", "BuscarAutosHome");
    }
    if (GObjParametrosConfig.B2C2_TienePlanesProp == "S") {
        InicializaPlanes("buscadorPlanesProp", "BuscarPlanesHome", 0, ["paquetes_propios"], "Paquetes");
        BuscadorPlan = "Circuitos";
    }
    else {
        BuscadorPlan = "Paquetes";
    }
    if (GObjParametrosConfig.B2C2_TienePlanes == "S" || GObjParametrosConfig.USABUSCADORTEMATICO == "S") {

        var ASources = GObjDatosEntidad.sources;
        if (GObjParametrosConfig.B2C2_TienePlanesProp == "S") {
            ASources = $.grep(GObjDatosEntidad.sources, function (e) {
                return e != "paquetes_propios";
            });
        }
        InicializaPlanes("buscadorPlanes", "BuscarPlanesHome", 0, ASources, BuscadorPlan);
    }

    if (GObjParametrosConfig.B2C2_TieneTransfers == "S") {
        InicializaTransfers("buscadorTransfers", "BuscarTransfersHome");
    }
    if (GObjParametrosConfig.B2C2_TieneTiquetes == "S") {
        InicializaTiquetes("buscadorTiquetes", "BuscarTiquetes");
    }

    if (GObjParametrosConfig.B2C2_TieneSeguros == "S") {
        InicializaSeguros("buscadorSeguros", "BuscarSeguros");
    }

    if (GObjParametrosConfig.B2C2_TieneCalendarVuelos == "S") {
        InicializaCalendar("calendar_flight", "BuscadorCalendar");
    }

    InicializaSlideV2("sliderDestinos-extendido", GObjParametrosConfig.B2C2_slideDestinosHome);
    if (typeof (GObjParametrosConfig.B2C2_slideDestinosHome_2) != "undefined") {
          InicializaSlideV2("sliderDestinos2", GObjParametrosConfig.B2C2_slideDestinosHome_2, "2");
        }
        InicializaSlideV3("bgCustom", GObjParametrosConfig.B2C2_slideVuelosHome);

    if (typeof (GObjParametrosConfig.SLIDEDINAMICO) != "undefined") {
        InicializaSlideDynamic("slideHomeDynamic", GObjParametrosConfig.SLIDEDINAMICO);
    }

    if (typeof (GObjParametrosConfig.SLIDETABLEHOME) != "undefined") {
        InicializaSlideTable("mayores_descuento", GObjParametrosConfig.SLIDETABLEHOME);
    }

    if (typeof (GObjParametrosConfig.SLIDELANDINGHOME) != "undefined") {
        InicializaSlideLanding("slide_Landings", GObjParametrosConfig.SLIDELANDINGHOME);
    }

    if (typeof (GObjParametrosConfig.B2C2_slideBackground) != "undefined") {
        InicializaSlideppalHome("slide_back_home", GObjParametrosConfig.B2C2_slideBackground);
    }
    
    if (typeof(GObjParametrosConfig.B2C2_BackgroundHome) != "undefined" &&  GObjParametrosConfig.B2C2_BackgroundHome != "") {
        $("#bgCustom").addClass("bgCustom");
        $("#bgCustom").css($.evalJSON(GObjParametrosConfig.B2C2_BackgroundHome));
    }
    if (GObjParametrosConfig.B2C2_CalendlyClient != undefined && GObjParametrosConfig.B2C2_CalendlyClientInfo != undefined) {
        if (isMobile.any() == null) {
            Calendly.initBadgeWidget({ url: '' + GObjParametrosConfig.B2C2_CalendlyClient + '', parentElement: document.getElementById('CALENDLYCLIENT'), text: '' + GObjParametrosConfig.B2C2_CalendlyClientInfo + '', color: '#00a2ff', textColor: '#ffffff', branding: true })
        }
    }
    if (typeof (GObjParametrosConfig.B2C2_slideDestinosHome) != "undefined")
        InicializaSlideV2("sliderDestinos", GObjParametrosConfig.B2C2_slideDestinosHome);

    ServiciosDestacadosCloud("destinosPopularesVuelos", "S", "V");
    DestacadosHomeVuelos("destinosList");

    ContenidoHotelesDestacados("HotelesDestacados", 6);

    InicializaSlideV1("slideVuelosHome", GObjParametrosConfig.B2C2_slideVuelosHome, 1);

    if (GObjParametrosConfig.B2C2_SlidesPromoHotel != undefined && GObjParametrosConfig.B2C2_SlidesPromoHotel != "") {
        if (GObjParametrosConfig.B2C2_SlidesPromoHotel.indexOf(",") != -1) {
            mostrarSlidesPromocionHotel(GObjParametrosConfig.B2C2_SlidesPromoHotel.split(","));
        } else {
            mostrarSlidesPromocionHotel([GObjParametrosConfig.B2C2_SlidesPromoHotel]);
        }
    }

}

function BuscarVuelosHome() {

    if (ValidarFormularioVuelos()) {
        BuscarVuelos();
    }
}

function BuscarVueloHotelHome() {
    if (ValidarFormularioVueloHotel()) {
        buscarVuelosHotel();
    }
}

function BuscarAutosHome() {

    if (ValidarFormularioAutos()) {
        BuscarAutos();
    }

}

function BuscarHotelesHome() {

    if (ValidarFormularioHoteles()) {
        BuscarHoteles();
    }
}

function BuscarPlanesHome(IDBuscador) {

    if (ValidarFormularioPlanes(IDBuscador)) {
        BuscarPlanes(IDBuscador);
    }
}

function BuscarTransfersHome() {
    if (ValidarFormularioTransfers()) {
        BuscarTransfers();
    }
}
