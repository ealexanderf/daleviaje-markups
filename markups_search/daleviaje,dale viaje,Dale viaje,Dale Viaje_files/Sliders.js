﻿function InicializaSlideV1(Contenedor, NombreSlider, id) {
    if (document.getElementById(Contenedor) == null)
        return;
    var objSlide = GObjSlides[NombreSlider];
    if (typeof (id) == "undefined")
        id = "";
    $("#" + Contenedor).setTemplate(getTemplateText('sliders_sliderv1'), null, { filter_data: false });
    $("#" + Contenedor).processTemplate(objSlide);
    $('#sliderHomeMain').carousel();
}


function InicializaSlideV2(Contenedor, NombreSlider, id) {
    if (document.getElementById(Contenedor) == null)
        return;
    var objSlide = GObjSlides[NombreSlider];
    if (typeof(id) == "undefined")
        id = "";
    $("#" + Contenedor).setTemplate(getTemplateText('sliders_sliderv2'), null, { filter_data: false });
    $("#" + Contenedor).processTemplate(objSlide);
    //$("#" + Contenedor).carousel();
  //  $("#" + Contenedor).carousel('pause');
}


function InicializaSlideV3(Contenedor, NombreSlider) {
    if (document.getElementById(Contenedor) == null)
        return;
    var objSlide = GObjSlides[NombreSlider];

    $("#" + Contenedor).setTemplate(getTemplateText('sliders_sliderv3'), null, { filter_data: false });
    $("#" + Contenedor).processTemplate(objSlide);
    //$("#" + Contenedor).carousel();   
}

function InicializaSlideDynamic(Contenedor, NombreSlider) {
    
    if (document.getElementById(Contenedor) == null)
        return;

    var objSlide = GObjSlides[NombreSlider];
    $("#" + Contenedor).setTemplate(getTemplateText('sliders_sliderdynamic'), null, { filter_data: false });
    $("#" + Contenedor).processTemplate(objSlide);
    
}


function InicializaSlideTable(Contenedor, NombreSlider) {

    if (document.getElementById(Contenedor) == null)
        return;

    var objSlide = GObjSlides[NombreSlider];
    $("#" + Contenedor).setTemplate(getTemplateText('sliders_table'), null, { filter_data: false });
    $("#" + Contenedor).processTemplate(objSlide);

}

function InicializaSlideLanding(Contenedor, NombreSlider) {

    if (document.getElementById(Contenedor) == null)
        return;

    var objSlide = GObjSlides[NombreSlider];
    $("#" + Contenedor).setTemplate(getTemplateText('sliders_landing'), null, { filter_data: false });
    $("#" + Contenedor).processTemplate(objSlide);

}


function InicializaSlideppalHome(Contenedor, NombreSlider) {

    if (document.getElementById(Contenedor) == null)
        return;

    var objSlide = GObjSlides[NombreSlider];
    $("#" + Contenedor).setTemplate(getTemplateText('sliders_slidermaster'), null, { filter_data: false });
    $("#" + Contenedor).processTemplate(objSlide);

}

/**
 * Inicializa y adiciona el widget Hoteles en promocion al contenedor seleccionado. Adiciona el contenido, sin limpiar el contenedor.
 * @param {any} Contenedor Nombre del elemento contendor del widget.
 * @param {any} NombreSlider Nombre del Slide con los datos en el objeto GObjSlides.
 */
function InicializaSlidePromoHoteles(Contenedor, NombreSlider, idSlide) {

    if (document.getElementById(Contenedor) == null)
        return;

    let html = $("<div />");
    html.setTemplate(getTemplateText('sliders_promohoteles'), null, { filter_data: false })
        .setParam("idSlide", NombreSlider)
        .processTemplate(GObjSlides[NombreSlider][idSlide]).appendTo($("#" + Contenedor));
    $("#" + Contenedor).append(html);
    $("#sliderHomeMainDynamic_" + idSlide).carousel();
}
