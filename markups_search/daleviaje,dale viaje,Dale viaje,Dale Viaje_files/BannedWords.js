﻿//All words in banks, should be lowercase
var wordsbank = ["test", "teste","testes"];
/*var phrasesbank = ["auto posto seculo xx", "testeparceiro", "testa saab", "testemaissamsung",
    "wilmar testoni", "testa conte", "dellasteste", "diferlubteste", "teston j", "adrianotesteobraprim",
    "testesitecielo", "titubroker testefc", "veridiana teston", "testemonitoria", "testeparceirocaixalo",
    "testeboladeouro", "testetecnisabeta", "ctbcteste", "testetelemar", "oisateste", "wickboldteste", "testewickbold",
    "testenovomundo", "scheiderteste", "syngentateste", "testesyngentadbranco", "testebungedbranco", "testeitdbranco",
    "testecallcenteramanc", "teste camed", "testeoiqualidadedbra", "testecamedscruz", "testeclickpremiosdbr",
    "usuariotestenovacom", "testecolgatedbranco", "testebmwdbranco", "testetijbs", "testeamancopadrao",
    "testeoibolaouro", "testeoisupercopadbra", "testeunicobadbranco", "testemegafortdbranco",
    "testevantagens", "testekazekage", "testeparceirotempode", "testeparceiroviverav", "testeparceirocampanh",
    "testeparceiroparceri", "testeparceiroshowdep", "testeparceiropontopa", "testeparceirofundopr", "testeparceiromerchan",
    "testeticampanhaimpul", "testeparceiroastrosd", "testeparceiromontesu", "testeaceleragaroto", "testeparceirohiperpr",
    "testecallcenterprogr", "testeparceiroportald", "testeparceiro premia", "testeastrosdasvendas", "testetiastrosdostran",
    "testecallcentermonte", "testecallcenterviver", "testecallcenterclube", "testetiaceleragaroto", "testecallcenterpremi",
    "testetiprogramasmart", "testeparceiroprogram", "testeparceriasoperac", "testewebpremios", "testejuliana chagas",
    "maria helena mittest", "jonas latin testador"];*/
//banbank 2 se usa en Brazil

function bannedWords(text) {
    text = text.trim();
    var word = text.split(" ");
    var patern = /([a-z]|[á-ú]|[ä-ü-ñ]|[à-ù]|[â-û]|[ã-ũ])\1\1/i;

    for (var i = 0; i < word.length; i++) {
        if (wordsbank.includes(word[i].toLowerCase())) {
            return false;
        }
    }     

    if (patern.test(text.toLowerCase()) || /*phrasesbank.includes(text.toLowerCase()) ||*/ text == "") {
        return false;
    }
    return true;
};

function validateInput(input) {
    if (!bannedWords($(input).val())) {
        $(input).val(""); 
    }
        return true;
};