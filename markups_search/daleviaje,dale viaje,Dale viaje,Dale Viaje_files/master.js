
var GCodEntidad = "";
var gtipotc = "";
var msgErrores = "";
var TotalesTripFinancing = new Object();
var objBtnReservar;

/* IE 8 SUPPORT */
if (!('indexOf' in Array.prototype)) {
    Array.prototype.indexOf = function (find, i /*opt*/) {
        if (i === undefined) i = 0;
        if (i < 0) i += this.length;
        if (i < 0) i = 0;
        for (var n = this.length; i < n; i++)
            if (i in this && this[i] === find)
                return i;
        return -1;
    };
}
if (!('forEach' in Array.prototype)) {
    Array.prototype.forEach = function (action, that /*opt*/) {
        for (var i = 0, n = this.length; i < n; i++)
            if (i in this)
                action.call(that, this[i], i, this);
    };
}
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    }
}

if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        value: function (predicate) {
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }
            var o = Object(this);
            var len = o.length >>> 0;
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }
            var thisArg = arguments[1];

            var k = 0;

            while (k < len) {
                var kValue = o[k];
                if (predicate.call(thisArg, kValue, k, o)) {
                    return kValue;
                }
                k++;
            }
            return undefined;
        }
    });
}

$(document).ready(function () {

    GCodEntidad = "";
    if ((typeof (GObjDatosEntidad) != "undefined") && (GObjDatosEntidad != null)) {
        GCodEntidad = "_" + GObjDatosEntidad.codigo;
    }
    if (typeof (GObjParametrosConfig) != "undefined" && typeof (GObjParametrosConfig.B2C_GoogleAnalyticsCodigo) != "undefined" && typeof (GObjParametrosConfig.B2C_GoogleAnalyticsURL) != "undefined") {
        googleAnalyctics(GObjParametrosConfig.B2C_GoogleAnalyticsCodigo, GObjParametrosConfig.B2C_GoogleAnalyticsURL);
    }
    if (typeof (isMobile) == "undefined") {
        fixIE();
    }  
    welcomeUser();


});

function cargue_pie() {
    $('.btnParamentrosBusqueda').click(function (e) {
        $('.modal').fadeIn();
        e.preventDefault();
    });
    $('.btnCerrar').click(function (e) {
        $('.modal').fadeOut();
        e.preventDefault();
    });
   
    $("#mainMenu").append(ObjIfMenus.RenderMenu(GObjMenus[GObjParametrosConfig.B2C2_MenuPpal]));

    VuelosDestacadosMaster("VuelosDestacados");




    $('.btnAmpliar').addClass('closed');
    $('.masInfoHotel').hide();

    $('.btnAmpliar').click(function (e) {
        $(this).toggleClass('closed');
        $(this).parent().next('.masInfoHotel').slideToggle();
        e.preventDefault();
    });

    $('#hLogout').click(function () {
        LogOut();
        return false;
    });

}

function cargueMaster() {

    if (appcontainer != "S") {
    }
    else {
        $("html").css("overflow", "auto");
        $("body").css("overflow", "auto");
    }

    welcomeUser();

    if (typeof (GObjParametrosConfig.B2C2_BackgroundBody) != "undefined")
        $("#bgCustom").css($.evalJSON(GObjParametrosConfig.B2C2_BackgroundBody));   
}

function Menu(contenedor, nombre, idUL, classUL) {
    
    ObjIfMenus.boostrapDefault = false;
    $("#" + contenedor).append(ObjIfMenus.RenderMenu(GObjMenus[nombre.replace(/ /g, "_")]));
    ObjIfMenus.boostrapDefault = true;

}

function LogOut() {
    toogleWait();
    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/LogOut",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {
            toogleWait();
            GObjDatosUsuario = null;
            if (GObjDatosDominio.convpuntos == "S") {
                if (afterShopPage != "undefined" && afterShopPage != "")
                    window.location = "/endSession.aspx?clear=S&redirect=" + afterShopPage;
                else
                    window.location = GObjParametrosConfig.B2C2_PaginaCerrarSession;
            } else {
                document.location = "/endSession.aspx?clear=S";
            }
        }
    });

}

function DatosLogin() {

    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/DatosLogin",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (res) {

            GObjDatosUsuario = $.evalJSON(res.d);
        }
    });

    return GObjDatosUsuario;
}

function EvalCiaAerea(Cia) {
    var evCia = null;
    try {
        evCia = GObjCiasAereas[Cia];
    }
    catch (err) {
        evCia = null;
    }

    if (evCia == null) {
        evCia = new Object();
        evCia.Cia = Cia;
        evCia.NomCia = "";
        evCia.LogoCia = "";
        evCia.CProm = "";
        evCia.CEcon = "";
        evCia.CFlex = "";
        evCia.CEjec = "";
    }
    return evCia;
}

function RegistrarEmail() {

    if ($("#emailSuscripcion").val() != '') {
        var nombre = $("#emailSuscripcionNombre").val();
        toogleWait();
        $.ajax({
            type: "POST",
            url: "/utilsb2c.aspx/GuardarSuscripcion",
            data: '{"email":"' + $("#emailSuscripcion").val() + '","nombre":"' + nombre +'"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                toogleWait();
                var ObjResGuardar = getJSONResultAJAX(res.d);
                if (ObjResGuardar.Error == "") {
                    Mensaje(GObjTraduccion.resRespuestaSuscripcion, document.title, "INFO");
                    $("#emailSuscripcion").val("");
                }
                else {
                    if (ObjResGuardar.Error == "registrado")
                        Mensaje(GObjTraduccion.resYaRegistrado, document.title, "INFO");
                    else
                        Mensaje(ObjResGuardar.Error, document.title, "INFO");
                }
            }
        });
    }
    else {
        Mensaje(GObjTraduccion.resValidaEmailUsuario, document.title, "INFO");
    }
}

function openurlwindow(link, titulo, ancho, alto, txtBtn) {

    $.get(link, function (data) {
        Mensaje_Boostrap(data, titulo, '', null, null, txtBtn);
    });
}

function opencontwindow(nombreCont, Titulo, ancho, alto) {

    var $ndiv = $('<div id="cont_' + nombreCont + '"/>');
    $('body').append($ndiv);
    $("body").remove("#MSGModal");
    var html = '<div id="MSGModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
    html += ' <div class="modal-dialog" style="z-index: 99999;"><div class="modal-content" style="z-index:99999">';
    html += '<div class="modal-header">';
    html += ' <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
    html += '<h4>' + Titulo + '</h4>'
    html += '</div>';
    html += '<div class="modal-body">';
    html += '<p id="txt_' + nombreCont + '"></p></div>';
    html += '<div class="modal-footer">';
    html += '<span class="btn" data-dismiss="modal" aria-hidden="true">' + GObjTraduccion.resBtnCerrar + '</span>'; // close button
    html += '</div></div></div></div>';
    $("#cont_" + nombreCont).html(html);
    if ($("#MSGModal").length > 0)
        $("#MSGModal").remove();
    ContenidoNombre("txt_" + nombreCont, nombreCont, function () {
        $("#MSGModal").modal();
    });
}

function FrontConvert(valor, moneda) {
    decimales = 0;
    if (typeof (decimales) == "undefined" || isNaN(decimales) || decimales == null) {
        decimales = parseInt(GObjParametrosConfig.B2C2_NumeroDecimales);
    }

    return ConvValorMoneda(valor, moneda, false, decimales);

}

function ConvValorPuntos(Valor, Moneda) {

    var val = 0;

    var vlrCambio = 0;
    if (GObjCambioMoneda[Moneda.toUpperCase() + "-" + GObjDatosDominio.moneda.toUpperCase()] != undefined) {
        vlrCambio = GObjCambioMoneda[Moneda.toUpperCase() + "-" + GObjDatosDominio.moneda.toUpperCase()];
    } else {
        $.ajax({
            type: "POST",
            url: "/UtilsB2C.aspx/getCambioMoneda",
            data: '{"monedaOrigen":"' + Moneda.toUpperCase() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (res) {
                vlrCambio = res.d;
                GObjCambioMoneda[Moneda.toUpperCase() + "-" + GObjDatosDominio.moneda.toUpperCase()] = vlrCambio;
            }
        });
    }
    val = Valor * parseFloat(GObjParametrosConfig.B2C2_FactorConversion.replace(/,/g, '.')) * vlrCambio;
    return val;
}

function ConvValorMoneda(Valor, Moneda, comoNumero, decimales) {
    try {
        if (Valor == null) {
            return Valor == null ? "&nbsp;" : Valor;
        }
        if (typeof (decimales) == "undefined" || isNaN(decimales) || decimales == null) {
            decimales = parseInt(GObjParametrosConfig.B2C2_NumeroDecimales);
        }
        if (typeof (Valor) == "string") {
            Valor = parseFloat(Valor);
        }
        if (typeof (comoNumero) == "undefined") {
            comoNumero = true;
        }
        if (GObjParametrosConfig.B2C2_AutorizaWP == "S") {
            Valor = ConvValorPuntos(Valor, Moneda)
            Moneda = GObjParametrosConfig.B2C2_EtiquetaPuntos;
        }
        else {
            if (GObjParametrosConfig.MONEDAPRESENTACION != undefined && GObjParametrosConfig.MONEDAPRESENTACION.includes(currentPage.split("_")[0])) {
                // currentPage.indexOf("resultado") != -1 || currentPage.indexOf("reserva") != -1) {

                var presentationCurrency = GObjParametrosConfig.MONEDAPRESENTACION.split('|');

                for (var i = 0; i < presentationCurrency.length; i++) {

                    if (presentationCurrency[i].toLowerCase().includes(currentPage.split("_")[0].toLowerCase()) && Moneda.toUpperCase() != presentationCurrency[i].split("_")[1].toUpperCase()) {
                        Valor = getCambioMonedaSpecific(Valor, Moneda, presentationCurrency[i].split("_")[1]);
                        Moneda = presentationCurrency[i].split("_")[1].toUpperCase();
                    }
                }
            }
            else {
                if (Moneda.toUpperCase() != GObjDatosDominio.moneda.toUpperCase()) {
                    Valor = getCambioMoneda(Valor, Moneda);
                    Moneda = GObjDatosDominio.moneda.toUpperCase();
                }
            }
        }
        if (comoNumero) {
            return parseFloat(Valor.toFixed(decimales));
        } else {
            return formatCurrency(Valor.toFixed(decimales), "", Moneda, comoNumero);
        }        
    }
    catch (ex) { }
    return Moneda + " " + Valor;
}

function ConvValorMonedaCambio(Valor, Moneda, comoNumero, decimales) {
    try {
        if (Valor == null) {
            return Valor == null ? "&nbsp;" : Valor;
        }
        if (typeof (decimales) == "undefined" || isNaN(decimales) || decimales == null) {
            decimales = parseInt(GObjParametrosConfig.B2C2_NumeroDecimales);
        }
        if (typeof (Valor) == "string") {
            Valor = parseFloat(Valor);
        }
        if (GObjParametrosConfig.B2C2_AutorizaWP == "S") {
            Valor = ConvValorPuntos(Valor, Moneda)
            Moneda = GObjParametrosConfig.B2C2_EtiquetaPuntos;
        }
        else {
            if (Moneda.toUpperCase() != GObjDatosDominio.monedaCambio.toUpperCase()) {
                Valor = getCambioMoneda(Valor, Moneda);
                Moneda = GObjDatosDominio.moneda.toUpperCase();
            }
        }
        if (comoNumero != undefined && comoNumero == true)
            return Valor;
        else
            return formatCurrency(Valor.toFixed(decimales), "", Moneda,true);
    }
    catch (ex) { }
    return Moneda + " " + Valor;
}

//TODO:  BORRAR ???
function ConvAMonedaDominio(Valor, Moneda, comoNumero) {

    var val = 0;
    if (GObjParametrosConfig.B2C2_AutorizaWP == "S") {

        var vlrCambio = 0;
        if (GObjCambioMoneda[Moneda.toUpperCase() + "-" + GObjDatosDominio.moneda.toUpperCase()] != undefined) {
            vlrCambio = GObjCambioMoneda[Moneda.toUpperCase() + "-" + GObjDatosDominio.moneda.toUpperCase()];
        } else {
            $.ajax({
                type: "POST",
                url: "/UtilsB2C.aspx/getCambioMoneda",
                data: '{"monedaOrigen":"' + Moneda.toUpperCase() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (res) {

                    vlrCambio = res.d;
                    GObjCambioMoneda[Moneda.toUpperCase() + "-" + GObjDatosDominio.moneda.toUpperCase()] = parseFloat(vlrCambio);
                }
            });
        }
        val = Valor * vlrCambio;

    } else {
        val = Valor;
    }
    if (comoNumero == undefined || comoNumero == 0 || comoNumero == false) {
        return GObjDatosDominio.moneda.toUpperCase() + " " + formatNumber(val.toFixed(2));
    } else {
        return val.toFixed(2);
    }
}

function getCambioMoneda(valor, Moneda) {
    var vlrCambio = 0;
    if (GObjCambioMoneda[Moneda.toUpperCase() + "-" + GObjDatosDominio.moneda.toUpperCase()] != undefined) {
        vlrCambio = GObjCambioMoneda[Moneda.toUpperCase() + "-" + GObjDatosDominio.moneda.toUpperCase()];
    } else {
        $.ajax({
            type: "POST",
            url: "/UtilsB2C.aspx/getCambioMoneda",
            data: '{"monedaOrigen":"' + Moneda.toUpperCase() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (res) {
                vlrCambio = res.d;
                GObjCambioMoneda[Moneda.toUpperCase() + "-" + GObjDatosDominio.moneda.toUpperCase()] = parseFloat(vlrCambio);
            }
        });
    }
    val = vlrCambio > 0 ? parseFloat(valor) * vlrCambio : parseFloat(valor) * 1;
    return val;

}

function MostrarLoginWP() {

    //if()
    $("#accessForm").dialog("open");

}

function CerrarLogin() {
    $("#accessForm").dialog("close");
}

function LogInWP(OnDespuesLogin) {

    $("#accessForm").dialog("close");
    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/LogInWP",
        data: '{"EUsuario":"' + $("#UsuarioLogin").val() + '", "Clave":"' + $("#PasswordLogin").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {

            GObjDatosUsuario = $.evalJSON(res.d);

            if (GObjDatosUsuario.Status == "ERR") {

                Mensaje(GObjDatosUsuario.Error, document.title, "INFO");
            } else {
                completaEmail();
                CerrarLogin();
                try {
                    OnDespuesLoginWP();
                } catch (e) { }
            }

        }
    });
}

function clickIdioma(codIdioma) {

    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/clickIdioma",
        data: '{"codIdioma":"' + codIdioma + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {

            location.reload(true);

        }
    });

}


function ConvValorMonedaHistorico(Valor, Moneda, factor, vlrCambio, comoNumero) {

    var res = 0;
    var val = 0;
    var decimales = parseInt(GObjParametrosConfig.B2C2_NumeroDecimales);

    if (GObjDatosEntidad.codCampania != "") {
        val = Valor * factor * vlrCambio;

        res = val.toFixed(decimales) + " " + GObjParametrosConfig.B2C2_EtiquetaPuntos;
    } else {

        res= ConvValorMoneda(Valor, Moneda, comoNumero, decimales)
        /*val = Valor;
        res = Moneda + " " + Valor.toFixed(decimales);*/
    }
    if (comoNumero == undefined) {
        return res;
    } else {
        return parseFloat(val).toFixed(decimales);
    }
}


function getTemplateText(Nombre) {
    if (typeof ("GDebugModeTemplates") != "undefined" && !GDebugModeTemplates) {
        return GObjTemplates[Nombre.toLowerCase()];
    } else {
        var lres = "";
        $.ajax({
            type: "POST",
            url: "/UtilsB2C.aspx/traeTemplateTxt",
            data: '{"Nombre":"' + Nombre + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (res) {
                lres = res.d;
            }
        });
        return lres;
    }
}

function getMenuText(Nombre) {
    if (GObjParametrosConfig[Nombre] != "")
        return GObjMenus[GObjParametrosConfig[Nombre]];
}

function marcarTodosChk(mask, idCheck) {
    GCiasFiltro = new Array();
    if ($('#chkCiasTodos').attr("checked") == 'checked')
        $('[id^="' + mask + '"]').each(function (e) {
            $(this).attr("checked", "checked");
            GCiasFiltro[e] = this.value;
        });
    else
        $('[id^="' + mask + '"]').each(function (e) {

            $(this).removeAttr('checked');
            GCiasFiltro = "";
        });
    if (idCheck && $('#' + idCheck).attr("checked") == 'checked') {
        $('[id^="' + mask + '"]').each(function (e) {
            $(this).attr("checked", "checked");
        });
    }
    AplicarFiltros();
}

function siCHKIguales(mascara) {
    //retorna true si todos los chks de grupo tienen igual estado
    if ($("input[name='" + mascara + "']:checked").length == $("input[name='" + mascara + "']").length || $("input[name='" + mascara + "']:not(:checked)").length == $("input[name='" + mascara + "']").length)
        return true
    else
        return false;
}

//Fix menus debe desaparecer 
function cargaIconosMenu() {
    //activa la pagina Actual
    activaMenu();
    /*  
    $("#mainMenu li a").each(function () { if (this.href.indexOf("vuelos") != -1) $(this).css("background-position", "0 10px") });
    $("#mainMenu li a").each(function () { if (this.href.indexOf("hoteles") != -1) $(this).css("background-position", "0 -16px") });
    $("#mainMenu li a").each(function () { if (this.href.indexOf("planes") != -1) $(this).css("background-position", "0 -41px") });
    $("#mainMenu li a").each(function () { if (this.href.indexOf("Programe") != -1) $(this).css("background-position", "0 -66px") });
    $("#mainMenu li a").each(function () { if (this.href.indexOf("Diferenciais") != -1) $(this).css("background-position", "0 -95px") });
    $("#mainMenu li a").each(function () { if (this.href.indexOf("Atencion") != -1) $(this).css("background-position", "0 -120px") });
  */
}

function activaMenu() {
    $("#mainMenu li a").each(function () {
        if ((this.href.indexOf(currentPage) != -1 && typeof (valContppal) == "undefined") || (typeof (valContppal) != "undefined" && this.href.indexOf(valContppal) != -1))
            $(this).addClass("seleccionado")
    });
}
//fin fix menus

//funciones de JS EXTERNOS
//TODO: meter en plugin o libreria
var jsExternos = 0; //js que quedan por cargar
var colaFns = Array();
function jsExt() {
    if (typeof (GObjParametrosConfig.B2C2_jsAdicionales) != "undefined") {
        var srcs = GObjParametrosConfig.B2C2_jsAdicionales.split(",");
        jsExternos = srcs.length;
        srcs.forEach(function (jss) {
            $.ajax({
                url: jss, dataType: "script", success: function () {
                    jsExternos--;
                    colaExecExt();
                }
            });
        });
    }
}

function colaExecExt(fn) {
    if (typeof (fn) == "function")
        colaFns.push(fn);
    if (jsExternos == 0) {
        colaFns.forEach(function (callback) { callback(); });
        colaFns = Array();
    }
}
// end FUNCIONES de JS externos

function enlazarBotonPromo() {
    var enlace;
    var info = $("#headerSlider").find(".cycle-slide-active").find("a");
    enlace = info[0].href;
    window.location = enlace;
}



function mapeoErrores(obj) {  //solo pse
    if (obj.pol_pasarela == "placetopay" || obj.pol_pasarela == "payu") {
        cad = obj.pol_pasaMotivo;
        if (obj.pol_pasaEstado == "-115")
            cad = "La entidad financiera " + obj.bancoNombre + " no puede ser contactada para iniciar la transacción, por favor seleccione otra o intente más tarde";

        if (obj.pol_pasaEstado == "-116")
            cad = "El monto de la transacción excede los límites establecidos, por favor comuníquese " + GObjTraduccion.resRazonSocial + " con  nuestras líneas de atención al cliente " + GObjTraduccion.resTelefonosContactenos + " o envíenos sus sugerencias al correo electrónico ";

        if (obj.pol_pasaEstado == "-103" || obj.pol_pasaEstado == "-104" || obj.pol_pasaEstado == "-105" || obj.pol_pasaEstado == "-48" || obj.pol_pasaEstado == null)
            cad = "No se pudo crear la transacción, por favor intente más tarde o comuníquese a nuestra línea de atención al cliente " + GObjTraduccion.resTelefonosContactenos + ", o envié sus inquietudes al correo electrónico mail@dominio.com";
        //cad = getEstadoTransaccion(obj.pol_pasaEstado, cad, obj.pol_pasarelaBanco);

        if (obj.pol_pasaEstado == "3")
            cad = "?- Transaccion pendiente. Por favor consulte con su entidad financiera si el debito fue realizado.";
        obj.pol_pasaMotivo = cad;
    }
    return obj;
}

function getEstadoTransaccion(id, msg, banco) {
    if (banco != "") {
        if (id == "1")
            return "Transacción Aprobada";
        if (id == "5" || id=="0")
            return "Transacción Fallida<br />" + msg;
        if (id == "4")
            return "Transacción Rechazada<br />" + msg;
        if (id == "9994")
            return "Transacción pendiente, por favor revisar si el débito fue realizado en el banco.";
    }
    if (id == "2")
        msg = "Transaccion Declinada<br />" + msg;
    if (id == "3")
        msg = "Transaccion Pendiente<br />" + msg
    return msg;
}

function toogleWait(product) {
    /*if (product)
    {
        InicializaSlideV2("slideWait", GObjParametrosConfig.B2C2_slideDestinosHome);
    }
    else
    {
        $("#slideWait").html("");
    }*/
    try {
        if (product == "Vuelo" || product == "VueloH") {
            $(".loading-text").html("<b>" + getInfoGeo(GObjParametrosVuelos.Itineraries[0].IATADeparture).description + "</b><br> <i class='fas fa-arrow-circle-down'></i> <br><b>"
                + getInfoGeo(GObjParametrosVuelos.Itineraries[0].IATAArrival).description + "</b><br> ");
        } else if (product == "Hotel") {
            $(".loading-text").html("<b>" + getInfoGeo(GObjParametrosHoteles.HotelSearchCriteria.Criterion.Address.AddressCode).description + "</b><br>");
        } else if (product == "Pack") {
            $(".loading-text").html("<b>" + GObjParametrosPlanes.NombreCiudad + "</b><br>");
        } else {
            $(".loading-text").html("");
        }
    } catch (e) { }

    if ($('#loading-indicator').css("display") == "none")
        $('#loading-indicator').css("display", "flex");
    else
        $('#loading-indicator').css("display", "none");

}

function toggleFilterSide(side, main) {

    if ($("#" + side).css("display") !== 'none') {
        $("#" + side).css("display", "none");
        $("#" + main).css("display", "block");
        $("#footer").css("display", "block");
        $("#header").css("display", "block");
    }
    else {
        $("#" + side).css("width", "100%");
        $("#" + side).css("display", "block");
        $("#footer").css("display", "none");
        // $("#header").css("display", "none");
    }
}


function getMenuUsuario(nombre) {
    $.ajax({
        type: "POST",
        url: "/UtilsB2C.aspx/HTMLMenuDinamico",
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (res) {
            return res;
        }
    });
}

function getInfoGeo(texto) {
    var info = { description: texto };
    try {
        if (GObjInfoGeo != null) {
            info = GObjInfoGeo[texto];
            info.description = getGeoDescription(info);
        }
    } catch (e) {
        return { description: texto };
    }
    return info;
}

function getGeoDescription(obj) {
    var txt = "";
    var A = (typeof (obj.A) != "undefined") ? obj.A.gnom_nombre + "(" + obj.A.geoCodigos.IATA.codi_codigo + ")" : "";
    if (isSafePath(obj, "A.geoCodigos.IATA.codi_codigo") && isSafePath(obj, "C.geoCodigos.IATA.codi_codigo") && obj.A.geoCodigos.IATA.codi_codigo == obj.C.geoCodigos.IATA.codi_codigo) {
        A = "";
    }
    var C = typeof (obj.C) != "undefined" ? obj.C.gnom_nombre + " " : "";
    var P = typeof (obj.P) != "undefined" ? obj.P.gnom_nombre + " " : "";
    txt = (A != "" ? A + ", " : "") + (C != "" ? C + " - " : "") + P;
    
    return txt;
}

function welcomeUser() {
    var welcome = "";
    if (GObjDatosUsuario != null) {
        welcome = GObjDatosUsuario.Person.FirstName;
        $("#welcomeUser").html(welcome).show();
        $("#welcomeUserDismissible").html(welcome).show();
    }

}

function fixIE() {
    if (typeof (isMobile) == "undefined" || typeof(isMobile.any) == "undefined") {
        isMobile = {};
        isMobile.any = function () { return false; };
    }
}


function getCambioMonedaSpecific(valor, Moneda, monedadestino) {

    var decimales = 0;
    if (typeof valor === 'string') {
        valor = valor.replace("$", "");
    }
    if (valor == null) {
        return Valor == null ? "&nbsp;" : Valor;
    }
    if (typeof (decimales) == "undefined" || isNaN(decimales) || decimales == null) {
        decimales = parseInt(GObjParametrosConfig.B2C2_NumeroDecimales);
    }
    if (typeof (Valor) == "string") {
        valor = parseFloat(Valor);
    }
    if (Moneda == monedadestino) {
        return formatNumber(valor, decimales);
    }

    var vlrCambio = 0;
    if (GObjCambioMoneda[Moneda.toUpperCase() + "-" + monedadestino.toUpperCase()] != undefined) {
        vlrCambio = GObjCambioMoneda[Moneda.toUpperCase() + "-" + monedadestino.toUpperCase()];
    } else {
        $.ajax({
            type: "POST",
            url: "/UtilsB2C.aspx/getCambioMonedaSpecific",
            data: '{"monedaOrigen":"' + Moneda.toUpperCase() + '","monedadestino":"' + monedadestino + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (res) {
                vlrCambio = res.d;
                GObjCambioMoneda[Moneda.toUpperCase() + "-" + monedadestino.toUpperCase()] = parseFloat(vlrCambio);
            }
        });
    }
    val = parseFloat(valor) * vlrCambio;
    return CeilDecimales(val, decimales);
}

function getCurrencyPresentation() {

    if (GObjParametrosConfig.MONEDAPRESENTACION != undefined && GObjParametrosConfig.MONEDAPRESENTACION.includes(currentPage.split("_")[0])) {

        var presentationCurrency = GObjParametrosConfig.MONEDAPRESENTACION.split('|');

        for (var i = 0; i < presentationCurrency.length; i++) {

            if (presentationCurrency[i].toLowerCase().includes(currentPage.split("_")[0].toLowerCase())) {
                return presentationCurrency[i].split("_")[1].toUpperCase();
            }
        }
    } else {
        return GObjDatosDominio.moneda.toUpperCase();
    }

}
function SlideConvertion(divId, InputValue, Value, MinValue, Step) {
    MinValue = MinValue == undefined || MinValue == null ? 0 : MinValue;
    Step = Step == undefined || Step == null ? 1 : Step;
    var factorConv = GObjParametrosConfig.B2C2_FactorConversion;
    var valueConvert = (Value * factorConv);

    $(divId).slider({
        min: MinValue,
        max: valueConvert,
        step: Step,
        value: MinValue,
        slide: function (event, ui) {
            $(InputValue).val(ui.value);
            $("#amountSliderPuntosNovaterra").val(ui.value);
        },
    });
    $("#amountSliderPuntosNovaterra").val($("#sliderPuntosNovaterra").slider("values", MinValue));
}