(function($){
  "use strict";

// Mouse-enter dropdown
$('#navbar li').on("mouseenter", function() {
  $(this).find('ul').first().stop(true, true).delay(350).slideDown(500, 'easeInOutQuad');
});
// Mouse-leave dropdown
$('#navbar li').on("mouseleave", function() {
  $(this).find('ul').first().stop(true, true).delay(100).slideUp(150, 'easeInOutQuad');
});

}(jQuery));

	