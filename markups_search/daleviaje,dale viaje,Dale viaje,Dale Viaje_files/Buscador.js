﻿var GCarpetaTemplBuscador = "Buscador";
var GObjSelHotel = null;
var PlanTematico;
var ListCompaniesRental = [];
var hideMT = false;
var OBJTypeFareCars = null;
var ExcludeDestinationTypes = null;
var countrysDest = [];

function listRsIatasAutocomplete(objIata) {
    var arrIata = new Array();
    var cantItm = 0;
    $.each(objIata, function (i, n) {
        cantItm++;
        if (cantItm > 1) {
            return false;
        }
    });
    $.each(objIata, function (i, n) {
        try {
            var objCity = null;
            if (n.Cities != null) {
                var lastCity = "";
                for (var i = 0; i < n.Cities.length; i++) {
                    objCity = new Object();
                   
                    objCity.coun_code = n.ISOCode;
                    objCity.coun_name = n.Name;
                    objCity.citi_code = n.Cities[i].IATACode;
                    objCity.citi_name = n.Cities[i].Name;
                    objCity.airp_code = n.Cities[i].IATACode;
                    objCity.airp_name = "";
                    objCity.elem_desc = objCity.citi_name.replace() + " (" + n.Cities[i].StateName + " - " + objCity.coun_name + ")";
                    objCity.elem_code = n.Cities[i].IDCity;
                    objCity.clase = "fas fa-building colorBuilding";
                    objCity.type = n.Type;

                    if (n.Cities[i].Airports != null && n.Cities[i].Airports.length > 0) {
                        var objAirport = null;
                        var arrAirport = [];
                        var paddingClass = "";
                        if (n.Cities[i].Airports.length > 1)
                        {
                            paddingClass = " paddSearch";
                        }
                        for (var j = 0; j < n.Cities[i].Airports.length; j++) {
                            objAirport = new Object();                      
                            objAirport.coun_code = n.ISOCode;
                            objAirport.coun_name = n.Name;
                            objAirport.citi_code = n.Cities[i].IATACode;
                            objAirport.citi_name = n.Cities[i].Name;
                            objAirport.airp_code = n.Cities[i].Airports[j].IATACode;
                            objAirport.airp_name = n.Cities[i].Airports[j].Name;
                            objAirport.elem_desc = objAirport.airp_name + " (" + objAirport.airp_code + ") - " + objAirport.citi_name + ", " + objAirport.coun_name;
                            objAirport.elem_code = n.Cities[i].Airports[j].IDAirport;
                            objAirport.clase = "fa fa-plane colorPlane" + paddingClass;
                            objAirport.type = n.Cities[i].Airports[j].Type;
                            arrAirport.push(objAirport);
                        }
                        if (arrAirport.length > 0) {
                            if (arrAirport.length > 1) {
                                arrIata.push(objCity);
                            }
                            for (var j = 0; j < arrAirport.length; j++) {
                                arrIata.push(arrAirport[j]);
                            }
                        }
                    }
                    else if (n.Cities[i].Zones != null && n.Cities[i].Zones.length > 0) {
                        var objZone = null;
                        var arrZone = [];
                        for (var j = 0; j < n.Cities[i].Zones.length; j++) {                            
                            objZone = new Object();
                            objZone.coun_code = n.ISOCode;
                            objZone.coun_name = n.Name;
                            objZone.citi_code = n.Cities[i].IATACode;
                            objZone.citi_name = n.Cities[i].Name;
                            objZone.airp_name = n.Cities[i].Zones[j].Name;
                            objZone.elem_desc = objZone.airp_name + " (" + objZone.citi_code + ") - " + objZone.citi_name + ", " + objZone.coun_name;
                            objZone.elem_code = n.Cities[i].Zones[j].IDZone;
                            objZone.clase = "fa fa-map-marker colorZone";
                            arrZone.push(objZone);
                        }
                        if (arrZone.length > 0) {
                            if (arrZone.length > 1) {
                                arrIata.push(objZone);
                            }
                            for (var j = 0; j < arrZone.length; j++) {
                                arrIata.push(arrZone[j]);
                            }
                        }
                    }
                    else
                        arrIata.push(objCity);
                }
            }
            if (n.Destinations != null) {
                for (var i = 0; i < n.Destinations.length; i++) {
                    if (ExcludeDestinationTypes == null || ExcludeDestinationTypes.indexOf(n.Destinations[i].Type)== -1) {
                        objDest = new Object();
                        objDest.cntr_isocode = n.ISOCode;
                        objDest.citi_name = n.Destinations[i].Name;
                        objDest.dest_iatacode = n.Destinations[i].IATACode;
                        objDest.dest_name = n.Destinations[i].Name;
                        objDest.elem_code = n.Destinations[i].IDDestination;
                        objDest.pare_label = n.Destinations[i].ParentLabel != "" ? "<i>" + n.Destinations[i].ParentLabel + "</i>" : "";
                        objDest.coun_name = n.Destinations[i].ParentName;
                        objDest.type = n.Destinations[i].Type;

                        objDest.elem_desc = objDest.dest_name + " " + n.Destinations[i].ParentLabel + " - " + n.Name;

                        if (objDest.type == 'E' || objDest.type == 'R' || objDest.type == 'C') {
                            objDest.type = 'D';//todo es un destino                                        
                            objDest.clase = "fa fa-building-o colorBuilding";
                        }
                        else if (objDest.type == 'A') {
                            objDest.clase = "fa fa-plane colorPlane";
                        }
                        else if (objDest.type == "D") {
                            objDest.clase = "fa fa-map-marker colorPlane";
                        }
                        else if (objDest.type == "Z")
                            objDest.clase = "fa fa-map-marker";

                        arrIata.push(objDest);
                    }
                }
                arrIata = arrIata.sort(function (a, b) {
                    if (a.type == b.type)
                        return 0;

                    if (a.type > b.type) {
                        return 1;
                    }
                    else {
                        return -1;
                    }
                });

                var dstIdx = 0;
                var typeGuide = "";
                $.each(arrIata, function (i, n) {
                    if (typeGuide != n.type) {
                        dstIdx = 0;
                        n.Idx = dstIdx;
                        typeGuide = n.type;
                    } else {
                        dstIdx++;
                        n.Idx = dstIdx;
                    }
                });
            }

        }
        catch (err) {
        }
    });  
    return arrIata;
}

function AutocompleteIatas(input, Metodo, onSelect) {
    try {
        $("#" + input).blur(function () {
            var keyEvent = $.Event("keydown");
            keyEvent.keyCode = $.ui.keyCode.ENTER;
            $(this).trigger(keyEvent);
            return false;
        }).autocomplete({
            minLength: 3,
            autoFocus: true,
            source: function (request, response) {
                tooltipAutoComplete(input, false);
                $.ajax({
                    type: "POST",
                    url: "/UtilsB2C.aspx/" + Metodo,
                    data: '{"q":"' + request.term + '"}',

                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var iatas = getJSONResultAJAX(data.d);
                        iatas = setAdictionalResults(request.term, iatas, Metodo);
                        setExcludeDestination(Metodo);
                        response(listRsIatasAutocomplete(iatas));
                    }
                });
            },
            select: function (event, ui) {
                if (Metodo == "AutoCompleteIataHotel") {
                    $("#" + input.replace("txt", "")).val(ui.item.elem_code);
                }
                else {
                    if (ui.item.airp_code != "") {
                        $("#" + input.replace("txt", "")).val(ui.item.airp_code + "_" + ui.item.elem_code);
                        if (input == 'txtRetiroVehiculo' || input == 'txtEntregaVehiculo') {
                            getCarsTypeCoverage(ui.item.coun_code, ui.item.airp_code, input.replace("txt", ""), ui.item.elem_desc, ui.item.type);
                        }
                    } else {
                        $("#" + input.replace("txt", "")).val(ui.item.citi_code);
                    }
                }
                event.preventDefault();
                $("#" + input).val(ui.item.elem_desc);

                if (typeof (onSelect) == 'function') {
                    onSelect(ui.item);
                }
                toggleClsIcon(input);
                if (hideMT) {
                    ocultarOpcionMT(ui.item, input);
                }
            }

        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li class='sboxli'>")
                .append("<a class='sboxitem'>" + "<span class='" + item.clase + " sboxIcon'></span> <span class='sboxText'>" + item.elem_desc.replace(new RegExp(this.term, "gi"), '<b>$&</b>') + "</span></a>")
                .appendTo(ul);
        };
        $("#" + input).on("focus", function () {
            if (this.value.length < 3)
                tooltipAutoComplete(input, true);
        });

    }
    catch (e) { }
}

function listRsCiasAutocomplete(data) {
    var ret = [];
    for (var i = 0; i < data.length; i++) {
        var cad = {};
        cad.label = data[i].NomCia + " (" + data[i].Cia + ")";
        cad.value = data[i].Cia;
        cad.icon = data[i].LogoCia;
        ret.push(cad);
    }
    return ret;
}

function autoCompleteAerolineas(id) {
    if (id == undefined)
        id = id = "txtAerolinea";
    $("#" + id).autocomplete({
        minLength: 2,
        source: function (request, response) {
            $.ajax({
                type: "POST",
                url: "/UtilsB2C.aspx/AutoCompleteCias",
                data: '{"q":"' + request.term + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var cias = getJSONResultAJAX(data.d);
                    response(listRsCiasAutocomplete(cias));
                }
            });
        },
        select: function (event, ui) {
            $("#" + event.target.id.replace("txt","")).val(ui.item.value);
            event.preventDefault();
            $("#" + event.target.id).val(ui.item.label);
        },
        autoFocus: true,
        change: function (event, ui) {
            if (ui.item == null) {
                //here is null if entered value is not match in suggestion list
                $(this).val((ui.item ? ui.item.id : ""));
            }
        }
    });
}
//VUELOS
function listRsCiasAutocomplete(data) {
    var ret = [];
    for (var i = 0; i < data.length; i++) {
        var cad = {};
        cad.label = data[i].NomCia + " (" + data[i].Cia + ")";
        cad.value = data[i].Cia;
        ret.push(cad);
    }
    return ret;
}

function InicializaVuelos(Contenedor, nombrefuncionBusqueda) {

    $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_buscadorvuelos'), null, { filter_data: false });
    $("#" + Contenedor).setParam("funcionBusqueda", nombrefuncionBusqueda);
    $("#" + Contenedor).processTemplate({});
    $("#tabSearch").append('<li><a href="#" class="activo btnbuscadorVuelos">Vuelos</a></li>');

    $("#opcionesViajeVuelos").setTemplate(getTemplateText(GCarpetaTemplBuscador + '_idaregreso'), null, { filter_data: false });
    $("#opcionesViajeVuelos").processTemplate({});

    $("#Ida").click(function (e) { clickTipoVuelo("OW"); });
    $("#IdaRegreso").click(function (e) { clickTipoVuelo("RT"); });
    $("#multiTrayecto").click(function (e) { clickTipoVuelo("MT"); });
    $("#btnEliminar").click(function (e) { clickTipoVuelo("DELETE"); });
   // $("#btnPlusHotel").click(function (e) { tooggleFlightplusHotel(Contenedor, nombrefuncionBusqueda); })
    addTrayecto("journeys", GObjTraduccion);
    clickTipoVuelo("RT");
    if (typeof (GObjParametrosConfig.B2C2_AeropDefaultVuelos) != "undefined" && GObjParametrosConfig.B2C2_AeropDefaultVuelos != "") {
        $("#txtOrigen_1").val(GObjParametrosConfig.B2C2_AeropDefaultVuelos);
        $("#Origen_1").val(GObjParametrosConfig.B2C2_IataDefaultVuelos);
        toggleClsIcon("Origen_1");
    }
    if (hideMT) {
        $(".flyControls").hide();
        $(".divFechasFlexibles").hide();
    }
    var cantPassDefaultVuelo = typeof (GObjParametrosConfig.NUMEROADULTOSBUSCADORVUELO) != "undefined" ? GObjParametrosConfig.NUMEROADULTOSBUSCADORVUELO : 1;
    var edadninoVuelo = typeof GObjParametrosConfig.B2C2_EdadesNinosVuelos != "undefined" ? parseInt(GObjParametrosConfig.B2C2_EdadesNinosVuelos) : 12;
    $("#FlightsInfo").IFPassengerBox({ productBox: "Flights", ageChdMax: edadninoVuelo, cantPassDefault: cantPassDefaultVuelo});
    fillFrmBusqVuelos();    
	autoCompleteAerolineas();
    ChangeCalendar();   
}

function fillFrmBusqVuelos() {
    if (GObjParametrosVuelos != null) {
        GobjPassengerSearch = [];
        fillGobjPassengerSearch("Flights");
        $("#FlightsInfo").IFPassengerBox("getInfoSearch", GobjPassengerSearch);
        var TipoVuelo = getTripType();
        if (TipoVuelo == "MT") {
            $("#chkOWRT").attr("checked", true);
            $("#journeyBack").hide();
			$("#btnEliminar").show();
        }
        if (TipoVuelo == "OW") {
            $("#journeyBack").hide();
            $("#chkOWRT").attr("checked", true);
            $("#dateJourney_back").html("");
        }
        for (var i = 0; i < GObjParametrosVuelos.Itineraries.length; i++) {
            if (TipoVuelo == "MT" && i > 0) {
                addTrayecto("journeys", GObjTraduccion);
            }
            $("#Origen_" + (i + 1)).val(GObjParametrosVuelos.Itineraries[i].IATADeparture);
            $("#txtOrigen_" + (i + 1)).val(getInfoGeo(GObjParametrosVuelos.Itineraries[i].IATADeparture).description);
            $("#txtDestino_" + (i + 1)).val(getInfoGeo(GObjParametrosVuelos.Itineraries[i].IATAArrival).description);
            $("#Destino_" + (i + 1)).val(GObjParametrosVuelos.Itineraries[i].IATAArrival);
            $("#dateJourney_" + (i + 1)).val(FormateaFecha(GObjParametrosVuelos.Itineraries[i].DateDeparture));
            toggleClsIcon("Origen_" + (i + 1));
            toggleClsIcon("Destino_" + (i + 1));
            AutocompleteIatas("txtOrigen_" + (i + 1), "AutoCompleteIataAirp");
            AutocompleteIatas("txtDestino_" + (i + 1), "AutoCompleteIataAirp");
            if (TipoVuelo == "MT") {
                removeEventsCalendar(i + 1);
                initInputOWRT("MT", (i + 1));
            }
            if (TipoVuelo == "OW") {
                removeEventsCalendar(i + 1);
                initInputOWRT("MT", (i + 1));
            }
            if (TipoVuelo == "RT") {
                $("#chkOWRT").attr("checked", false);
                $("#dateJourney_back").val(FormateaFecha(GObjParametrosVuelos.Itineraries[1].DateDeparture));
                break;
            }
            if (hideMT && i < 2) {
                ocultarOpcionMT({ "coun_code": GObjParametrosVuelos.Trayectos[i].isoPaisOrigen }, "txtOrigen_" + (i + 1));
                ocultarOpcionMT({ "coun_code": GObjParametrosVuelos.Trayectos[i].isoPaisDestino }, "txtDestino_" + (i + 1));
            }
        }

    }
}

function getTripType() {
    var cad = "MT";
    if (GObjParametrosVuelos.Itineraries.length == 2 && GObjParametrosVuelos.Itineraries[0].IATAArrival == GObjParametrosVuelos.Itineraries[1].IATADeparture)
        cad = "RT";
    if (GObjParametrosVuelos.Itineraries.length == 1)
        cad = "OW";
    return cad;
}

function clickTipoVuelo(TipoVuelo) {
    var idNum = 1;
    if (TipoVuelo == "") {
        TipoVuelo = $("#chkOWRT").is(':checked') > 0 ? "OW" : "RT";
    }
    switch (TipoVuelo) {
        case "OW":

        case "RT":
            $("#titleTramo1").hide();
            $("#btnEliminar").hide();
            if (TipoVuelo == "OW") {
                $("#journeyBack").hide();
                $("#dateJourney_back").html("");
            }
            else {
                $("#journeyBack").show();
                $("#btnPlusHotel").show();
            }
            if (removeOthers(1, "journey_1"))
            {
                removeEventsCalendar(1);
                initInputOWRT("RT", 1);
                return;
            }
            else
            {
                removeEventsCalendar(idNum);
                initInputOWRT("RT");
            }
            break;

        case "MT":
            idNum = addTrayecto("journeys", GObjTraduccion);
            if (idNum == 2) {
                $("#dateJourney_1").data("dateRangePicker").destroy();
                $("#titleTramo1").show();
                $("#btnEliminar").show();
                $("#journeyBack").hide();
                $("#btnPlusHotel").hide();
                $("#dateJourney_back").html("");
            }
            break;

        case "DELETE":
            if ($(".trayectoInputs").length > 1) {
                removeOthers(($(".trayectoInputs").length - 1), "journey_1");
            }
            if ($(".trayectoInputs").length == 1) {
                $("#titleTramo1").hide();
                $("#btnEliminar").hide();
                $("#btnPlusHotel").show();
                $("#divOwRt").show();
                $("#chkOWRT").attr("checked", false);
                $("#journeyBack").show();
                initInputOWRT("RT");              
            }
            return;
            break;

    }
    iniciaInputs(idNum, TipoVuelo);

}

function getNumberOfMonths() {
    return isMobile.any() != null ? 1 : 2;
}

function initInputOWRT(tipoVuelo, id) {
    if (typeof (id) == "undefined") {
        id = 1;
    }
    _numberOfMonths = getNumberOfMonths;
    var delta = typeof (GObjParametrosConfig.B2C2_DeltaFechaVuelos) != "undefined" ? parseInt(GObjParametrosConfig.B2C2_DeltaFechaVuelos) : 1;
    
    if (tipoVuelo == "RT") {
        initCalendar({
            type: 'ui',
            selectorOne: "#dateJourney_1",
            selectorTwo: "#dateJourney_back",
            numberOfMonths: 2,
            minIntOne: parseInt(GObjParametrosConfig.B2C2_OffsetFechaVuelos),
            minIntTwo: delta
        });
    }
    else {
        initCalendar({
            type: 'ui',
            selectorOne: "#dateJourney_" + id,
            numberOfMonths: _numberOfMonths,
            minIntOne: parseInt(GObjParametrosConfig.B2C2_OffsetFechaVuelos)
        });
    }
} 

function iniciaInputs(i, TipoVuelo) {

    var _numberOfMonths = getNumberOfMonths();

    if (TipoVuelo == "MT" && i == 2) {
       
        initInputOWRT("OW");
    }
    var minDate = moment().add(parseInt(GObjParametrosConfig.B2C2_OffsetFechaVuelos), 'days'); 
    if (i > 1) {
        minDate = $("#dateJourney_" + (i - 1)).datepicker("getDate");
    }

    AutocompleteIatas("txtOrigen_" + i, "AutoCompleteIataAirp");
    AutocompleteIatas("txtDestino_" + i, "AutoCompleteIataAirp");

    if (TipoVuelo != "RT") { 
        initCalendar({
            type: 'ui',
            selectorOne: "#dateJourney_" + i,
            numberOfMonths: _numberOfMonths,
            minIntOne: parseInt(GObjParametrosConfig.B2C2_OffsetFechaVuelos)
        });
    }
    if (i > 1) {
        $("#txtOrigen_" + i).val($("#txtDestino_" + (i - 1)).val());
        $("#Origen_" + i).val($("#Destino_" + (i - 1)).val());
        $("#dateJourney_" + i).val($("#dateJourney_" + (i - 1)).val());
    }
}

function addTrayecto(container, idiom) {
    var count = $(".trayectoInputs").length + 1;
    if (count > 5)
        return 5;
    var journey = $("<div />");
    journey.setTemplate(getTemplateText(GCarpetaTemplBuscador + '_IdaRegreso'), null, { filter_data: false });
    journey.setParam("id", count);
    journey.setParam("idiom", GObjTraduccion);
    journey.processTemplate({});
    $("#" + container).append(journey);
    return count;
}

function removeOthers(id, idElement) {
    id++;
    pre = idElement.split("_")[0];
    var removi = false;
    for (var i = id; $("#" + pre + "_" + i).length > 0 && i <= 5; i++) {
        $("#" + pre + "_" + i).remove();
        removi = true;
    }
    return removi;
}

function BuscarVuelos() {
    if (!ValidarFormularioVuelos()) {
        return;
    }
    var Virtual = getVirtualPath("Vuelos");
    if ((GObjParametrosConfig.B2C2_TipoPortal == "B2B") && (GObjDatosUsuario == null)) {
        Mensaje_Boostrap(GObjTraduccion.resMensajeValidaLogin, GObjTraduccion.resMensajeValidaLogin);
    } else {
        var datosBusqueda = $("#frmBuscadorVuelos").serialize();
        var j = $.evalJSON(params2json(datosBusqueda));

        var origenes = "";
        var destinos = "";
        var fechas = "";
        var regreso = "";
        var infoPassengers = $("#FlightsInfo").IFPassengerBox("getRoomsInfo");
        if ($("input[id^='Origen_']").length == 1) {
            var tipoVuelo = ($("#chkOWRT").is(':checked')) ? "OW" : "RT";
            origenes = $("#Origen_1").val();
            destinos = $("#Destino_1").val();
            fechas = $("#dateJourney_1").val();
            regreso = $("#dateJourney_back").val();

        }
        else {
            var tipoVuelo = "MT";
            for (var i = 1; $("#txtOrigen_" + i).length > 0; i++) {
                var coma = $("#txtOrigen_" + (i + 1)).length > 0 ? "," : "";
                origenes += $("#Origen_" + i).val() + coma;
                destinos += $("#Destino_" + i).val() + coma;
                fechas += $("#dateJourney_" + i).val() + coma;

            }
        }
        if (j.selNinosVuelosAges == "") {
            j.selNinosVuelosAges = "0";
        }
        if (infoPassengers.ages.length == 0) {
            infoPassengers.ages.push("0");
        }
        else
        {
            for (var i = 0; i < infoPassengers.ages.length; i++)
            {
                if (infoPassengers.ages[i] == "-1")
                {
                    return;
                }
            }
            
        }
        j.VuelosDirectos = checkIatasVueloDirecto() ? true: ($("#VuelosDirectos").is(':checked'));
        j.flexi = GObjParametrosConfig.B2C2_CalendarVuelos == "S" && $("#fechasFlexibles").is(':checked');
        j.tipoVuelo = tipoVuelo;
        if (typeof (j.txtPromocode) == "undefined" || j.txtPromocode == "")
            j.txtPromocode = "";
        
        var filtros = addFilters(j);
        if (tipoVuelo == "RT") {
            fechas += "," + regreso;
        }
        document.location = "/" + Virtual + "/" + origenes + "/" + destinos + "/" + fechas + "/" + infoPassengers.adts + "/" + infoPassengers.chds + "/" + infoPassengers.ages + "/" + filtros.join(";");
    }
}

function addFilters(j,type) {
    var filtros = [];
    type = (typeof (type) != "undefined" && type != "") ? type : "";
    if (typeof (j.tipoVuelo) != "undefined") {
        filtros.push(j.tipoVuelo);
    }
    j["selBagage" + type] = (j["selBagage" + type] == undefined || j["selBagage" + type] == "") ? j["selBagage" + type] = "2" : j["selBagage" + type];

    if (j["Aerolinea" + type] != "") {
        filtros.push(j["Aerolinea" + type]);
    }
    if (j["selClase"] != "" && j["selClase"] != "Economica") {
        for (var i = filtros.length; i < 2; i++)
            filtros.push("");
        filtros.push(j["selClase"]);
    }
    if (j["VuelosDirectos" + type] != "") {
        for (var i = filtros.length; i < 3; i++)
            filtros.push("");
        filtros.push(j["VuelosDirectos" + type]);
    }
    if (j["selBagage" + type] != "" && j["selBagage" + type] !=2) {
        for (var i = filtros.length; i < 4; i++)
            filtros.push("");
        filtros.push(j["selBagage" + type]);
    }
    if (j.flexi!="") {
        for (var i = filtros.length; i < 5; i++)
            filtros.push("");
        filtros.push("true");
    }
    if (j["txtPromocode" + type] != "") {
        for (var i = filtros.length; i < 6; i++)
            filtros.push("");
        filtros.push(j["txtPromocode" + type]);
    }
    if (type == "VH") {
        var city = "";
        if ($("#AlloyCityVH").is(':checked')) {
            if (j["txtCiudad" + type] != undefined && j["txtCiudad" + type] != "") {
                city = j.CiudadVH
            }
        }
        var dates = "";
        if ($("#CheckDateCityVH").is(':checked')) {
            if (j["dateCity" + type] != "" && j["dateCity_back" + type] != "") {
                dates = j.dateCityVH + "," + j.dateCity_backVH;
            }
        }
        for (var i = filtros.length; i < 7; i++)
            filtros.push("");
        filtros.push(city + "," + dates);
    }
    return filtros;
}

function ValidarFormularioVuelos() {

    if ($("input[id^='Origen_']").length == 1) {
        if (!$("#chkOWRT").is(':checked')) {
            if ($("#dateJourney_back").val() == "") {
                Mensaje(GObjTraduccion.resValidaFechaRegreso, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
                return false;
            }
        }
    }
    for (var i = 1; $("#txtOrigen_" + i).length > 0; i++) {
        var ori = $("#txtOrigen_" + i).val();
        var ori_aux;        
        var preDate = i == 1 ? $("#dateJourney_1").val() : $("#dateJourney_" + (i - 1)).val(); 
        if ($("#Origen_" + i).val() == "") {
            Mensaje(GObjTraduccion.resValidaOrigen, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
            return false;
        }

        ori = $("#txtDestino_" + i).val();

        if ($("#Destino_" + i).val() == "") {
            Mensaje(GObjTraduccion.resValidaDestino, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
            return false;
        }

        if ($("#Origen_" + i).val() == $("#Destino_" + i).val())
        {
            Mensaje(GObjTraduccion.resValidaOrigen, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
            return false;
        }
        // && (getObjFecha(preDate, "dd-MM-yyyy") - getObjFecha($("#dateJourney_" + i).val(),"dd-MM-yyyy") >= 0)
        if ($("#dateJourney_" + i).val() == "") {
            Mensaje(GObjTraduccion.resValidaFechaIda, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
            return false;
        }

    }

    if (parseInt($("#selAdultosVuelos").val()) + parseInt($("#selNinosVuelos").val()) == 0) {
        Mensaje(GObjTraduccion.resValidaPasajerosVuelos, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
        return false;
    }
        
    if (parseInt($("#selNinosVuelos").val()) > 0) {
        var selAges = [] = $("#selNinosVuelosAges").val().split(",");
       if (selAges.length != parseInt($("#selNinosVuelos").val()))
            return false;
    }

    ori = $("#txtAerolinea").val();
    $("#Aerolinea").val("");
    if (ori.length > 4) {
        ori_aux = ori.substring(ori.length - 4);

        if ((ori_aux.substring(0, 1) == "(") && (ori_aux.substring(3, 4) == ")")) {
            $("#Aerolinea").val(ori_aux.substring(1, 3));
        }
    }

    return true;
}

function BuscarVuelosParametros(Origen, Destino, FechaIni, FechaFin) {

    var AOrigen = Origen.split("-");
    var ADestino = Destino.split("-");

    document.location.href = "/Vuelos/1/" + $.trim(AOrigen[AOrigen.length - 1]) + "/" + $.trim(ADestino[ADestino.length - 1])
        + "/" + FechaIni + "/" + FechaFin + "/1/0/0/_/Economica/false";
}




//HOTELES

function BuscarHoteles() {
    if ((GObjParametrosConfig.B2C2_TipoPortal == "B2B") && (GObjDatosUsuario == null)) {
        $("#accessForm").dialog("open");
        $("#msgLogin").html(GObjTraduccion.resMensajeValidaLogin);
    } else {
        GObjParametrosHoteles = form2js("frmBuscadorHoteles", ".", false);

       var hab = $("#hotelsInfo").IFPassengerBox("getRoomsInfoDown");
       
        GObjParametrosHoteles.CheckInHoteles = $("#CheckInHoteles").val();
        GObjParametrosHoteles.CheckOutHoteles = $("#CheckOutHoteles").val();

        var Virtual = getVirtualPath("Hoteles");
        document.location.href = "/" + Virtual + "/" + GObjParametrosHoteles.DestinoHoteles + "/" + GObjParametrosHoteles.CheckInHoteles + "/" + GObjParametrosHoteles.CheckOutHoteles + "/" + hab.adts + "/" + hab.chds.join(''); 

    }
}

function ValidarFormularioHoteles() {

    if ($("#txtDestinoHoteles").val() == "" || $("#DestinoHoteles").val() == "") {
        Mensaje(GObjTraduccion.resValidaDestino, document.title, "INFO");
        return false;
    }

    if ($("#CheckInHoteles").val() == "") {
        Mensaje(GObjTraduccion.resValidaFechaInicial, document.title, "INFO");
        return false;
    }

    if ($("#CheckOutHoteles").val() == "") {
        Mensaje(GObjTraduccion.resValidaFechaFinal, document.title, "INFO");
        return false;
    }

    var Adultos = $("select[id^='selAdultosHoteles_']");
    for (var i = 0; i < Adultos.length; i++) {
        if (parseInt($(Adultos[i]).val()) == 0) {
            Mensaje(GObjTraduccion.resValidaNumAdultos, document.title, "INFO");
            return false;
        }
    }
    var edades = $("select[id^='selEdadNinosHoteles_']");
    for (var i = 0; i < edades.length; i++) {
        if (parseInt($(edades[i]).val()) == 0) {
            Mensaje(GObjTraduccion.resValidaEdadMenor, document.title, "INFO");
            return false;
        }
    }
    for (CountSource = 0; CountSource < GObjDatosEntidad.sources.length ; CountSource++) {
        if (GObjDatosEntidad.sources[CountSource].toUpperCase() == "CANGOOROO") {
            var objAcc = [];

            var selRooms = parseInt($("#HabitacionesHoteles").val());
            for (i = 0; i < selRooms ; i++) {
                var objHabita = new Object();
                objHabita.ADT = parseInt($("#selAdultosHoteles_" + (i + 1)).val());
                objHabita.CHD = parseInt($("#selNinosHoteles_" + (i + 1)).val());
                objAcc[i] = objHabita;

                if (i > 0) {
                    if (!(objAcc[i].ADT == objAcc[i - 1].ADT) && (objAcc[i].CHD == objAcc[i - 1].CHD)) {
                        Mensaje(GObjTraduccion.resCangoorooAccommodationValidation, "");
                        return false;
                    }
                }
            }
        }
    }

    return true;
}

function BuscarHotelesParametros(Destino, FechaIni, FechaFin) {

    var ADestino = Destino.split("-");

    document.location.href = "/Hoteles/" + $.trim(ADestino[ADestino.length - 1]) + "/" + FechaIni + "/" + FechaFin + "/2/0-";
}

function InicializaHoteles(Contenedor, nombreFuncionBusqueda) {

    $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_buscadorhoteles'), null, { filter_data: false });
    $("#" + Contenedor).setParam("funcionBusqueda", nombreFuncionBusqueda);
    $("#" + Contenedor).processTemplate({});

    $("#tabSearch").append('<li><a href="#" class="btnbuscadorHoteles">Hoteles</a></li>');
    if (GObjParametrosConfig.B2C2_AutoCompleteHotels != "N") {
        AutocompleteIatas("txtDestinoHoteles", "AutoCompleteIataHotel");
    }
    var offset = typeof (GObjParametrosConfig.B2C2_OffsetFechaHoteles) != "undefined" ? parseInt(GObjParametrosConfig.B2C2_OffsetFechaHoteles) : 1;
    var delta = typeof (GObjParametrosConfig.B2C2_DeltaFechaHoteles) != "undefined" ? parseInt(GObjParametrosConfig.B2C2_DeltaFechaHoteles) : 1;
    var _numberOfMonths = getNumberOfMonths();
    var maxDaysHotel = typeof (GObjParametrosConfig.B2C2_NumMaxDiasHotel) != "undefined" ? parseInt(GObjParametrosConfig.B2C2_NumMaxDiasHotel) : 30;
    initCalendar({
        type: 'ui',
        selectorOne: "#CheckInHoteles",
        selectorTwo: "#CheckOutHoteles",
        minIntOne: offset,
        numberOfMonths: _numberOfMonths,
        minIntTwo: delta,
        maxDays: maxDaysHotel,
    });
    var cantPassDefaultHotel = typeof (GObjParametrosConfig.NUMEROADULTOSBUSCADORHOTEL) != "undefined" ? GObjParametrosConfig.NUMEROADULTOSBUSCADORHOTEL : 1;
    $("#hotelsInfo").IFPassengerBox({ maxRooms: parseInt(GObjParametrosConfig.B2C2_NumHabitaHoteles), ageChdMax: parseInt(GObjParametrosConfig.B2C2_EdadesNinosHoteles), productBox: "Hotels", cantPassDefault: cantPassDefaultHotel });
    if (GObjParametrosHoteles != null) {
        GObjSelHotel = getInfoGeo(GObjParametrosHoteles.HotelSearchCriteria.Criterion.Address.AddressCode)
        $("#DestinoHoteles").val(GObjParametrosHoteles.HotelSearchCriteria.Criterion.Address.AddressCode);
        $("#txtDestinoHoteles").val(GObjSelHotel.description);
        $("#CheckInHoteles").val(FormateaFecha(GObjParametrosHoteles.HotelSearchCriteria.Criterion.StayDateRange.Start));
        $("#CheckOutHoteles").val(FormateaFecha(GObjParametrosHoteles.HotelSearchCriteria.Criterion.StayDateRange.End));
        GobjPassengerSearch = [];
        fillGobjPassengerSearch("Hotels");
        $("#hotelsInfo").IFPassengerBox("getInfoSearch", GobjPassengerSearch);
    } 
}


//PLANES
function InicializaPlanes(Contenedor, nombreFuncionBusqueda, PlanTematico, Sources, IDBuscador) {

    if (typeof (PlanTematico) == "undefined")
        var PlanTematico = 0;

    if (typeof (Sources) == "undefined")
        var Sources = GObjDatosEntidad.sources;

    if (typeof (IDBuscador) == "undefined")
        var IDBuscador = "Paquetes";

    var defaultConOrigen = (Sources.indexOf("visualturismo") > -1) || (Sources.indexOf("paquetes_propios") > -1 && (typeof(GObjParametrosConfig.B2C2_TieneOrigenPlanPropio) != 'undefined' &&  GObjParametrosConfig.B2C2_TieneOrigenPlanPropio == "S"));
    defaultConOrigen = (defaultConOrigen ? 'C' : 'S');
    defaultConOrigen = (typeof (GObjParametrosConfig.USABUSCADORTEMATICO) != 'undefined' && GObjParametrosConfig.USABUSCADORTEMATICO == "S") ? 'T' : defaultConOrigen;
    var mostrarSelectorTipo = defaultConOrigen && (Sources.indexOf("paquetes_propios") == -1 ||
        Sources.indexOf("panavision") > -1 ||
        Sources.indexOf("specialtours") > -1);
    if (defaultConOrigen == "T") {
        $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_paquetestematicos'), null, { filter_data: false });
    } else
    {
        $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_buscadorplanes'), null, { filter_data: false });
    }
    

    $("#" + Contenedor).setParam("Sources", Sources.toString());
    $("#" + Contenedor).setParam("IDBuscador", IDBuscador);
    $("#" + Contenedor).setParam("funcionBusqueda", nombreFuncionBusqueda);
    $("#" + Contenedor).setParam("defaultConOrigen", defaultConOrigen);
    $("#" + Contenedor).setParam("mostrarSelectorTipo", mostrarSelectorTipo);
    $("#" + Contenedor).processTemplate({});
    if (typeof (GObjParametrosConfig.B2C2_DeltaFechaPlanes) == "undefined")
        GObjParametrosConfig.B2C2_DeltaFechaPlanes = 30;
    if ((typeof (GObjParametrosConfig.B2C2_TipoCalendarioPlanes) != "undefined") && GObjParametrosConfig.B2C2_TipoCalendarioPlanes == 'S')
        var tipoCal = "year";
    else
        var tipoCal = "day";

    var _numberOfMonths = getNumberOfMonths();        

    initCalendar({
        type: 'ui',
        selectorOne: "#CheckInPlanes" + IDBuscador,
        selectorTwo: "#CheckOutPlanes" + IDBuscador,
        minIntOne: parseInt(GObjParametrosConfig.B2C2_OffsetFechaPlanes),
        minIntTwo: parseInt(GObjParametrosConfig.B2C2_DeltaFechaPlanes),
        numberOfMonths: _numberOfMonths,
        calendarType: tipoCal=="year" ? "month": "day", 
    });
    if(defaultConOrigen != "T")
    {
        var cantPassDefaultPaquete = typeof (GObjParametrosConfig.NUMEROADULTOSBUSCADORPAQUETE) != "undefined" ? GObjParametrosConfig.NUMEROADULTOSBUSCADORPAQUETE : 1;
        $("#PackagesInfo").IFPassengerBox({ ageChdMax: parseInt(GObjParametrosConfig.B2C2_EdadesNinosPaquetes), productBox: "packages", cantPassDefault: cantPassDefaultPaquete});
        if (typeof (GObjParametrosPlanes) != 'undefined' && GObjParametrosPlanes != null && typeof (GObjParametrosPlanes.Acomodaciones) != 'undefined') {
	        GobjPassengerSearch = [];
	        var habs = GObjParametrosPlanes != null ? GObjParametrosPlanes.Acomodaciones.List : 0;
	        var adts = 0;
	        var chds = 0;
	        for (var contHab = 0; contHab < habs.length; contHab++) {
	            var agesHab = [];
	            adts = habs[contHab].Adults;
	            chds = habs[contHab].Children;
	            for (var agesChds = 0; agesChds < habs[contHab].childrenAges.length; agesChds++)
	            {
	                agesHab.push(habs[contHab].childrenAges[agesChds]);
	            }
	            GobjPassengerSearch.push({
	                adt: adts,
	                chd: chds,
	                ageChd: agesHab,
	                hab: habs.length,
	                product: "Packages"
	            });
	        }
	        $("#PackagesInfo").IFPassengerBox("getInfoSearch", GobjPassengerSearch);
	        $("#CheckInPlanes" + IDBuscador).val(FormateaFecha(GObjParametrosPlanes.FechaIda));
	        $("#frmBuscadorPlanes" + IDBuscador + " input[name=tipoPaquete][value=" + GObjParametrosPlanes.tipoPaquete + "]").attr("checked", true);
	    }
	}
    clickTipoPlan(defaultConOrigen, Sources, IDBuscador);
    if (typeof (GObjParametrosConfig.OCULTARCAMPOLLEGADAPAQUETESBUSCADOR) != 'undefined' &&
        GObjParametrosConfig.OCULTARCAMPOLLEGADAPAQUETESBUSCADOR == "S") {
        getLastDatePackages(IDBuscador);
    } 
    if (PlanTematico != "0")
        $("#frmBuscadorPlanes" + IDBuscador + " #planNalInt").css("display", "none");

}

function clickTipoPlan(TipoPlan, Sources, IDBuscador) {
    var IDOrigen = 0;
    if (TipoPlan == "C") {
        $("#opcionesViaje" + IDBuscador).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_paquetesconorigen'), null, { filter_data: false });
        //Sources = ["visualturismo"];
        $("#frmBuscadorPlanes" + IDBuscador + " #HabitacionesPlanesDiv").hide();

    } else if (TipoPlan == "S") {
        $("#opcionesViaje" + IDBuscador).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_paquetessinorigen'), null, { filter_data: false });
        $("#frmBuscadorPlanes" + IDBuscador + " #HabitacionesPlanesDiv").show();
    } else if (TipoPlan == "T") {
        $("#opcionesViaje" + IDBuscador).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_paquetestematicos'), null, { filter_data: false });
        $("#frmBuscadorPlanes" + IDBuscador + " #HabitacionesPlanesDiv").show();
    }

    if (typeof (window.PlanTematico) == "undefined")
        window.PlanTematico = 0;
    $("#opcionesViaje" + IDBuscador).setParam("IDBuscador", IDBuscador);
    $("#opcionesViaje" + IDBuscador).processTemplate({});
    $("#opcionesViaje" + IDBuscador).show();

    var optionalBlank = null;
    if (GObjParametrosConfig.B2C2_BusPaqCiudadesSinDefecto != undefined && GObjParametrosConfig.B2C2_BusPaqCiudadesSinDefecto.toUpperCase() == 'S') {
        optionalBlank = ' ';
    }
    var optionsIda = {};
    var optionsSalida = {};
    var tipoPaquete = ($("#frmBuscadorPlanes" + IDBuscador + " input[name='tipoPaquete" + IDBuscador + "']:checked").val() != undefined) ? $("#frmBuscadorPlanes" + IDBuscador + " input[name='tipoPaquete" + IDBuscador + "']:checked").val() : 'S';
    var firstCity = "";
    if (typeof (Sources) == "string")
        Sources = Sources.split(",");
    
    if (TipoPlan == 'C' || TipoPlan == "ST") {      

        $.ajax({
            url: "/UtilsB2C.aspx/OrigenesPlan",
            type: "POST",
            contentType: "application/json;charset=iso8859-1",
            dataType: "json",
            async: false,
            success: function (data) {
                optionsSalida = JSON.parse(data.d);
                for (var i = 0; i < optionsSalida.length; i++) {
                    var selected = "";
                    if (GObjParametrosPlanes != undefined && optionsSalida[i].location_name == GObjParametrosPlanes.ciudadOrigen)
                        selected = " selected='seleted'";
                    $("#origen" + IDBuscador).append("<option value='" + optionsSalida[i].location_code + "'" + selected + ">" + optionsSalida[i].location_name + "</option>");
                }
                $("#origen" + IDBuscador).select2();                
                $("#origen" + IDBuscador).change(function () {
                    //IDOrigen = $("#origen" + IDBuscador).val();
                }); 
            }
        });
    }
    if (TipoPlan == 'T' || TipoPlan== 'ST') {      
        var codCity = 0;
        $.ajax({
            url: "/UtilsB2C.aspx/getPackageListSeason",
            type: "POST",
            data: '{"codCity":' + codCity + '}',
            contentType: "application/json;charset=iso8859-1",
            dataType: "json",
            async: false,
            success: function (data) {

                optionsSalida = JSON.parse(data.d);
                for (var i = 0; i < optionsSalida.length; i++) {
                    var selected = "";
                    if (GObjParametrosPlanes != undefined && optionsSalida[i].para_nombre )
                        selected = " selected='seleted'";
                    $("#Tematicos" + IDBuscador).append("<option value='" + optionsSalida[i].para_para + "'" + selected + ">" + optionsSalida[i].para_nombre + "</option>"); 
                }
                $("#Tematicos" + IDBuscador).select2();                
                $("#Tematicos" + IDBuscador).change(function () {
                });
            }
        });

    }


    getCiudadesPlan(IDBuscador, tipoPaquete, 0, Sources, IDOrigen);
}

function getCiudadesPlan(IDBuscador, tipoPaquete, PlanTematico, Sources, IDOrigen) {
    var req = { tipoPaquete: tipoPaquete, tematica: window.PlanTematico, sources: Sources, codOrigen: IDOrigen }

    $.ajax({
        url: "/UtilsB2C.aspx/CiudadesPlan",
        type: "POST",
        contentType: "application/json;charset=iso8859-1",
        dataType: "json",
        data: JSON.stringify(req),
        async: true,
        success: function (data) {
            optionsIda = JSON.parse(data.d);
            for (var i = 0; i < optionsIda.length; i++) {
                var selected = "";
                if (GObjParametrosPlanes != undefined && optionsIda[i].ciud_nombre == GObjParametrosPlanes.NombreCiudad)
                    selected = " selected='seleted'";
                $("#destino" + IDBuscador).append("<option value='" + optionsIda[i].ciud_ciud + "'" + selected + ">" + optionsIda[i].ciud_nombre + "</option>");
                $("#destino" + IDBuscador + "T").append("<option value='" + optionsIda[i].ciud_ciud + "'" + selected + ">" + optionsIda[i].ciud_nombre + "</option>"); /*firstCity = optionsIda[i].ciud_nombre;*/
                
            }
            $("#destino" + IDBuscador).select2();  //{ placeholder: firstCity }
            $("#destino" + IDBuscador + "T").select2();

        }
    });
}
/*
function AjustarFechasPlanes(tipoCal, IDBuscador) {
    var fechaIda = new Date($("#CheckInPlanes" + IDBuscador).datepicker('getDate'));
    var fechaReg = new Date($("#CheckOutPlanes" + IDBuscador).datepicker('getDate'));

    if ($("#CheckInPlanes" + IDBuscador).datepicker('getDate') == null) {
        //aqui debe ir offset no delta
        fechaIda = Date.today().add(parseInt(GObjParametrosConfig.B2C2_DeltaFechaPlanes)).days();
    }

    if (tipoCal == "year" &&
        !((fechaIda.getYear() == (new Date()).getYear()) &&
          (fechaIda.getMonth() == (new Date()).getMonth()))) {
        fechaIda.set({ day: 1 });
    }

    if ($("#CheckOutPlanes" + IDBuscador).datepicker('getDate') == null) {
        fechaReg = fechaIda.clone().add(parseInt(GObjParametrosConfig.B2C2_DeltaFechaPlanes)).days();
    }

    if (fechaIda > fechaReg)
        fechaReg = fechaIda.clone();

    if (tipoCal == "year") {
        fechaReg.moveToLastDayOfMonth();
    }

}
*/
function ValidarFormularioPlanes(IDBuscador) {
    var fecha;
    if (IDBuscador == "Paquetess") {
        if ($("#select2-destinoPaquetes-container") == "") {
            Mensaje(GObjTraduccion.resValidaDestino, document.title, "INFO");
            return false;
        }
        if ($("#select2-TematicosPaquetes-container") == "") {
            Mensaje(GObjTraduccion.resValidaDestino, document.title, "INFO");
            return false;
        }
        console.log($("#select2-destinoPaquetes-container"));

        return true;
    }
    else
    {
        if ($("#destinoPaquetes").val() == "") {
            Mensaje(GObjTraduccion.resValidaDestino, document.title, "INFO");
            return false;
        }

        if ($("#CheckInPlanes" + IDBuscador).val().length == 7) {
            $("#CheckInPlanes" + IDBuscador).val("01" + "-" + $("#CheckInPlanes" + IDBuscador).val());
            if ($("#CheckOutPlanes" + IDBuscador).val().length < 9)
                $("#CheckOutPlanes" + IDBuscador).val(
                    ultimoDiaMes($("#CheckOutPlanes" + IDBuscador).val().substring(0, 2),
                        $("#CheckOutPlanes" + IDBuscador).val().substring(4, 7)) + "-" + $("#CheckOutPlanes" + IDBuscador).val());
        }
        if ($("#CheckInPlanes" + IDBuscador).val() == "") {
            Mensaje(GObjTraduccion.resValidaFechaInicial, document.title, "INFO");
            return false;
        }

        if ($("#CheckOutPlanes" + IDBuscador).val() == "") {
            Mensaje(GObjTraduccion.resValidaFechaFinal, document.title, "INFO");
            return false;
        }

        var Adultos = $("select[id^='selAdultos" + IDBuscador + "_']");

        for (var i = 0; i < Adultos.length; i++) {
            if (parseInt($(Adultos[i]).val()) == 0) {
                Mensaje(GObjTraduccion.resValidaNumAdultos, document.title, "INFO");
                return false;
            }
        }
        return true;
    }
   
   
}

function BuscarPlanes(IDBuscador) {
    if (IDBuscador == "Paquetess") {
        if ((GObjParametrosConfig.B2C2_TipoPortal == "B2B") && (GObjDatosUsuario == null)) {
            $("#accessForm").dialog("open");
            $("#msgLogin").html(GObjTraduccion.resMensajeValidaLogin);
        } else {
            IDBuscador = "Paquetes";
            var json = form2js("frmBuscadorPlanes" + IDBuscador, ".", false);
            var Destino = $("#destino" + IDBuscador + " option:selected").text();
            var Tematicos = $("#Tematicos" + IDBuscador + " option:selected").text();
            json.Destino = Destino;
            json.Destino = json.Destino.replace(/ /g, "");
            json.Tematicos = Tematicos;
            json.Tematicos = json.Tematicos.replace(/ /g, "");

            var Virtual = getVirtualPath(IDBuscador);

            //PaquetesTemporadas/{Destino}/{codDestino}/{Tematico}/{codTema}

            document.location = "/PaquetesTemporadas"
                + "/" + encodeURI(json.Destino)
                + "/" + json["destino" + IDBuscador]
                + "/" + encodeURI(json.Tematicos)
                + "/" + json["Tematicos" + IDBuscador];


        }
    }
    else {
        if ((GObjParametrosConfig.B2C2_TipoPortal == "B2B") && (GObjDatosUsuario == null)) {
            $("#accessForm").dialog("open");
            $("#msgLogin").html(GObjTraduccion.resMensajeValidaLogin);
        } else {
            var json = form2js("frmBuscadorPlanes" + IDBuscador, ".", false);
            var Adultos = $("select[id^='selAdultos" + IDBuscador + "_']");
            var Ninos = $("select[id^='selNinos" + IDBuscador + "_']");
            var Origen = "_";

            if ($("#origen" + IDBuscador).length > 0) {
                Origen = $("#origen" + IDBuscador + " option:selected").text();

            }
            var Destino = $("#destino" + IDBuscador + " option:selected").text();
            json.Origen = Origen;
            json.Destino = Destino;

            
            var checkin = $("#CheckInPlanes" + IDBuscador).val();
            var checkout = $("#CheckOutPlanes" + IDBuscador).val();

            
            var hab = $("#PackagesInfo").IFPassengerBox("getRoomsInfoDown");
            var Virtual = getVirtualPath(IDBuscador);
            if (typeof (window.PlanTematico) == 'undefined' || window.PlanTematico == 0) {
                //Paquetes/{fecInicio}/{fecFinal}/{Origen}/{Destino}/{codDestino}/{Adultos}/{Menores}
                document.location.href = "/" + Virtual + "/" + checkin
                    + "/" + checkout
                    + "/" + replaceAll(json.Origen, "+", "%20")
                    + "/" + replaceAll(json.Destino, "+", "%20")
                    + "/" + json["destino" + IDBuscador]
                    + "/" + hab.adts
                    + "/" + hab.chds.join('');
            }
            else {
                // PlanesTematicos/{fecInicio}/{fecFinal}/{Origen}/{Destino}/{codDestino}/{Adultos}/{Menores}/{Tema}
                document.location.href = "/PaquetesTematicos"
                    + "/" + $("#frmBuscadorPlanes" + IDBuscador + " #CheckInPlanes" + IDBuscador).val()
                    + "/" + $("#frmBuscadorPlanes" + IDBuscador + " #CheckOutPlanes" + IDBuscador).val()
                    + "/" + replaceAll(json.Origen, "+", "%20")
                    + "/" + replaceAll(json.Destino, "+", "%20")
                    + "/" + json.destinoPlanes
                    + "/" + hab.adts
                    + "/" + hab.chds.join('')
                    + "/" + window.PlanTematico;
            }
        }
        
    }
}
//TOURS
function BuscarTiquetes() {


    if (!ValidarFormularioTours()) {
        return;
    }


    if ((GObjParametrosConfig.B2C2_TipoPortal == "B2B") && (GObjDatosUsuario == null)) {
        $("#accessForm").dialog("open");
        $("#msgLogin").html(GObjTraduccion.resMensajeValidaLogin);
    } else {
        $('#preloader').css("background-color", "rgba(255,255,255,0.5)");
        $('#preloader').fadeIn();
        var datosBusqueda = $("#frmBuscadorTiquetes").serialize();
        var j = $.evalJSON(params2json(datosBusqueda));
        //var origen = $("#ticketCiudad").data("kendoDropDownList").value();
        document.location.href = "/Tours/_/" + j.dteInicioTkt + "/" + j.dteFinTkt + "/" + j.ticketCiudad + "/_/_/" + j.selAdultosTkt + "/" + j.selNinosTkt;
    }
}

function InicializaTiquetes(Contenedor, nombreFuncionBusqueda) {

    $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_buscadortiquetes'), null, { filter_data: false });
    $("#" + Contenedor).setParam("funcionBusqueda", nombreFuncionBusqueda);
    $("#" + Contenedor).processTemplate({});
    $("#tabSearch").append('<li><a href="#" class="btnbuscadorTransfers">Transfers</a></li>');

    fillCmbTiquetes();

    initCalendar({
        type: 'ui',
        selectorOne: "#dteInicioTkt",
        selectorTwo: "#dteFinTkt",
        numberOfMonths: 2,
        minIntOne: parseInt(GObjParametrosConfig.B2C2_OffsetFechaTickets),
        minIntTwo: parseInt(1),
    });

    if (GObjParametrosTiquetes != null) {
        $("#dteInicioTkt").datepicker('setDate', formatDateUTC(GObjParametrosTiquetes.DateFrom, "dd-MM-yyyy"));
        $("#dteFinTkt").datepicker('setDate', formatDateUTC(GObjParametrosTiquetes.DateTo, "dd-MM-yyyy"));
        $("#txtticketCiudad").val(GObjParametrosTiquetes.Destination.gnom_nombre);
        $("#ticketCiudad").val(GObjParametrosTiquetes.Destination.geon_geon);
        $("#selAdultosTkt").val(GObjParametrosTiquetes.Acommodation.Adults);
        $("#selNinosTkt").val(GObjParametrosTiquetes.Acommodation.AllChildren);
    }
}

function fillCmbTiquetes() {
    $("#txtticketCiudad").autocomplete({
        minLength: 3,
        autoFocus: true,
        source: function (request, response) {
            // tooltipAutoComplete(input, false);
            $.ajax({
                type: "POST",
                url: "/UtilsB2C.aspx/TicketsDestinations",
                data: '{"q":"' + request.term + '"}',

                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var iatas = getJSONResultAJAX(data.d);
                    var arrIata = new Array();
                    $.each(iatas, function (i, n) {
                        objElem = new Object();
                        if (n.A != undefined) {
                            objElem = n.A;
                            objElem.clase = "ysearchSuggestAllAirports";
                            objElem.tipo = GObjTraduccion.resAeropuerto;
                            objElem.elem_desc = n.A.gnom_nombre + (typeof (n.C) != 'undefined' ? (", " + n.C.gnom_nombre) : "");
                            arrIata[arrIata.length] = objElem;
                        }
                        if (n.C != undefined) {
                            objElem = n.C;
                            objElem.clase = "ysearchSuggestHotel";
                            objElem.tipo = (typeof (n.P) != undefined ? n.P.gnom_nombre : GObjTraduccion.resCiudad);
                            objElem.elem_desc = n.C.gnom_nombre + (typeof (n.E) != 'undefined' ? (", " + n.E.gnom_nombre) : "");
                            arrIata[arrIata.length] = objElem;
                        }
                    });
                    response(arrIata);
                }
            });
        },
        select: function (event, ui) {
            $("#ticketCiudad").val(ui.item.geon_geon);
            event.preventDefault();
            $("#txtticketCiudad").val(ui.item.elem_desc);

            if (typeof (onSelect) == 'function') {
                onSelect(ui.item);
            }
        }

    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
          .append("<a>" + "<span class='" + item.clase + "'></span> " + item.elem_desc.replace(new RegExp(this.term, "gi"), '<b>$&</b>') + "</a>")
          .appendTo(ul);
    };

}

function ValidarFormularioTours() {
  
    if ($("#txtticketCiudad").val() == "") {
        Mensaje(GObjTraduccion.resValidaOrigen, document.title, "INFO");
        return false;
    }

    var fecha;
    if ($("#dteInicioTkt").val().length == 7) {
        $("#dteInicioTkt").val("01" + "-" + $("#dteInicioTkt").val());
        if ($("#dteFinTkt").val().length < 9)
            $("#dteFinTkt").val(ultimoDiaMes($("#dteFinTkt").val().substring(0, 2), $("#dteFinTkt").val().substring(4, 7)) + "-" + $("#dteFinTkt").val());
    }
    if ($("#dteInicioTkt").val() == "") {
        Mensaje(GObjTraduccion.resValidaFechaInicial, document.title, "INFO");
        return false;
    }

    if ($("#dteFinTkt").val() == "") {
        Mensaje(GObjTraduccion.resValidaFechaFinal, document.title, "INFO");
        return false;
    }

    var Adultos = $("select[id^='selAdultosTkt']");

    for (var i = 0; i < Adultos.length; i++) {
        if (parseInt($(Adultos[i]).val()) == 0) {
            Mensaje(GObjTraduccion.resValidaNumAdultos, document.title, "INFO");
            return false;
        }
    }


    return true;
}

function ValidaFormularioTiquetes() {
    msg = "";
    if ($("#ticketCiudad").val() == "")
        msg += "resValidaTktCiudad" + "<br />";
    if ($("#dteInicioTkt").val() == "")
        msg += "resValidaTktDesde" + "<br />";
    if ($("#dteFinTkt").val() == "")
        msg += "resValidaTktFin" + "<br />";
    if ($("#selAdultosTkt").val() == 0)
        msg += "resValidaTktAdultos" + "<br />";
    if (msg != "") {
        Mensaje(msg, document.title, "INFO");
        return false;
    }
    return true;
}

//TRANSFERS
function InicializaTransfers(Contenedor, nombreFuncionBusqueda) {

    $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_buscadortransfers'), null, { filter_data: false });
    $("#" + Contenedor).setParam("funcionBusqueda", nombreFuncionBusqueda);
    $("#" + Contenedor).processTemplate({});
    $("#tabSearch").append('<li><a href="#" class="btnbuscadorTransfers">Transfers</a></li>');

    fillCmbTransfers();

    $("#transfersDropOffPlace_Type").change(function () { onSubDestinationTypeChange(this); })
    $("#transfersPickupPlace_Type").change(function () { onSubDestinationTypeChange(this); })


    if (GObjParametrosTransfers != null) {
        $('input[name=rutaTipoTransfer][value=' + GObjParametrosTransfers.sTripType + ']').attr('checked', true);
        $('#txtTransfersCity').val(GObjParametrosTransfers.citySearch.gnom_nombre);
        GObjSelDestTransfers = {};
        GObjSelDestTransfers.elem_desc = GObjParametrosTransfers.citySearch.gnom_nombre;
        GObjSelDestTransfers.elem_code = GObjParametrosTransfers.citySearch.geon_geon;
        fillSubDestinationControl("#transfersPickupPlace");
        fillSubDestinationControl("#transfersDropOffPlace");
        $('#entregaCodigoTransfers').val(GObjParametrosTransfers.Destination.geon_geon);
        $('#recogidaCodigoTransfers').val(GObjParametrosTransfers.Destination.geon_geon);
        $('#recogidaSitioTransfers').val(GObjParametrosTransfers.Destination.geon_geon);
        $('#entregaSitioTransfers').val(GObjParametrosTransfers.DestinationPlaceText);
        clickTipoTransfer(GObjParametrosTransfers.TripType == 0 ? "round_trip" : "one_way");
    } else {
        clickTipoTransfer('round_trip');
    }
    
    $('#transfersFromDate').datetimepicker({
        altField: "#transfersFromDateTime",
        stepMinute: 30
    });
}

function BuscarTransfers() {

    if (!ValidarFormularioTransfers()) {
        return;
    }
    if ((GObjParametrosConfig.B2C2_TipoPortal == "B2B") && (GObjDatosUsuario == null)) {
        $("#accessForm").dialog("open");
        $("#msgLogin").html(GObjTraduccion.resMensajeValidaLogin);
    } else {
        var datosBusqueda = $("#frmBuscadorTransfers").serialize();
        var j = $.evalJSON(params2json(datosBusqueda));

        var HabitaNinos = "";
        var Ninos = $("select[name='transfersChd']");
        var Recogida = $("#transfersPickupPlace").val();
        var Entrega = $("#transfersDropOffPlace").val();

        var EdadesNinos = $("select[id^='selEdadNinosTransfers_']");
        var HabitaNinos = EdadesNinos.length > 0 ? "" : "_";

        for (var i = 0; i < EdadesNinos.length; i++) {
            if (i > 0) { HabitaNinos += ";"; }
            HabitaNinos += $(EdadesNinos[i]).val();
        }

        document.location.href = "/Transfers"
            + "/-"
            + "/" + (j.transfersFromDate + "T" + j.transfersFromDateTime).replace("+", " ").replace("%3A", ".")
            + "/_"
            + "/" + GObjSelDestTransfers.elem_code
            + "/" + j.transfersPickupPlace    // j.recogidaCodigoTransfers
            + "/" + j.transfersDropOffPlace
            + "/" + j.transfersAdt
            + "/" + HabitaNinos


    }
}

function SeleccionaNinosTransfers() {
    var numNinos = $("#selNinosTransfers").val();
    if (numNinos > 0) {
        $("#divEdadesNinosTransfers").setTemplate(getTemplateText(GCarpetaTemplBuscador + '_edadesninoshabitacionestransfers'), null, { filter_data: false });
        $("#divEdadesNinosTransfers").setParam("numNinos", numNinos);
        $("#divEdadesNinosTransfers").processTemplate({});

        //$("select[name='selEdadNinosTransfers']").kendoDropDownList();

    } else {
        $("#divEdadesNinosTransfers").html("");
    }
}

function clickTipoTransfer(tipoTransfer) {

    if (tipoTransfer == "one_way") {
        $("#opcionesViajeTransfers").setTemplate(getTemplateText(GCarpetaTemplBuscador + '_soloidatransfers'), null, { filter_data: false });
    } else if (tipoTransfer == "round_trip") {
        $("#opcionesViajeTransfers").setTemplate(getTemplateText(GCarpetaTemplBuscador + '_idaregresotransfers'), null, { filter_data: false });
    }

    $("#opcionesViajeTransfers").processTemplate({});
    $("#opcionesViajeTransfers").show();
    var offset = GObjParametrosConfig.B2C2_OffsetFechaTransfers != undefined ? GObjParametrosConfig.B2C2_OffsetFechaTransfers : 1;
    initCalendar({
        type: 'ui',
        selectorOne: "#idaFechaTransfers",
        selectorTwo: "#regresoFechaTransfers",
        numberOfMonths: 2,
        minIntOne: offset,
        minIntTwo: (offset + 1)
    });

}

function ValidarFormularioTransfers() {

    if ($("#txtTransfersCity").val() == "") {
        Mensaje(GObjTraduccion.resValidaDestino, document.title, "INFO");
        return false;
    }
    if ($("#transfersPickupPlace").val() == "") {
        Mensaje("", document.title, "INFO");
        return false;
    }
    if ($("#transfersDropOffPlace").val() == "") {
        Mensaje("", document.title, "INFO");
        return false;
    }
    if ($("#transfersFromDate").val() == "") {
        Mensaje("", document.title, "INFO");
        return false;
    }
    if ($("#transfersFromDateTime").val() == "") {
        Mensaje("", document.title, "INFO");
        return false;
    }
 
    return true;
}

function fillCmbTransfers() {
    AutocompleteIatas("txtTransfersCity", "AutoCompleteIataHotel", function (item) {
        GObjSelDestTransfers = item;
        fillSubDestinationControl("#transfersPickupPlace");
        fillSubDestinationControl("#transfersDropOffPlace");
    });
}

function fillSubDestinationControl(Control, preSelectedValue) {
    var controlTypeValue = $(Control + '_Type').val();
    $(Control).empty();

    if (GObjSelDestTransfers != null) {

        if (controlTypeValue == "P") {
            $(Control).append('<option value="' + GObjSelDestTransfers.elem_code + '" >' + GObjSelDestTransfers.elem_desc + '</option>');
        } else {
            $.ajax({
                type: "POST",
                url: "/UtilsB2C.aspx/TransfersSubDestinations",
                data: '{"Type":"' + controlTypeValue + '", "Parent":' + GObjSelDestTransfers.elem_code + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (res) {
                    if (res.d != null) {
                        var Obj = getJSONResultAJAX(res.d);
                        var subDestinationControl = $(Control);
                        $.each(Obj, function (idx, Dest) {
                            try {
                                var isSelected = '';
                                if (controlTypeValue == "A") {
                                    isSelected = (preSelectedValue != undefined && preSelectedValue == Dest.A.geon_geon) ? 'SELECTED' : '';

                                    subDestinationControl.append('<option value="' + Dest.A.geon_geon + '" ' + isSelected + '>' + Dest.A.gnom_nombre + '</option>');
                                }
                                if (controlTypeValue == "H") {
                                    isSelected = (preSelectedValue != undefined && preSelectedValue == Dest.T.geon_geon) ? 'SELECTED' : '';
                                    subDestinationControl.append('<option value="' + Dest.T.geon_geon + '" ' + isSelected + '>' + Dest.T.gnom_nombre + '</option>');
                                }
                            } catch (e) { }
                        });
                    }
                }
            });
        }
    }
}

function onSubDestinationTypeChange(Sender) {
    $("#" + Sender.id.replace("_Type", "")).children().remove();
    fillSubDestinationControl("#" + Sender.id.replace("_Type", ""));
};

//--
function replaceAll(text, busca, reemplaza) {
    while (text.toString().indexOf(busca) != -1)
        text = text.toString().replace(busca, reemplaza);
    return text;
}

function TabsBusqueda(Contenedor, Template) {
    $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_' + Template), null, { filter_data: false });
    $("#" + Contenedor).processTemplate({});
}

function getVirtualPath(product) {
    if (typeof (GObjParametrosConfig.B2C2_VirtualRutasBuscador) != "undefined") {
        var arrPath = GObjParametrosConfig.B2C2_VirtualRutasBuscador.split(",");
        if (product == "Vuelos")
            return arrPath[0];
        if (product == "Hoteles")
            return arrPath[1];
        if (product == "Paquetes")
            return arrPath[2];
        if (product == "Paquete")
            return arrPath[3];
        if (product == "Tickets")
            return arrPath[4];
        if (product == "Transfers")
            return arrPath[5];
    }
    else
        return product;
}

function tooltipAutoComplete(input, mostrar) {

}

//AUTOS
function InicializaAutos(Contenedor, nombrefuncionBusqueda) {

    $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_buscadorautos'), null, { filter_data: false });
    $("#" + Contenedor).setParam("funcionBusqueda", nombrefuncionBusqueda);
    $("#" + Contenedor).processTemplate({});

    $("#tabSearch").append('<li><a href="#" class="btnbuscadorAutos">Autos</a></li>');
    AutocompleteIatas("txtRetiroVehiculo", "AutoCompleteIataAirp", function (objInfo) {
        //if ($("#EntregaVehiculo").val() == "") {
        $("#txtEntregaVehiculo").val($("#txtRetiroVehiculo").val());
        $("#EntregaVehiculo").val($("#RetiroVehiculo").val());
        toggleClsIcon("EntregaVehiculo");
        //}
    });
    AutocompleteIatas("txtEntregaVehiculo", "AutoCompleteIataAirp");

    var offset = typeof (GObjParametrosConfig.B2C2_OffsetFechaAutos) != "undefined" ? parseInt(GObjParametrosConfig.B2C2_OffsetFechaAutos) : 1;
    var delta = typeof (GObjParametrosConfig.B2C2_DeltaFechaAutos) != "undefined" ? parseInt(GObjParametrosConfig.B2C2_DeltaFechaAutos) : 1;

    if (isMobile.any() != null)
        var numberOfMonths = 1;
    else
        var numberOfMonths = 2;

    initCalendar({
        type: 'ui',
        selectorOne: "#dteDesde",
        selectorTwo: "#dteHasta",
        minIntOne: offset,
        numberOfMonths: numberOfMonths,
        minIntTwo: delta
    });

    if (GObjParametrosAutos != null && GObjParametrosAutos.IataRetiro != null && GObjParametrosAutos.IataEntrega != null) {
        $("#txtRetiroVehiculo").val(GObjParametrosAutos.TxtRetiro);
        $("#txtEntregaVehiculo").val(GObjParametrosAutos.TxtEntrega);
        $("#RetiroVehiculo").val(GObjParametrosAutos.IataRetiro);
        $("#EntregaVehiculo").val(GObjParametrosAutos.IataEntrega);
        $("#dteDesde").val(GObjParametrosAutos.FechaRetiro);
        $("#dteHasta").val(GObjParametrosAutos.FechaEntrega);
        $("#tipoTarifa").val(GObjParametrosAutos.tipoTarifa);

        if (GObjParametrosAutos?.TiposTarifas != null) {

            if (GObjParametrosAutos.TiposTarifas.RentalsCompanies != null && GObjParametrosAutos.TiposTarifas.RentalsCompanies.length == 0) {
                GObjParametrosAutos.TiposTarifas.RentalsCompanies = GObjParametrosConfig.B2C2_CiasAutosBusqueda.split(",");
            }

            for (var k = 0; k < GObjParametrosAutos.TiposTarifas.RentalsCompanies.length; k++) {
                ListCompaniesRental.push(GObjParametrosAutos.TiposTarifas.RentalsCompanies[k].code);
            }
            for (var i = 0; i < GObjParametrosAutos.TiposTarifas.TypesCoverages.length; i++) {
                $("#tipoTarifa").append("<option value='" + GObjParametrosAutos.TiposTarifas.TypesCoverages[i].id + "' title='" + GObjParametrosAutos.TiposTarifas.TypesCoverages[i].description + "'>" + GObjParametrosAutos.TiposTarifas.TypesCoverages[i].name + "</option>");
            }
        }
        $("#tipoTarifa").val(GObjParametrosAutos.TipoTarifa);

        if (typeof (GObjParametrosAutos.Categorias) != 'undefined' && GObjParametrosAutos.Categorias != null) {
            $("#tiposAutos").val(GObjParametrosAutos.Categorias.length > 1 ? "A" : GObjParametrosAutos.Categorias[0]);
        }
        toggleClsIcon("RetiroVehiculo");
        toggleClsIcon("EntregaVehiculo");
        if (typeof (GObjParametrosConfig.VISUALIZALOCALESAUTOS) != 'undefined' && GObjParametrosConfig.VISUALIZALOCALESAUTOS == 'S') {
            var ArrayLocationList = LocalSave('RentalLocationList');
            if (typeof (ArrayLocationList) != 'undefined' && ArrayLocationList != null) {
                $("#lugarRetiroVehiculo").append("<option value='" + GObjParametrosAutos.IataRetiro.substring(0, 3) + "_ALL_From'>" + GObjParametrosAutos.TxtRetiro + "</option>");
                $("#lugarEntregaVehiculo").append("<option value='" + GObjParametrosAutos.IataEntrega.substring(0, 3) + "_ALL_To'>" + GObjParametrosAutos.TxtEntrega + "</option>");

                printPlacesCarList(ArrayLocationList, 'RetiroVehiculo', '');
                if (GObjParametrosAutos.Rentadoras.length = 1) {
                    onChangeSelectLocationRental(GObjParametrosAutos.Rentadoras[0]);
                }
                $("#lugarRetiroVehiculo").val(LocalSave('lugarEntrega'));
                $("#lugarEntregaVehiculo").val(LocalSave('lugarRetiro'));
            }
        }
    }
}

function ValidarFormularioAutos() {


    if ($("#RetiroVehiculo").val() == "" || $("#txtRetiroVehiculo").val() == "") {
        Mensaje(GObjTraduccion.resValidaRetiroVehiculo, document.title, "INFO");
        return false;
    }

    if ($("#lugarRetiroVehiculo").val() == "") {
        Mensaje("Elige el lugar de retiro", document.title, "INFO");
        return false;
    }

    if ($("#lugarEntregaVehiculo").val() == "") {
        Mensaje("Elige el lugar entrega", document.title, "INFO");
        return false;
    }

    if ($("#EntregaVehiculo").val() == "" || $("#txtEntregaVehiculo").val() == "") {
        Mensaje(GObjTraduccion.resValidaEntregaVehiculo, document.title, "INFO");
        return false;
    }

    if ($("#dteDesde").val() == "") {
        Mensaje(GObjTraduccion.resValidaFechaDesde, document.title, "INFO");
        return false;
    }

    if ($("#dteHasta").val() == "") {
        Mensaje(GObjTraduccion.resValidaFechaHasta, document.title, "INFO");
        return false;
    }

    return true;
}

function BuscarAutos() {
    if ((GObjParametrosConfig.B2C2_TipoPortal == "B2B") && (GObjDatosUsuario == null)) {
        $("#accessForm").dialog("open");
        $("#msgLogin").html(GObjTraduccion.resMensajeValidaLogin);
    } else {
        $('#preloader').css("background-color", "rgba(255,255,255,0.5)");
        $('#preloader').fadeIn();
        var datosBusqueda = $("#frmBuscadorAutos").serialize();
        var j = $.evalJSON(params2json(datosBusqueda));
        var listTypeCars = j.tiposAutos;
        HoraDesdeAutos = decodeURIComponent(j.HoraDesdeAutos).replace(":", "-");
        HoraHastaAutos = decodeURIComponent(j.HoraHastaAutos).replace(":", "-");
        var RetiroVehiculo = "";
        var EntregaVehiculo = ""
        var Rentadora = "";
        if (typeof (GObjParametrosConfig.VISUALIZALOCALESAUTOS) != 'undefined' && GObjParametrosConfig.VISUALIZALOCALESAUTOS == 'S') {
            LocalSave('lugarEntrega', $("#lugarRetiroVehiculo").val());
            LocalSave('lugarRetiro', $("#lugarEntregaVehiculo").val());
            RetiroVehiculo = j.lugarRetiroVehiculo.split('_')[0];
            EntregaVehiculo = j.lugarEntregaVehiculo.split('_')[0];
            Rentadora = $("#lugarRetiroVehiculo").val().split('_')[1];
        } else {
            RetiroVehiculo = j.RetiroVehiculo.split('_')[0];
            EntregaVehiculo = j.EntregaVehiculo.split('_')[0];
            Rentadora = "ALL";
        }
        document.location.href = "/Autos/" + RetiroVehiculo + "/" + EntregaVehiculo + "/" + j.dteDesde + "/" + j.dteHasta + "/" + HoraDesdeAutos + "/" + HoraHastaAutos + "/" + j.tipoTarifa + "/" + listTypeCars + "/" + Rentadora;
    }
}

function getLocationListCars(IATA, id, city, type) {
    var rentalSelect = "";
    if (id == 'RetiroVehiculo') {
        $("#lugarRetiroVehiculo").html("");
        $("#lugarEntregaVehiculo").html("");
    } else {
        $("#lugarEntregaVehiculo").html("");
        rentalSelect = $("#lugarRetiroVehiculo").val().split('_')[1];
    }
    ListCompaniesRental = [];
    if (OBJTypeFareCars != null) {
        for (var k = 0; k < OBJTypeFareCars.RentalsCompanies.length; k++) {
            ListCompaniesRental.push(OBJTypeFareCars.RentalsCompanies[k].code);
        }
    }
    var text = type == "A" ? city : "Seleccione Oficina";

    $("#lugarRetiroVehiculo").append("<option value='" + (type == "A" ? IATA + "_ALL_From'>" : "'>") + text + "</option>");
    $("#lugarEntregaVehiculo").append("<option value='" + (type == "A" ? IATA + "_ALL_To'>" : "'>") + text + "</option>");

    $.ajax({
        url: "/Autos/resultado_autos.aspx/ListarOficinasRentadora",
        type: "POST",
        data: '{"ciudad":"' + IATA + '", "rentadora":' + $.toJSON(ListCompaniesRental) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        asycn: false,
        success: function (data) {
            var ObjLocationList = data.d.CarLocationLists;
            LocalSave('RentalLocationList', ObjLocationList);
            printPlacesCarList(ObjLocationList, id, rentalSelect);
        }
    });
}

function getCarsTypeCoverage(isoCountry, airpCode, inputText, elem_desc, type) {
    $.ajax({
        url: "/UtilsB2C.aspx/AutoListarCoberturas",
        type: "POST",
        data: '{"isoCountry":"' + isoCountry + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        asycn: false,
        success: function (data) {
            $("#tipoTarifa").html("");
            OBJTypeFareCars = JSON.parse(data.d);
            if (typeof (OBJTypeFareCars) != 'undefined' && OBJTypeFareCars != null) {
                for (var k = 0; k < OBJTypeFareCars.RentalsCompanies.length; k++) {
                    ListCompaniesRental.push(OBJTypeFareCars.RentalsCompanies[k].code);
                }

                for (var i = 0; i < OBJTypeFareCars.TypesCoverages.length; i++) {
                    $("#tipoTarifa").append("<option value='" + OBJTypeFareCars.TypesCoverages[i].id + "' title='" + OBJTypeFareCars.TypesCoverages[i].description + "'>" + OBJTypeFareCars.TypesCoverages[i].name + "</option>");

                }
                if (typeof (GObjParametrosConfig.VISUALIZALOCALESAUTOS) != 'undefined' && GObjParametrosConfig.VISUALIZALOCALESAUTOS == 'S')
                    getLocationListCars(airpCode, inputText, elem_desc, type);
            }
        }
    });
}

function onChangeSelectLocationRental(codeRental) {

    var companyRentalCode = $("#lugarRetiroVehiculo").val().split('_')[1];
    if (typeof (codeRental) != 'undefined' && codeRental != null) {
        companyRentalCode = codeRental;
    }
    var RentalSelect = null;
    var ObjLocationList = LocalSave('RentalLocationList');
    for (var p = 0; p < ObjLocationList.length; p++) {

        if (RentalSelect != null && RentalSelect.length > 0) {
            break;
        }

        var response = ObjLocationList;
        if (response != null) {
            RentalSelect = $.grep(response, function (carLocations) {
                return carLocations.Rental.code == companyRentalCode;
            });
        }
    }

    var firstValue = $("#lugarEntregaVehiculo")[0][0].value;
    var firstText = $("#lugarEntregaVehiculo")[0][0].label;
    $("#lugarEntregaVehiculo").html("");
    $("#lugarEntregaVehiculo").append("<option value='" + firstValue + "'>" + firstText + "</option>")

    if (RentalSelect != null && RentalSelect.length > 0) {
        for (var i = 0; i < RentalSelect.length; i++) {
            var locationCodeIATA = RentalSelect[i].locationCode;
            var addressLocation = RentalSelect[i].Address.streetNumber + " " + response[i].Address.cityName;
            var companyNameRental = RentalSelect[i].Rental.name;
            var companyCodeRental = RentalSelect[i].Rental.code;
            $("#lugarEntregaVehiculo").append("<option value='" + locationCodeIATA + "_" + companyCodeRental + "_To'>" + addressLocation + " - (" + companyNameRental + ")" + "</option>");
        }
    }
}

function printPlacesCarList(response, id, rentalSelect) {    
    if (response != null && response.length > 0) {
        if (id == 'RetiroVehiculo') {
            for (var i = 0; i < response.length; i++) {
                var locationCodeIATA = response[i].locationCode;
                var addressLocation = response[i].Address.streetNumber + " " + response[i].Address.cityName;
                var companyNameRental = response[i].Rental.name;
                var companyCodeRental = response[i].Rental.code;
                $("#lugar" + id).append("<option value='" + locationCodeIATA + "_" + companyCodeRental + "_From'>" + addressLocation + " - (" + companyNameRental + ")" + "</option>");
            }
        } else {
            var RentalSelect = $.grep(response, function (carLocations) {
                return carLocations.Rental.code == rentalSelect;
            });
            for (var j = 0; j < RentalSelect.length; j++) {
                var locationCodeIATA = RentalSelect[j].locationCode;
                var addressLocation = RentalSelect[j].Address.streetNumber + " " + RentalSelect[j].Address.cityName;
                var companyNameRental = RentalSelect[j].Rental.name;
                var companyCodeRental = RentalSelect[j].Rental.code;
                $("#lugar" + id).append("<option value='" + locationCodeIATA + "_" + companyCodeRental + "_To'>" + addressLocation + " - (" + companyNameRental + ")" + "</option>");
            }
        }
    }
    
}

//
function ocultarOpcionMT(objGeo, id) {
    countrysDest[id] = objGeo.coun_code;
    if ((id == "txtOrigen_1" || id == "txtDestino_1") && Object.keys(countrysDest).length > 1) {
        var other = "txtOrigen_1" == id ? "txtDestino_1" : "txtOrigen_1";
        var mostrar = (countrysDest[id] != GObjDatosDominio.pais || countrysDest[other] != GObjDatosDominio.pais);
        if (mostrar) {
            $(".flyControls").show();
            $(".divFechasFlexibles").show();
        }
        else {
            $(".flyControls").hide();
            $(".divFechasFlexibles").hide();
        }
    }
}

function clsAutocomplete(id) {
    $("#txt" + id).val("");
    $("#" + id).val("");
    toggleClsIcon(id);
}

function toggleClsIcon(id) {
    if ($("#" + id).val() == "") {
        $("#" + id).nextAll("span").removeClass("glyphicon-remove").addClass("glyphicon-globe");
        $("#" + id).nextAll("span").css("cursor", "inherit");
    }
    else
        $("#" + id).nextAll("span").addClass("glyphicon-remove").removeClass("glyphicon-globe");
    $("#" + id).nextAll("span").css("cursor", "pointer");
}


/**
 * inicializa el buscador de vuelos mas hotel
 */

function InicializaVueloHotel(Contenedor, nombrefuncionBusqueda) {

    $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_vuelohotel'), null, { filter_data: false });
    $("#" + Contenedor).setParam("funcionBusqueda", nombrefuncionBusqueda);
    $("#" + Contenedor).processTemplate({});
    $("#tabSearch").append('<li><a href="#" class="btnbuscadorVueloHotel">Vuelos + Hotel</a></li>');

    AutocompleteIatas("txtOrigenVH", "AutoCompleteIataAirp");
    AutocompleteIatas("txtDestinoVH", "AutoCompleteIataAirp");
    AutocompleteIatas("txtCiudadVH", "AutoCompleteIataHotel");
    $("#txtCiudadVH").attr("disabled", true);
    var numberOfMonths = (isMobile.any() == null) ? 2 : 1;
    var maxDaysVueloHotel = typeof (GObjParametrosConfig.B2C2_NumMaxDiasHotel) != "undefined" ? parseInt(GObjParametrosConfig.B2C2_NumMaxDiasHotel) : 30;
    initCalendar({
        type: 'ui',
        selectorOne: "#dateJourneyVH",
        selectorTwo: "#dateJourney_backVH",
        numberOfMonths: numberOfMonths,
        maxDays: maxDaysVueloHotel,
        minIntOne: parseInt(GObjParametrosConfig.B2C2_OffsetFechaVuelos),
        minIntTwo: parseInt(0)
    });
    initCalendar({
        type: 'ui',
        selectorOne: "#dateCityVH",
        selectorTwo: "#dateCity_backVH",
        numberOfMonths: numberOfMonths,
        minIntOne: parseInt(GObjParametrosConfig.B2C2_OffsetFechaVuelos),
        minIntTwo: parseInt(0)
    })
    var cantPassDefaultVueloHotel = typeof (GObjParametrosConfig.NUMEROADULTOSBUSCADORVUELOHOTEL) != "undefined" ? GObjParametrosConfig.NUMEROADULTOSBUSCADORVUELOHOTEL : 1;
    $("#dateCityVH").attr("disabled", true);
    $("#dateCity_backVH").attr("disabled", true);
    $("#AlloyCityVH").click(function (e) { accommodationAnotherCity("AlloyCityVH", "txtCiudadVH"); });
    $("#CheckDateCityVH").click(function (e) { accommodationAnotherCity("CheckDateCityVH", "dateCityVH"); accommodationAnotherCity("CheckDateCityVH", "dateCity_backVH"); });
    $("#FlightPlusInfo").IFPassengerBox({ productBox: "FlightsPlus", maxRooms: parseInt(GObjParametrosConfig.B2C2_NumHabitaHoteles), ageChdMax: parseInt(GObjParametrosConfig.B2C2_EdadesNinosHoteles), maxADT: parseInt(GObjParametrosConfig.B2C2_NumAdultosHoteles), maxCHD: parseInt(GObjParametrosConfig.B2C2_NumNinosHoteles), cantPassDefault: cantPassDefaultVueloHotel });

    fillFrmBusqVuelosH();
    autoCompleteAerolineas("txtAerolineaVH");

    
}


function tooggleFlightplusHotel(Contenedor, nombrefuncionBusqueda) {
    if ($("#btnPlusHotel:checked").length != 0) {
        //$("#btnPlusHotel").html('Solo Vuelo');
        // $("#lblbtnPlusHotel").html('');
        hiddeMtFuntion();
        InicializaVuelosHotel(Contenedor, nombrefuncionBusqueda);
    }
    else {
        InicializaVuelos(Contenedor, nombrefuncionBusqueda);
    }
}

function buscarVuelosHotel() {
    var datosBusqueda = $("#frmBuscadorVueloHotel").serialize();
    var dataFRM = $.evalJSON(params2json(datosBusqueda));

    var origenes = $("#OrigenVH").val();
    var destinos = $("#DestinoVH").val();
    var inicio = $("#dateJourneyVH").val();
    var regreso = $("#dateJourney_backVH").val();
    var hab = $("#FlightPlusInfo").IFPassengerBox("getRoomsInfoDown");

    dataFRM.VuelosDirectosVH = ($("#VuelosDirectosVH").is(':checked'));

    if (dataFRM.AerolineaVH != "") {
       dataFRM.Aerolinea = dataFRM.AerolineaVH; 
    }
    if (typeof (dataFRM.txtPromocodeVH) != "undefined" && dataFRM.txtPromocodeVH != "")
        dataFRM.txtPromocode = dataFRM.txtPromocodeVH;

    dataFRM.VuelosDirectos = ($("#VuelosDirectos").is(':checked'));
    dataFRM.flexi = GObjParametrosConfig.B2C2_CalendarVuelos == "S" && $("#fechasFlexibles").is(':checked');
    dataFRM.tipoVuelo = "RT";
    
    var filtros = addFilters(dataFRM,"VH");    
    if (origenes != "" && destinos != "" && hab.adts != "" && inicio != "" && regreso != "") {
        document.location = "/VueloPlus/Hotel/" + origenes + "/" + destinos + "/" + inicio + "/" + regreso + "/" + hab.adts + "/" + hab.chds.join('') + "/" + filtros.join(";");
        //console.log();
    }
    else {
        var msg = "";
        msg += origenes == "" ? GObjTraduccion.resValidaOrigen : "";
        if (destinos == "")
            msg += msg == "" ? GObjTraduccion.resValidaDestino : ", " + GObjTraduccion.resValidaDestino;
        if (hab.adts == "")
            msg += msg == "" ? GObjTraduccion.resValidaPasajerosVuelos : ", " + GObjTraduccion.resValidaPasajerosVuelos;
        if (inicio == "")
            msg += msg == "" ? GObjTraduccion.resValidaFechaIda : ", " + GObjTraduccion.resValidaFechaIda;
        if (regreso == "")
            msg += msg == "" ? GObjTraduccion.resValidaFechaFin : ", " + GObjTraduccion.resValidaFechaFin;

        Mensaje(msg, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
    }
}

function hiddeMtFuntion() {
    $("#btnEliminar").hide();
    $("#multiTrayecto").hide();
    $("#divOwRt").hide();

}

function getGuestRoomStay(GuestCountsType)
{
    var guest = {};
    guest.Adult = 0;
    guest.Child = 0;
    guest.Ages = [];
    for (var i = 0; i < GuestCountsType.GuestCounts.length; i++) {
        if (GuestCountsType.GuestCounts[i].AgeQualifyCode == 10) {
            guest.Adult = GuestCountsType.GuestCounts[i].Count;
        } else {
            guest.Child++;
            guest.Ages.push(GuestCountsType.GuestCounts[i].Age);
        }
    }
    return guest;
}


function ValidarFormularioVueloHotel() {  

    if ($("#txtOrigenVH").val() == "") {
            Mensaje(GObjTraduccion.resValidaOrigen, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
            return false;
        }
        
    if ($("#DestinoVH").val() == "") {
            Mensaje(GObjTraduccion.resValidaDestino, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
            return false;
        }        

    if ($("#dateJourneyVH").val() == "") {
            Mensaje(GObjTraduccion.resValidaFechaIda, GObjTraduccion.resAtencion, null, null, GObjTraduccion.resBtnCerrar);
            return false;
        }  

    return true;
}


function fillFrmBusqVuelosH(Contenedor, nombrefuncionBusqueda) {
    if (GObjParametrosVuelos != null) {       
        GobjPassengerSearch = [];
        fillGobjPassengerSearch("FlightsPlus");
        $("#FlightPlusInfo").IFPassengerBox("getInfoSearch", GobjPassengerSearch);
        $("#OrigenVH").val(GObjParametrosVuelos.Itineraries[0].IATADeparture);
        $("#txtOrigenVH").val(getInfoGeo(GObjParametrosVuelos.Itineraries[0].IATADeparture).description);
        $("#txtDestinoVH").val(getInfoGeo(GObjParametrosVuelos.Itineraries[0].IATAArrival).description);
        $("#DestinoVH").val(GObjParametrosVuelos.Itineraries[0].IATAArrival);
        $("#dateJourneyVH").val(FormateaFecha(GObjParametrosVuelos.Itineraries[0].DateDeparture));
        if (GObjParametrosVuelos.Itineraries.length > 1)
        {
            $("#dateJourney_backVH").val(FormateaFecha(GObjParametrosVuelos.Itineraries[1].DateDeparture));
        }

        if (GObjParametrosHoteles != null) {
            var temp = getUrl();
            if (temp != undefined && temp != "") {
                var filter = temp.split(',');
                if (filter[0] != undefined && filter[0] != "") {
                    fillAlloyDestinyVH();
                }
                if (filter[1] != undefined && filter[1] != "") {
                    fillAlloyDateVH();
                }
            } else {
                fillAlloyDestinyVH();
                fillAlloyDateVH();
            }
        }
    }
}

function setExcludeDestination(Metodo) {
    if (Metodo == "AutoCompleteIataHotel")
        ExcludeDestinationTypes = ["Z", "E", "T"];
    else
        ExcludeDestinationTypes = null;
}

function getLastDatePackages(IDBuscador)
{
    if (GObjParametrosPlanes == null || GObjParametrosPlanes.FechaRegreso != "" ) {
        $.ajax({
            url: "/UtilsB2C.aspx/getLastDatePackages",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            asycn: true,
            success: function (data) {
                var fecha = data.d;
                $("#CheckOutPlanes" + IDBuscador).val(fecha);
                $("#DivCheckOutPlanes" + IDBuscador).hide();
            }
        });
    }
    else {
        $("#CheckOutPlanes" + IDBuscador).val(GObjParametrosPlanes.FechaRegreso);
    }
}

function InicializaCalendar(Contenedor, nombrefuncionBusqueda) {
    $("#" + Contenedor).setTemplate(getTemplateText(GCarpetaTemplBuscador + '_calendar_flight'), null, { filter_data: false });
    $("#" + Contenedor).setParam("funcionBusqueda", nombrefuncionBusqueda);
    $("#" + Contenedor).processTemplate({});
    $("#tabSearch").append('<li><a href="#" class="activo btnbuscadorCalendar">Calendar</a></li>');
    InicializaInputCalendar();

    $("#BusCalendar").on("click", function () {
        if ($(this).is(":checked")) {
            $("#buscadorVuelos").show();
            $("#calendar_flight").hide();
        } else {
            $("#buscadorVuelos").hide();
            $("#calendar_flight").show();
        }

        $("#BusCalendar").prop("checked", false);     
    });
}
function InicializaInputCalendar() {
    AutocompleteIatas("txtOrigenCalendar", "AutoCompleteIataAirp");
}

function BuscadorCalendar() {
    if ($("#OrigenCalendar").val() != "") {
        document.location = "/vuelos/calendar/" + $("#OrigenCalendar").val() + "/" + $("#dias_calendar").val();
    }
}



function ChangeCalendar() {
    $("#calendarF").click(function () {
        if ($(this).is(":checked")) {
            $("#calendar_flight").show();
            $("#buscadorVuelos").hide();
        } else {
            $("#calendar_flight").hide();
            $("#buscadorVuelos").show();
        }

        $("#calendarF").prop("checked", false);     
    });


}

function checkIatasVueloDirecto() {
    if (typeof (GObjParametrosConfig.B2C2_IatasVueloDirecto) != "undefined" && GObjParametrosConfig.B2C2_IatasVueloDirecto != null
        && GObjParametrosConfig.B2C2_IatasVueloDirecto != "") {    

        var VecIatasVueloDirecto = GObjParametrosConfig.B2C2_IatasVueloDirecto.split("|");        
        for (var i = 0; i < VecIatasVueloDirecto.length; i++) {
            var IatasVueloDirecto = [];
            IatasVueloDirecto = VecIatasVueloDirecto[i].split(",");
            if ($("#Origen_1").val().includes(IatasVueloDirecto[0]) && $("#Destino_1").val().includes(IatasVueloDirecto[1])) {
                return true;
            } 
        }
    }
    return false;
}
function cambiarDestino(idSelect) {

    var codCity = $('#destino' + idSelect).val();
    $('#Tematicos' + idSelect).empty();
    $.ajax({
        url: "/UtilsB2C.aspx/getPackageListSeason",
        type: "POST",
        data: '{"codCity":' + codCity + '}',
        contentType: "application/json;charset=iso8859-1",
        dataType: "json",
        async: false,
        success: function (data) {
            var selected = " selected='seleted'";
            optionsSalida = JSON.parse(data.d);
            $("#Tematicos" + idSelect).append("<option value='0'" + selected + "> Todas las temporadas </option>");
            for (var i = 0; i < optionsSalida.length; i++) {
                $("#Tematicos" + idSelect).append("<option value='" + optionsSalida[i].para_para + "' >" + optionsSalida[i].para_nombre + "</option>");
            }
            $("#Tematicos" + idSelect).select2();

        }
    });
}

function fillFlights() {
    // methods.changePassengersChd();
    if (GObjParametrosVuelos != null && GObjParametrosVuelos.AgeChild != null) {
        for (var i = 0; i < GObjParametrosVuelos.AgeChild.length; i++) {
            $("#selChdBoxAge_" + divBox + "_" + idBox + "_" + (i + 1)).val(GObjParametrosVuelos.AgeChild[i]);
        }
    }
}
function clickChangePlanes(Buscador)
{
    if (Buscador == "Tematico")
    {
        $("#BuscadorPlanesContent").addClass("hidden");
        $("#BuscadorTematicoContent").removeClass("hidden");
        $("#ChangeTematico").prop("checked", false);  
    }
    if (Buscador == "Planes")
    {
        $("#BuscadorPlanesContent").removeClass("hidden");
        $("#BuscadorTematicoContent").addClass("hidden");
        $("#ChangePlanes").prop("checked", false);
    }
}
function masOpcionesHotel(validador,numHab,maxHab)
{
    if (validador == "plus") {
        for (var i = 5; i <= maxHab; i++) {
            $("#cuartoNo_" + numHab + "_" + i).removeClass("hidden");
        }
        $("#masOpcionesH_" + numHab).addClass("hidden");
        $("#menosOpcionesH_" + numHab).removeClass("hidden");

    }
    else
    {
        for (var i = 5; i <= maxHab; i++) {
            $("#cuartoNo_" + numHab + "_" + i).addClass("hidden");
        }
        $("#menosOpcionesH_" + numHab).addClass("hidden");
        $("#masOpcionesH_" + numHab).removeClass("hidden");
    }
}

function FuncionCambiosbusqueda() {
    document.getElementById("buscador").style.display = "block";
    document.getElementById("barrita-resumen").style.display = "none";
    $(".resumen-resultados").css("background", "#333");
}

function accommodationAnotherCity(control, select) {
    if ($('#' + control).is(':checked')) {
        $('#' + select).attr("disabled", false);
    } else {
        $('#' + select).attr("disabled", true);
        $('#' + select).val("");
    }
}

function getUrl() {
    //Se obtiene el valor de la URL desde el navegador
    var actual = window.location.pathname;
    //Se realiza la división de la URL
    var split = actual.split(";");
    //Se obtiene el ultimo valor de la URL
    var filters = split[7];
    return filters;
}

function fillAlloyDestinyVH() {
    GObjSelHotel = getInfoGeo(GObjParametrosHoteles.HotelSearchCriteria.Criterion.Address.AddressCode)
    var description = GObjSelHotel.description;
    if (description != $("#txtDestinoVH").val()) {
        $("#txtCiudadVH").val(GObjSelHotel.description);
        $("#CiudadVH").val(GObjParametrosHoteles.HotelSearchCriteria.Criterion.Address.AddressCode);
        $("#AlloyCityVH").prop('checked', true);
        $('#txtCiudadVH').attr("disabled", false);
    } 
}

function fillAlloyDateVH() {
    var dateStart = FormateaFecha(GObjParametrosHoteles.HotelSearchCriteria.Criterion.StayDateRange.Start);
    var dateEnd = FormateaFecha(GObjParametrosHoteles.HotelSearchCriteria.Criterion.StayDateRange.End);
    if (dateStart != $("#dateJourneyVH").val() || dateEnd != $("#dateJourney_backVH").val()) {
        $("#dateCityVH").val(dateStart);
        $("#dateCity_backVH").val(dateEnd);
        $("#CheckDateCityVH").prop('checked', true);
        $("#dateCityVH").attr("disabled", false);
        $("#dateCity_backVH").attr("disabled", false);
    }
}

function setAdictionalResults(term, results, method) {
    try {
        if (results != null) {
            term = term.toLowerCase();
            if (typeof (destinationAuxAirpor) != 'undefined' && typeof (destinationAuxAirpor[term]) != 'undefined' && method == "AutoCompleteIataAirp") {
                if (conditionalAditionalResult(results, destinationAuxAirpor[term])) {
                    results.unshift(destinationAuxAirpor[term]);
                }
            }
            if (typeof (destinationAuxHotel) != 'undefined' && typeof (destinationAuxHotel[term]) != 'undefined' && method == "AutoCompleteIataHotel") {
                if (conditionalAditionalResult(results, destinationAuxHotel[term])) {
                    results.unshift(destinationAuxHotel[term]);
                }
            }
        }
    }
    catch (e) {
    }
    return results;
} 
function conditionalAditionalResult(results, aditOject) {
    var finder = true;
    results.some(function (item) {
        if (typeof (aditOject.Cities) != "undefined" && typeof (item.Cities) != "undefined") {
            item.Cities.forEach(function (obj) {
                if (obj.IDCity == aditOject.Cities[0].IDCity) { finder = false; return; }
            });
            if (finder == false) {
                return;
            }
            if (typeof (aditOject.Destination) != "undefined" && typeof (item.Cities) != "undefined") {
                item.Cities.forEach(function (obj) {
                    if (obj.IDCity == aditOject.Destination[0].IDCity) { finder = false; return; }
                });
            }
        }
    });
          
    return finder; 
}

function removeEventsCalendar(i)
{
    if (typeof (i) == "undefined") {
        i = 1;
    }
    try {
        if (typeof ($("#dateJourney_" + i).data("dateRangePicker")) != "undefined") {
            $("#dateJourney_" + i).data("dateRangePicker").destroy();
        }
        if (typeof ($("#dateJourney_back").data("dateRangePicker")) != "undefined") {
            $("#dateJourney_back").data("dateRangePicker").destroy();
        }
    }
    catch{ }
}