﻿ObjIfMenus = {
    preNameId: "",
    pathImages: "",
    boostrapDefault: true,
};

ObjIfMenus.RenderMenu = function (objMenu) {
    if (objMenu == undefined)
        return;
    var rootNav = document.createElement("nav");
    rootNav.setAttribute("class", "wrapper");
    var ulRoot = document.createElement('ul');

    if (objMenu.length > 0) {
        for (var i = 0; i < objMenu.length; i++) {
            ulRoot.appendChild(ObjIfMenus.RenderItemMenu(objMenu[i]));
        }
        if (objMenu.length > 10)
            ulRoot.setAttribute("class", "megaMenu");
        return ulRoot;
    }
    var cssClass = objMenu.cssClass;
    if (this.boostrapDefault)
        cssClass = "nav navbar-nav" + objMenu.cssClass;
    else
        ulRoot.setAttribute("class", "navMenu");
    if (cssClass != "")
        ulRoot.setAttribute("class", cssClass);
    for (var i = 0; i < objMenu.Items.length; i++) {
        ulRoot.appendChild(ObjIfMenus.RenderItemMenu(objMenu.Items[i]));
    }
    rootNav.appendChild(ulRoot);
    return ulRoot;
}



ObjIfMenus.RenderItemMenu = function (objMenu) {
    var li = document.createElement('li');
    if (objMenu.Picture1 != null && objMenu.Picture1 != "") {
        var img = document.createElement('img');
        img.setAttribute("src", objMenu.Picture1);
        li.appendChild(img);
    }
    var lnk = document.createElement('a');
    if (objMenu.Link != "")
        lnk.setAttribute("href", objMenu.Link);
    if (objMenu.Window == false)
        lnk.setAttribute("target", objMenu.MenuName);
    //lnk.textContent = objMenu.Label;
    if (objMenu.CssClass.indexOf("fab ") != -1 || objMenu.CssClass.indexOf("far ") != -1 || objMenu.CssClass.indexOf("fas ") != -1) {
        var i = document.createElement('i');
        i.setAttribute("class", objMenu.CssClass);
        lnk.appendChild(i);
    }
    else {
        lnk.setAttribute("class", objMenu.CssClass);
    }
    var span = document.createElement("span");
    span.textContent = " " + objMenu.Label;
    lnk.appendChild(span);
    li.appendChild(lnk);

    if (objMenu.Picture2 != null && objMenu.Picture2 != "") {
        var img = document.createElement('img');
        img.setAttribute("src", objMenu.Picture2);
        li.appendChild(img);
    }
    if (objMenu.Items != null && objMenu.Items.length > 0)
        li.appendChild(ObjIfMenus.RenderMenu(objMenu.Items));
    return li;
}
