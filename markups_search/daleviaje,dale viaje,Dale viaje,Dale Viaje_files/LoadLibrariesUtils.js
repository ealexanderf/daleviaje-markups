﻿function getQueryParameters(_tipe) {
    var _str = $("script[" + _tipe + "]:first").attr(_tipe);
    return (_str || document.location.search).replace(/(^\?)/, '').split("&").map(function (n) { return n = n.split("="), this[n[0]] = n[1], this }.bind({}))[0];
}

(function () {

    var queryParams = {};
    var tempActualFunction = 0;
    var arrayFunction = [];
    jQuery.cachedScript = function (url, options) {
        var _modoDev = "";
        if (GDebugMode) {
            _modoDev = '?_=' + new Date().getTime();
        }
        options = $.extend(options || {}, {
            dataType: "script",
            cache: true,
            url: url + _modoDev
        });
        return jQuery.ajax(options).done(function () { nextFunctionArray(); })
                .fail(function () { addScriptNextError(url + queryParams.session); /*nextFunctionArray();*/ });
    };
    function initArray() {
   if (typeof(GObjParametrosConfig.B2C2_URLRecursos) != "undefined")
            arrayFunction.push(GObjParametrosConfig.B2C2_URLRecursos + "/assets/script.js");
    }

    function _init() {
        queryParams = getQueryParameters('data-utils');

        if ("_" in queryParams) {
            queryParams.session = '?_=' + queryParams['_'];
        } else {
            queryParams.session = '?_=' + new Date().getTime();
        }
        initArray();
        $.ajaxSetup({ async: true });
        if (arrayFunction.length > 0) {
            if (typeof (arrayFunction[0]) == "function") //fix this
                arrayFunction[0]();
            else
                addScripJS(arrayFunction[0]);
        }

    }
    function addScriptNextError(_url, _return) {

        if (_return) {
            return $('<script/>').attr({
                'type': 'text/javascript',
                'src': _url
            });
        }
        $('body').append(
                $('<script/>').attr({
                    'type': 'text/javascript',
                    'src': _url
                })
            );
    }
    function addScripJS(url)
    {
        $.cachedScript(url);
    }

    function nextFunctionArray() {
        tempActualFunction++;
        if (tempActualFunction < arrayFunction.length) {
                addScripJS(arrayFunction[tempActualFunction]);
        } 
        if(tempActualFunction == arrayFunction.length) {
            $('#preloader').fadeOut();
          //  InitPage();
           // completaUI();
            
            setTimeout(function () {
                InitPage();
                completaUI();
            }, 300);
            
        }
    }
    _init();
})();
