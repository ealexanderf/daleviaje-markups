$(document).ready(function() {

//Login
$("input[id$='dnnLogin']").click(function(){ ga('send','event','home-boton','iniciar', 'boton-login'); });

//Login mobile
$("input[id$='dnnLoginM']").click(function(){ ga('send','event','home-boton','iniciar', 'boton-login-mobile'); });

//Iniciar Sesion
$("input[id$='btnLogin']").click(function(){ ga('send','event','home-boton','iniciar', 'boton-iniciar-sesion'); });

//ida y vuelta
$("input[id$='BusCalendar']").click(function(){ ga('send','event','home-boton','box', 'boton-ida-vuelta'); });

//ida
$("input[id$='chkOWRT']").click(function(){ ga('send','event','home-boton','box', 'boton-ida'); });

//buscarMejorOpcion
$("input[id$='calendarF']").click(function(){ ga('send','event','home-boton','box', 'boton-busca-mejor-opcion'); });

//banners
$('[id^="carruselLandingexpress"]').each(function(i){ $(this).click( function(){ ga('send','event','home-boton','slider', 'ver-mas-'+i); }); });

//Extreme 
$('[id^="sliderHomeMainDynamic"]').each(function(i){ $(this).click( function(){ ga('send','event','home-boton','slider-ofertas', 'boton-oferta'+i); }); });

//Viajar al Mejor Precio
$('[id^="xsmejorprecio"]').click(function(){ ga('send','event','home-boton','booking', 'boton-extreme-search'); });

//Caluga Izquierda Booking
$('[id^="lnkUrlCalendario"]').click(function(){ ga('send','event','home-boton','booking', 'boton-booking-ver-mas'); });

//boton-sus
$("input[id$='btnSusc']").click(function(){ ga('send','event','home-boton','boton-box', 'boton-quiero-suscribirme'); });

//boton-especial
$("#boton-especial-1").click(function(){ ga('send','event','home-boton','especial', 'especial-1'); });

$("#boton-especial-2").click(function(){ ga('send','event','home-boton','especial', 'especial-2'); });

$("#boton-especial-3").click(function(){ ga('send','event','home-boton','especial', 'especial-3'); });

$("#boton-especial-4").click(function(){ ga('send','event','home-boton','especial', 'especial-4'); });


//seccion info
$("#landing-security-pesos").click(function(){ ga('send','event','home-boton','info', 'boton-security-pesos'); });

$("#landing-security-pesos-link").click(function(){ ga('send','event','home-link','info', 'link-informate'); });

$("#centro-contactos").click(function(){ ga('send','event','home-boton','info', 'boton-asistencia-tecnica'); });

$("#centro-contactos-link").click(function(){ ga('send','event','home-link','info', 'link-zona-de-contacto'); });

$("#puntos-cencosud").click(function(){ ga('send','event','home-boton','info', 'boton-puntos-cencosud'); });

$("#puntos-cencosud-link").click(function(){ ga('send','event','home-link','info', 'link-revisa-nuestros-programas'); });

//footer

$("#lnkFacebook").click(function(){ ga('send','event','home-boton','head', 'boton-facebook'); });

$("#lnkTwitter").click(function(){ ga('send','event','home-boton','head', 'boton-twitter'); });

$("#lnkInstagram").click(function(){ ga('send','event','home-boton','head', 'boton-instagram'); });

$("#lnkKLinkedin").click(function(){ ga('send','event','home-boton','head', 'boton-linkedin'); });

});

