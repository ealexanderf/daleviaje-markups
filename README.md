# daleviaje.com - Cambios en estilos y markup #

En este README se resumirán los cambios a tomar en cuenta para integrar con el markup actual del proyecto.

[Demo Online](https://jovial-wright-40b33b.netlify.app/)

### Estructura de carpetas ###

* assets
    * css
    * img
    * scss `Código fuente de los nuevos estilos`
* markups

### Estilos ###
* Los estilos no reemplazan a nada que actualmente este en el proyecto, para aplicarlos hay que agregar las clases necesarias en los elementos actuales.
* Se recomienda que los estilos sean llamados en último lugar, para que tengan más relevancia al momento de aplicarse y evitar cualquier problema.
* Se recomienda usar el archivo `newStyles.min.css`
* El archivo `embedStyles.css` no es un archivo que haya que aplicar, estos son los estilos que se imprimen dinamicamente en el proyecto, solo se realizó para igualar el ambiente de trabajo.
* Si se quieren editar los estilos se recomienda hacerlo desde los archivos `scss` y compilarlos, para evitar problemas de versiones.

### Imágenes ###

* Todas las imágenes están optimizadas en calidad y tamaño, no hace falta hacerles algún tipo de mejora.

### Markups ###

* En la carpeta `markups` se encuentran los formularios por separado para que sea más sencillo revisar el nuevo markup.
* Se hicieron cambios en el markup, se eliminaron divs y clases innecesarias.
* El markup de los inputs y select no cambia, a excepción de nuevos atributos en algunos campos.

#### Cambios ####
* `dvForm` en todos los formularios, junto a la clase existente `contPsajeros`.
* `dvForm-header` para agrupar el encabezado principal con otros elementos.
* `dvForm-title` encabezado principal.
* `dvForm-legend` encabezado secundario.
* `dvForm-group` div para agrupar distintos formularios dentro de un `dvForm`.
* `dvForm-row` junto a la clase `row`, se hace un row por cada linea de campos.
* `dvForm-label` reemplaza el span con clase `labelFrmReserva`.
* `dvTabs` van junto a los tabs con el id `#tabpayment`.
    * Comparar markup, ya que hubieron algunos cambios.
    * Es importante poner entre <span> la palabra **Pago con** para ocultarla en moviles.
* `dvPriceResume` reemplaza totalmente el markup de `#divmillastotal` para mostrar el valor a pagar.
* `dvCards` contiene el markup para la selección de tarjetas, el markup cambió significativamente.
* `dvForm-dues` complementa el parrafo con id `infocuotas`.
* `dvForm-advice` reemplaza el markup actual.

#### Observaciones ####
* El botón `copiar los datos el primer pasajero` se pasó al formulario de Facturación, dentro del `dvForm-header` de ese formulario.
* Añadir el atributo `inputmode="numeric"` a los campos de **Pasaporte**, **Tarjetas**, **Código de seguridad**, **Teléfono** para que salga el teclado númerico en teléfonos.
* Añadir el atributo `inputmode="email"` a los campos de **Email** para que salta un teclado optimizado para este tipo de data.
* Limitar el código de pais a 3 digitos.
* En el formulario de Bi Puntos, la tarjeta de Club Bi debe estar siempre visible.